<?php

namespace app\modules\pad\models;

/**
 * This is the model class for table "pad.t_usaha_wp".
 *
 * @property int $id_usaha_wp
 * @property string $npwpd
 * @property string $kd_ayt
 * @property string $jn_ayt
 * @property string $nm_usaha
 * @property string $nmr_siup
 * @property string $tg_siup
 * @property int $jm_peg
 * @property int $createdby
 * @property string $createdtime
 * @property int $updatedby
 * @property string $updatedtime
 */
class TUsahaWp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pad.t_usaha_wp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['npwpd', 'kd_ayt', 'jn_ayt', 'createdby', 'updatedby'], 'required'],
            [['tg_siup', 'createdtime', 'updatedtime'], 'safe'],
            [['jm_peg', 'createdby', 'updatedby'], 'default', 'value' => null],
            [['jm_peg', 'createdby', 'updatedby'], 'integer'],
            [['npwpd'], 'string', 'max' => 14],
            [['kd_ayt', 'jn_ayt'], 'string', 'max' => 4],
            [['nm_usaha'], 'string', 'max' => 50],
            [['nmr_siup'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_usaha_wp' => 'Id Usaha Wp',
            'npwpd' => 'NPWPD',
            'kd_ayt' => 'Kd Ayt',
            'jn_ayt' => 'Jn Ayt',
            'nm_usaha' => 'Nama Usaha',
            'nmr_siup' => 'No. SIUP',
            'tg_siup' => 'Tanggal SIUP',
            'jm_peg' => 'Jumlah Karyawan',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
