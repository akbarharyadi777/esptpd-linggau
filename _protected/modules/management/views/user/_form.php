<?php

use app\modules\management\models\User;
use app\modules\management\UserManagementModule;
use app\modules\pad\models\Pegawai;
use app\modules\pad\models\TDaftar;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use nenad\passwordStrength\PasswordInput;
use yii\bootstrap\ActiveForm as ActiveForm2;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var User $model
 * @var ActiveForm2 $form
 */
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'form-user',
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 4]
    ]);
    ?>
    <div class="col-md-12">
        <div class="col-md-6">
            <?=
                    $form->field($model->loadDefaultValues(), 'status')
                    ->dropDownList(User::getStatusList())
            ?>
        </div>
        <div class="col-md-6">  
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <?php
            $data = TDaftar::find()->all();
            $data2 = Pegawai::find()->all();
            $dat = array();
            foreach ($data2 as $da) {
                $dat[$da->nip] = $da->nip . ' - ' . $da->nm_pegawai;
            }
            foreach ($data as $d) {
                $dat[$d->npwpd] = $d->npwpd . ' - ' . $d->nm_wp;
            }
            echo $form->field($model, 'username')->widget(Select2::classname(), [
                'data' => $dat,
                'options' => ['placeholder' => '-- Pilih WP / Pegawai --'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);

            //Html::activeDropDownList($model, 'username', , 'npwpd', 'nm_wp'))
            ?>

        </div>
        <div class="col-md-6">
        </div>
    </div>
    <?php if ($model->isNewRecord): ?>
        <div class="col-md-12">
            <div class="col-md-6">
                <?=
                $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                    'pluginOptions' => [
                        'showMeter' => true,
                        'toggleMask' => true
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <?=
                $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                    'pluginOptions' => [
                        'showMeter' => true,
                        'toggleMask' => true
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    <?php endif; ?>

    <?php if (User::hasPermission('editUserEmail')): ?>
        <div class="col-md-12">
            <div class="col-md-6">
                <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
            </div>
            <div class="col-md-6">
            </div>
        </div>
    <?php endif; ?>


    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <?php if ($model->isNewRecord): ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Buat'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php else: ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php endif; ?>
                <?=
                Html::a(
                        UserManagementModule::t('back', 'Kembali'), ['/user-management/user/index'], ['class' => 'btn btn-warning']
                )
                ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
