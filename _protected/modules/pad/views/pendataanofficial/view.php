<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TData */

$this->title = $model->th_spt;
$this->params['breadcrumbs'][] = ['label' => 'Tdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdata-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'th_spt' => $model->th_spt, 'no_data' => $model->no_data], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'th_spt' => $model->th_spt, 'no_data' => $model->no_data], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'th_spt',
            'no_data',
            'id_spt',
            'id_tetap',
            'kode',
            'st_spt',
            'id_ayt',
            'nilai',
            'jml_pr',
            'st',
            'id_dt1',
            'id_dt2',
            'jml_pok',
            'jumlah',
            'banyak',
            'judul',
            'lokasi',
            'hari',
            'muka',
            'panjang',
            'lebar',
            'tinggi',
            'luas',
            'kenaikan',
            'sdt_pandang',
            'bunga',
            'peremajaan',
            'createdby',
            'createdtime',
            'updatedby',
            'updatedtime',
            'nmr_bayar',
            'flag',
        ],
    ]) ?>

</div>
