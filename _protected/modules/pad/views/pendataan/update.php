<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TDataSelf */

$this->title = 'Update Tdata Self: ' . ' ' . $model->th_spt;
$this->params['breadcrumbs'][] = ['label' => 'Tdata Selves', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->th_spt, 'url' => ['view', 'th_spt' => $model->th_spt, 'no_data_self' => $model->no_data_self]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdata-self-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
