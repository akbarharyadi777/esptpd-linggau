<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TData */

$this->title = 'Update Tdata: ' . ' ' . $model->th_spt;
$this->params['breadcrumbs'][] = ['label' => 'Tdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->th_spt, 'url' => ['view', 'th_spt' => $model->th_spt, 'no_data' => $model->no_data]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tdata-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
