<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_unitusaha".
 *
 * @property string $kode_unit
 * @property string $nama_unit
 * @property string $singk
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TUnitusaha extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_unitusaha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kode_unit'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['kode_unit'], 'string', 'max' => 10],
            [['nama_unit'], 'string', 'max' => 255],
            [['singk'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kode_unit' => 'Kode Unit',
            'nama_unit' => 'Nama Unit',
            'singk' => 'Singk',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
