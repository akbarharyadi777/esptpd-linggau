<style>

    .logo-login {
        text-align: center;
        margin-bottom: 15px;
    }

    .reauth-email {
        display: block;
        color: #FFF;
        line-height: 2;
        margin-bottom: 10px;
        font-size: 14px;
        text-align: center;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    .title-login h4 {
        color: #000;
        font-size: 15px;
        margin: 15px 0 5px 0;
        text-shadow: 2px 1px 0 #fff, 4px 3px 0 rgba(0, 0, 0, 0.15);
    }

    .title-login h1 {
        color: #000;
        font-size: 21px;
        margin: 0;
        text-shadow: 2px 1px 0 #fff, 4px 3px 0 rgba(0, 0, 0, 0.15);
    }

    .box-login {
        margin-left: -15px;
        margin-right: -15px;
        padding-top: 73px;
    }

    .radio label, .checkbox label {
        min-height: 20px;
        padding-left: 20px;
        margin-bottom: 0;
        font-weight: normal;
        cursor: pointer;
        color: #86cdf7;
    }

    .form-group.has-success label {
        color: #86cdf7;
    }

    .has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
        color: #ffffff;
    }

    input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill {
        background-color: #ffffff !important;
        background-image: none;
        color: rgb(0, 0, 0);
    }

</style>

<?php

use app\models\LoginForm;
use kartik\widgets\ActiveForm;
use yii\bootstrap\ActiveForm as ActiveForm2;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $form ActiveForm2 */
/* @var $model LoginForm */

$this->title = 'E - SPTPD Kota Lubuklinggau | Login';
?>

<?php
$form = ActiveForm::begin([
            'id' => 'login-form',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'enableAjaxValidation' => false,
            'fieldConfig' => [
                'autoPlaceholder' => false,
            ],
            'formConfig' => [
                'deviceSize' => ActiveForm::SIZE_SMALL,
                'showLabels' => false,
            ],
        ]);
?>
<div class="container">
    <div class="logo-login"><img src="<?= Yii::getAlias('@images'); ?>/logo-e-sptpd.png"></div>
    <div class="box box-widget" style="padding: 20px 15px;background: rgba(230,241,249,1);
background: -moz-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(230,241,249,1)), color-stop(57%, rgba(88,182,237,1)), color-stop(62%, rgba(75,177,236,1)), color-stop(100%, rgba(243,248,252,1)));
background: -webkit-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -o-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -ms-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: linear-gradient(to bottom, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6f1f9', endColorstr='#f3f8fc', GradientType=0 );">
        <div class="col-md-8 col-sm-7">
            <div class="box-login">
                <div class="col-md-2 col-sm-3">
                    <img src="<?= Yii::getAlias('@images'); ?>/logo-pemda.png" style="height: 130px;">
                </div>
                <div class="col-md-10 col-sm-9">
                    <div class="title-login">
                       	<h4>&nbsp;&nbsp;Badan Keuangan Daerah</h4>
                        <h1>&nbsp;&nbsp;Pemerintah Kota Lubuklinggau</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-5" style="background: #387090; padding-bottom: 15px; border-radius: 3px;">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <!--<img id="profile-img" class="profile-img-card" src="dist/img/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>-->
            <form class="form-signin">
                <span id="reauth-email" class="reauth-email">Login</span>
                <?=
                $form->field($model, 'username', [
                    'addon' => ['prepend' => ['content' => '<span aria-hidden="true" class="glyphicon glyphicon-user"></span>']],
                ])->input('text', ['placeholder' => 'Username']);
                ?>

                <?=
                $form->field($model, 'password', [
                    'addon' => ['prepend' => ['content' => '<span aria-hidden="true" class="glyphicon glyphicon-lock"></span>']],
                ])->input('password', ['placeholder' => 'Password']);
                ?>
                <div id="remember" class="checkbox">
                    <?php //= $form->field($model, 'rememberMe')->checkbox(['style' => 'color: #86cdf7;'])->label('Remember me')?>
                </div>
                <?= Html::submitButton('Sign in', ['class' => 'btn btn-lg btn-success btn-block btn-signin', 'name' => 'login-button']); ?>
                <?= Html::a('Register', ['/pad/pendaftaran/create'], ['class' => 'btn btn-lg btn-success btn-block btn-reg', 'name' => 'reg-button']); ?>
            </form>
            <!-- /form -->
        </div>
        <!-- /card-container -->
    </div>
</div>
<?php
ActiveForm::end();
?>