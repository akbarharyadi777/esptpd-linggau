<?php

namespace app\modules\pad\models;

use yii\db\Expression;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "t_daftar".
 *
 * @property string $no_pokok
 * @property string $npwpd
 * @property string $nm_wp
 * @property string $alm_wp
 * @property string $nm_pemilik
 * @property string $alm_pemilik
 * @property string $kel_pemilik
 * @property string $kec_pemilik
 * @property string $kota_pemilik
 * @property string $tgl_daftar
 * @property string $no_daftar
 * @property string $kd_usaha
 * @property string $tgl_kirim
 * @property string $tgl_kembali
 * @property string $st
 * @property string $telp1
 * @property string $telp2
 * @property string $kel_wp
 * @property string $kec_wp
 * @property int $createdby
 * @property string $createdtime
 * @property int $updatedby
 * @property string $updatedtime
 * @property TDaftar $npwpd0
 * @property TDaftar $tDaftar
 * @property TDataself[] $tDataselves
 * @property THapus[] $tHapuses
 * @property TKeberatan[] $tKeberatans
 * @property TKukuh $tKukuh
 */
class TDaftar extends \yii\db\ActiveRecord
{
    public $pr;
    public $golongan;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pad.t_daftar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pr', 'golongan', 'no_pokok', 'kec_wp', 'kel_wp', 'telp1', 'telp2'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['no_pokok'], 'string', 'max' => 8],
            [['npwpd'], 'string', 'max' => 14],
            [['nm_wp', 'alm_wp', 'nm_pemilik', 'alm_pemilik'], 'string', 'max' => 255],
            [['kel_pemilik', 'kec_pemilik', 'kota_pemilik'], 'string', 'max' => 30],
            [['no_daftar'], 'string', 'max' => 20],
            [['kd_usaha', 'kec_wp'], 'string', 'max' => 2],
            [['st'], 'string', 'max' => 1],
            [['telp1', 'telp2'], 'string', 'max' => 15],
            [['kel_wp'], 'string', 'max' => 3],
            [['tgl_daftar', 'tgl_kirim', 'tgl_kembali', 'createdtime', 'updatedtime', 'npwpd, nm_wp'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'no_pokok' => 'No. Pokok',
            'npwpd' => 'NPWPD',
            'nm_wp' => 'Nama WP/WR',
            'alm_wp' => 'Alamat WP/WR',
            'nm_pemilik' => 'Nama Pemilik',
            'alm_pemilik' => 'Alamat Pemilik',
            'kel_pemilik' => 'Kelurahan Pemilik',
            'kec_pemilik' => 'Kecamatan Pemilik',
            'kota_pemilik' => 'Kota Pemilik',
            'tgl_daftar' => 'Tgl Daftar',
            'no_daftar' => 'No Daftar',
            'kd_usaha' => 'Kd Usaha',
            'tgl_kirim' => 'Tgl Kirim',
            'tgl_kembali' => 'Tgl Kembali',
            'st' => 'St',
            'telp1' => 'No Telp. WP',
            'telp2' => 'No Telp. Pemilik',
            'kel_wp' => 'Kelurahan WP',
            'kec_wp' => 'Kecamatan WP',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'pr' => 'Pajak',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNpwpd0()
    {
        return $this->hasOne(self::className(), ['npwpd' => 'npwpd']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTDaftar()
    {
        return $this->hasOne(self::className(), ['npwpd' => 'npwpd']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTDataselves()
    {
        return $this->hasMany(TDataself::className(), ['npwpd' => 'npwpd']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTHapuses()
    {
        return $this->hasMany(THapus::className(), ['npwpd' => 'npwpd']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTKeberatans()
    {
        return $this->hasMany(TKeberatan::className(), ['npwpd' => 'npwpd']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTKukuh()
    {
        return $this->hasOne(TKukuh::className(), ['npwpd' => 'npwpd']);
    }

    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['=', 'npwpd', $this->npwpd])
            ->andFilterWhere(['like', 'upper(nm_wp)', strtoupper($this->nm_wp)]);

        return $dataProvider;
    }

    public function getNoPokok()
    {
        $number = self::find()->select(new Expression('MAX(substring(no_pokok,2,7)) as no_pokok'))->one()->no_pokok;
        $numb = $number + 1;
        $numbs = str_pad($numb, 7, '0', STR_PAD_LEFT);

        return $numbs;
    }
}
