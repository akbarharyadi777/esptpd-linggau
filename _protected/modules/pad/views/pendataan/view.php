<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TDataSelf */

$this->title = $model->th_spt;
?>
<div class="tdata-self-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'th_spt' => $model->th_spt, 'no_data_self' => $model->no_data_self], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'th_spt' => $model->th_spt, 'no_data_self' => $model->no_data_self], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'th_spt',
            'no_data_self',
            'id_ayt',
            'nilai',
            'jml_pjk',
            'jumlah',
            'keterangan',
            'koef1',
            'koef2',
            'npwpd',
            'masaspt',
            'no_spt',
            'tgl_awal',
            'tgl_akhir',
            'tahun',
            'tgl_terima',
            'nmr_bayar',
            'flag',
            'createdby',
            'createdtime',
            'updatedby',
            'updatedtime',
            'denda',
            'tgl_jatuh_tempo',
            'kd_ayt',
        ],
    ]) ?>

</div>
