<?php

namespace app\modules\pad\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use app\modules\management\models\User;

/**
 * This is the model class for table "pad.t_tetap".
 *
 * @property integer $id_tetap
 * @property string $tg_tetap
 * @property integer $no_tetap
 * @property string $tg_terima
 * @property string $tg_jtempo
 * @property string $kd_oper
 * @property string $cr_btp
 * @property integer $jm_btp
 * @property integer $no_setor
 * @property string $tg_setor
 * @property double $sisa_pajak
 * @property string $st_set
 * @property integer $tahun
 * @property string $st_tegur
 * @property string $st_stpd
 * @property string $st_lain
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TTetap extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public $tdata, $npwpd, $nm_wp, $kd_ayt, $nm_ayt, $jml_pr, $id_spt, 
    $no_tetap_akhir, $nmr_bayar, $verifikasi, $tgl_awal, $tgl_akhir, $st_bukti_bayar;

    public static function tableName() {
        return 'pad.t_tetap';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['tg_tetap', 'tg_terima', 'tg_jtempo', 'tg_setor'], 'safe'],
            [['no_tetap', 'jm_btp', 'no_setor', 'tahun', 'createdby', 'updatedby'], 'integer'],
            [['sisa_pajak'], 'number'],
            [['kd_oper'], 'string', 'max' => 2],
            [['cr_btp', 'st_set', 'st_tegur', 'st_stpd', 'st_lain'], 'string', 'max' => 1],
            [['tdata', 'npwpd', 'nm_wp', 'kd_ayt'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_tetap' => 'Id Tetap',
            'tg_tetap' => 'Tg Tetap',
            'no_tetap' => 'No Tetap',
            'tg_terima' => 'Tg Terima',
            'tg_jtempo' => 'Tg Jtempo',
            'kd_oper' => 'Kd Oper',
            'cr_btp' => 'Cr Btp',
            'jm_btp' => 'Jm Btp',
            'no_setor' => 'No Setor',
            'tg_setor' => 'Tg Setor',
            'sisa_pajak' => 'Sisa Pajak',
            'st_set' => 'St Set',
            'tahun' => 'Tahun',
            'st_tegur' => 'St Tegur',
            'st_stpd' => 'St Stpd',
            'st_lain' => 'St Lain',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->createdtime = date('Y-m-d H:i:s');
                $this->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            } else {
                $this->updatedby = Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            }
            return true;
        }
    }

    public function getTdata() {
        return $this->hasOne(TData::className(), ['id_tetap' => 'id_tetap']);
    }

    public function search($params) {

//        $session = Yii::$app->session;
//        $this->kd_ayt = $session->get('kd_ayt');
//        $this->tahun = $session->get('tahun');

        $query = TTetap::find()
                ->joinWith(['tdata'])
                ->where("t_data.st_spt IN ('K', 'B', 'N') AND t_data.st='2' AND t_tetap.st_set='0'")
                ->join('INNER JOIN', 'pad.t_spt', 'pad.t_spt.id_spt = pad.t_data.id_spt')
                ->join('INNER JOIN', 'pad.t_daftar', 'pad.t_daftar.npwpd = pad.t_spt.npwpd')
                ->join('INNER JOIN', 'pad.t_ayat', 'pad.t_ayat.id_ayt = pad.t_data.id_ayt')
                ->orderBy("tahun desc, no_tetap desc");


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        print_r($this->tg_tetap);
//        die;

        if ($this->kd_ayt != '') {
            if ($this->kd_ayt == '1106') {
                $this->kd_ayt = [1106, 1111];
            } else {
                $this->kd_ayt = [$this->kd_ayt];
            }
        }

        $query->andFilterWhere([
            't_tetap.tahun' => $this->tahun,
            "to_char(tg_tetap, 'MM/DD/YYYY')" => $this->tg_tetap,
            'no_tetap' => $this->no_tetap,
            't_spt.npwpd' => str_replace('.', '', $this->npwpd)
        ]);
//
        $query->andFilterWhere(['like', 'upper(trim(t_daftar.nm_wp))', strtoupper(trim($this->nm_wp))])
                ->andFilterWhere(['IN', 't_ayat.kd_ayt', $this->kd_ayt]);

        return $dataProvider;
    }


    public function searchSkpd(){ // search skpd
        $user = User::find()->where(['id' => Yii::$app->user->identity->id])->one();
        $session = Yii::$app->session;
        $tg_tetap_awal = $session->get('tg_tetap_awal');
        $tg_tetap_akhir = $session->get('tg_tetap_akhir');
        $kd_ayt = $session->get('kd_ayt');

        $no_tetap = $session->get('no_tetap');
        $no_tetap_akhir = $session->get('no_tetap_akhir');


        $query = TTetap::find()
                ->joinWith(['tdata']) 
                ->select('pad.t_tetap.tahun, pad.t_tetap.no_tetap, pad.t_tetap.tg_tetap, pad.t_daftar.npwpd, pad.t_daftar.nm_wp, pad.t_ayat.nm_ayt, pad.t_data.jml_pr, pad.t_spt.id_spt, pad.t_ayat.kd_ayt, pad.t_data.tg_cetak, pad.t_data.nmr_bayar')
                //->where("t_data.st_spt IN ('K', 'B', 'N') AND t_data.st='2' AND t_tetap.st_set='0'")
                ->where("t_ketspt.kd_spt IN ('K', 'B', 'N') AND t_data.st='2' AND t_tetap.st_persetujuan='1' AND t_tetap.st_pengesahan='1'")
                ->join('INNER JOIN', 'pad.t_spt', 'pad.t_spt.id_spt = pad.t_data.id_spt')
                ->join('INNER JOIN', 'pad.t_daftar', 'pad.t_daftar.npwpd = pad.t_spt.npwpd')
                ->join('INNER JOIN', 'pad.t_ketspt', 'pad.t_data.st_spt = pad.t_ketspt.kd_spt')
                ->join('INNER JOIN', 'pad.t_ayat', 'pad.t_ayat.id_ayt = pad.t_data.id_ayt')
                ->groupBy("pad.t_tetap.tahun, pad.t_tetap.no_tetap, pad.t_tetap.tg_tetap, pad.t_daftar.npwpd, pad.t_daftar.nm_wp, pad.t_ayat.nm_ayt, pad.t_data.jml_pr, pad.t_spt.id_spt, pad.t_ayat.kd_ayt, pad.t_data.tg_cetak, pad.t_data.nmr_bayar")
                ->orderBy("pad.t_tetap.tahun desc, pad.t_tetap.no_tetap desc");
        
        // var_dump($session->get('tg_tetap'));
        // die();
        
        if (!empty($tg_tetap_awal && $tg_tetap_akhir)) {
            $query = $query->andWhere("pad.t_tetap.tg_tetap::date BETWEEN '$tg_tetap_awal' AND '$tg_tetap_akhir' ");
        } else if (!empty($tg_tetap_awal)){
            $query = $query->andWhere("pad.t_tetap.tg_tetap::date = '$tg_tetap_awal'");
        }

        if (!empty($kd_ayt)) {
            $query = $query->andWhere("pad.t_ayat.kd_ayt = '$kd_ayt'");
        }

        if (!empty($no_tetap && $no_tetap_akhir)) {
            $query = $query->andWhere("pad.t_tetap.no_tetap::int BETWEEN '$no_tetap'::int AND '$no_tetap_akhir'::int");
        } else if (!empty($no_tetap)){
            $query = $query->andWhere("pad.t_tetap.no_tetap::int = '$no_tetap'");
        }

        if ($user->superadmin == '0' && strlen($user->username) <= 14 ){
            $query = $query->andWhere(['=', 'pad.t_daftar.npwpd', $user->username]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

//        print_r($this->tg_tetap);
//        die;

        

//         $query->andFilterWhere([
//             't_tetap.tahun' => $this->tahun,
//             "to_char(tg_tetap, 'MM/DD/YYYY')" => $this->tg_tetap,
//             'no_tetap' => $this->no_tetap,
//             't_spt.npwpd' => str_replace('.', '', $this->npwpd)
//         ]);
// // //
//         $query->andFilterWhere(['like', 'upper(trim(t_daftar.nm_wp))', strtoupper(trim($this->nm_wp))])
//                 ->andFilterWhere(['IN', 't_ayat.kd_ayt', $this->kd_ayt]);

        return $dataProvider;
    }
}
