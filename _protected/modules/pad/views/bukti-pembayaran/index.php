<?php

use app\modules\pad\models\TAyat;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TData;
use app\modules\pad\models\TSpt;
use app\modules\pad\models\TTetap;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use kartik\datecontrol\DateControl;
use yii\widgets\ListView;
use yii\helpers\Url;
use kartik\widgets\FileInput;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Menu Upload Bukti Pembayaran';
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

   
    <?php
        $form = ActiveForm::begin([
            'id' => 'cetak-skpd-form',
            'type' => ActiveForm::TYPE_VERTICAL,
            'options' => [
                'enctype' => 'multipart/form-data',
            ]
        ]);
    ?>

    <div class="row">
        <div class="col-md-12">
         <div class="col-md-12">
    <?php if (Yii::$app->session->hasFlash('bukti_pembayaran')): ?>
        <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="fa fa-window-close-o"></i>Error!</h4>
        <?= Yii::$app->session->getFlash('bukti_pembayaran') ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('bukti_pembayaran_berhasil')): ?>
        <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="fa fa-window-close-o"></i>Success!</h4>
        <?= Yii::$app->session->getFlash('bukti_pembayaran_berhasil') ?>
        </div>
    <?php endif; ?>
</div>
                 </div>
             </div>
    <div class="row">
        <div class="col-md-12">
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            Nomor Kohir
                        </label>
                    </div>
                    <div class="col-md-1">    -->
                         <?php
                            // echo $form->field($modelTetap, 'no_tetap')->textInput(['id' => 'no_tetap', 'maxlength' => 7])->label(false)
                         ?>
                    <!--</div>
                 </div>
             </div>-->
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            Nomor Bayar
                        </label>
                        <!-- <select name="TData[no_rekening]" id="no_rekening"><option value="">--Pilih Rekening--</option></select>&nbsp; -->
                    </div>
                    <div class="col-md-3">    
                         <?=
                            $form->field($modelData, 'nmr_bayar')->textInput()->label(false)
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            Bukti Pembayaran
                        </label>
                        </div>
                    <div class="col-md-5">    
                        <?=
                            $form->field($modelData, 'upload_bukti_bayar')->label(false)->widget(FileInput::classname(), [
                                'name' => 'file',
                                'options' => ['accept' => '.jpg, .png, .jpeg, .gif'],
                                'pluginOptions' => [
                                    'browseClass' => 'btn btn-success',
                                    'showPreview' => true,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'removeClass' => 'btn btn-danger',
                                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> '
                                ]
                            ]);
                            ?>
                    </div>
                </div>
            </div>
<div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                    <div class="col-md-2">
                    </div><div class="col-md-3">
                        <!-- <a class="btn btn-success" id="tampilkan_data">Tampilkan</a> -->
                         <?=
                    Html::submitButton(
                           'Upload', ['class' => 'btn btn-success']
                    )
                    ?>
                        <div class="help-block"></div>
                    <!-- </div> -->
                    </div>
                </div>
                </div>
        </div>
    </div>    
</div>

                    <?php
                        ActiveForm::end();
                    ?>