<?php

use app\modules\management\models\User;
use app\modules\management\UserManagementModule;
use nenad\passwordStrength\PasswordInput;
use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var User $model
 */
$this->title = UserManagementModule::t('back', 'Ganti Password untuk: ') . ' ' . $model->username;
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <?php
    $form = ActiveForm::begin([
                'id' => 'form-user',
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
    ?>
    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                'pluginOptions' => [
                    'showMeter' => true,
                    'toggleMask' => true
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                'pluginOptions' => [
                    'showMeter' => true,
                    'toggleMask' => true
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <?php if ($model->isNewRecord): ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Buat'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php else: ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Ganti'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php endif; ?>
                <?=
                Html::a(
                        UserManagementModule::t('back', 'Kembali'), ['/user-management/user/index'], ['class' => 'btn btn-warning']
                )
                ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

