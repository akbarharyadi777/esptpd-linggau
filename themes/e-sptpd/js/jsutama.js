$(window).on('load', function () {
    $("#spinner").fadeOut("slow");
});

function gantiImage() {
    // console.log($("#image").css('background-color'));
    if ($("#image").css('background-color') == 'rgb(54, 127, 169)') {
        $("#image").css('background-color', 'transparent');
        $("#image").css('border-bottom', '1px solid transparent');
        $("#image").css('box-shadow', 'transparent 0px 1px 0px 0px');
    } else {
        $("#image").css('background-color', 'rgb(54, 127, 169)');
        $("#image").css('border-bottom', '1px solid rgb(62, 160, 216)');
        $("#image").css('box-shadow', 'rgb(23, 101, 146) 0px 1px 0px 0px');
    }
}

function padLeft(nr, n, str) {
    return Array(n - String(nr).length + 1).join(str || '0') + nr;
}

function numerik(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
}

Number.prototype.numberFormat = function (decimals, dec_point, thousands_sep) {
    dec_point = typeof dec_point !== 'undefined' ? dec_point : ',';
    thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : '.';

    var parts = this.toFixed(decimals).split(',');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

    return parts.join(dec_point);
};

function stringToDate(text){
    var date = new Date(text);
    return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
}

function resetNotifikasi(){
    $.get( "/site/notifikasi", function( data ) {
        console.log(data);
        $('#notification_count').html(Object.keys(jQuery.parseJSON(data)).length);
        $('#notifikasi').html('');
        $.each(jQuery.parseJSON(data), function( index, value ) {
            var status;
            if(value.verifikasi == '0'){
                status = 'Menunggu Verifikasi';
            } else if(value.verifikasi == '2'){
                status = 'Gagal Verifikasi';
            } else if(value.st == '1'){
                status = 'Menunggu Penetepan';
            } else if(value.st_persetujuan == '0' || value.st_persetujuan == '2'){
                status = 'Menunggu Persetujuan';
            } else if(value.st_pengesahan == '0' || value.st_pengesahan == '2'){
                status = 'Menunggu Disahkan';
            } else {
                if(value.st_bukti_bayar=='1'){
                    status = 'Menunggu Verifikasi Pembayaran';
                } else if(value.st_bukti_bayar=='2'){
                    status = 'Gagal Verifikasi Pembayaran';
                } else {
                    status = 'Cetak SKPD';
                }
            }
            console.log('value', value);
            if (value.verifikasi == '2') {
                $('#notifikasi').append(
                    '<li><a href=#><h4><small>' 
                    + status + '</small></h4><br><p>' 
                    + value.nm_wp + '(' + value.npwpd 
                    + ')</p><p>Pajak : ' +
                    value.nm_ayt + '</p><p>Nomor Bayar : ' +
                value.nmr_bayar + '</p><p>Periode : ' + 
                    stringToDate(value.tgl_awal) + ' s/d  ' + 
                    stringToDate(value.tgl_akhir) + 
                    '</p><p>Keterangan : ' + value.komentar +
                    '</p></a></li>');                
            } else {
                $('#notifikasi').append('<li><a href=#><h4><small>' 
                + status + '</small></h4><br><p>' 
                + value.nm_wp + '(' + value.npwpd 
                + ')</p><p>Pajak : ' +
                value.nm_ayt + '</p><p>Nomor Bayar : ' +
                value.nmr_bayar + '</p><p>Periode : ' + 
                stringToDate(value.tgl_awal) + ' s/d  ' + 
                stringToDate(value.tgl_akhir) + 
                '</p></a></li>');
            }
        });
    });
}

// resetNotifikasi();

// setInterval(function () {
//     resetNotifikasi();
// }, 10000);






