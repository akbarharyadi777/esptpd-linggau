<?php

/**
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\management\models\rbacDB\Permission $model
 */
use app\modules\management\UserManagementModule;

$this->title = UserManagementModule::t('back', 'Editing permission: ') . ' ' . $model->name;
?>

<?=

$this->render('_form', [
    'model' => $model,
])
?>