<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_daftar_file".
 *
 * @property int $id
 * @property string $nama_file
 * @property string $file
 * @property string $npwpd
 */
class TDaftarFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pad.t_daftar_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_file', 'file', 'npwpd'], 'required'],
            [['nama_file', 'file'], 'string', 'max' => 200],
            [['npwpd'], 'string', 'max' => 14],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_file' => 'Nama File',
            'file' => 'File',
            'npwpd' => 'Npwpd',
        ];
    }

    protected function getHash()
    {
        return md5($this->npwpd.'-'.$this->id);
    }

    public function getPath()
    {
        $dir = Yii::getAlias('@uploads/'.$this->npwpd);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return Yii::getAlias('@uploads/'.$this->npwpd.'/'.$this->getHash().'.'.$this->file);
    }
}
