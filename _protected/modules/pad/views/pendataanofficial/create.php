<?php

use app\modules\pad\models\TDataSelf;
use yii\web\View;

/* @var $this View */
/* @var $model TDataSelf */
?>
<div class="tdata-off-create">
    <?=
    $this->render($form, [
        'model' => $model,
        'modelSpt' => $modelSpt,
        'modelDaftar' => $modelDaftar,
        'id' => $id,
        'searchAyat' => $searchAyat,
        'dataAyat' => $dataAyat,
        'form' => $form,
        'modelAyat' => $modelAyat,
        'modelReklame' => $modelReklame,
        'modelData' => $modelData
    ])
    ?>

</div>
<?php
$js = <<<JS

        $("#tdaftar-no_pokok").change(function () {
        $(this).val(padLeft($(this).val(), 6));
        $.ajax({
            url: "getwp",
            data: 'no_daftar=' + $(this).val(),
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                if (parseInt(data) === 1) {
                    $('#tspt-npwpd').val('');
                    $('#tdaftar-kel_wp').val('');
                    $('#tdaftar-kec_wp').val('');
                    $('#tdaftar-nm_wp').val('');
                    $('#tdaftar-alm_wp').val('');
                } else {
                    $('#tspt-npwpd').val(data.npwpd);
                    $('#tdaftar-kel_wp').val(data.kelurahan);
                    $('#tdaftar-kec_wp').val(data.kecamatan);
                    $('#tdaftar-nm_wp').val(data.nm_wp);
                    $('#tdaftar-alm_wp').val(data.alm_wp);
                }
                dapatkanNoRegisterOfficial();
            }
        });
    });


    function dapatkanNoRegisterOfficial() {
    // $("#loader-noregister").show();
    $.ajax({
        url: "getregister",
        data: {"tahun": $('#tdata-th_spt').val(), "kd_ayt": $('#kd_ayt').val(), "status": 2, "tgl_awal": $('#tspt-tgl_awal').val(), "no_pokok": $('#tdaftar-no_pokok').val(), "npwpd": $('#tspt-npwpd').val()},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#tdata-nmr_bayar').val(data);
            // $("#loader-noregister").hide();
        }
    });
}

$('#m_ayat').on('show.bs.modal', function () {
    var modal = $(this);
    modal.find('.modal-body').html('');
    modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
    $.ajax({
        url: "listrekening",
        data: {"tahun": $('#tdata-th_spt').val(), "kd_ayt": $('#kd_ayt').val()},
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            //console.log(data);
            modal.find('.modal-body').html(data);
        }
    });
});

$("#tspt-tgl_awal").change(function () {
    var parts = $(this).val().split('-');
    var now = new Date(parseInt(parts[2]), parseInt(parts[1]), parts[0]);
    now = moment(now).add(1, 'year').calendar();
    var part = now.split('/');
    if ($(this).val() != '') {
        $("#tspt-tgl_akhir").val(part[1] + '-' + part[0] + '-' + part[2]);
    }
    dapatkanNoRegisterOfficial();
});

$('#tdata-th_spt').change(function () {
    dapatkanNoRegisterOfficial();
});

$('#tdata-nilai, #tdata-panjang, #tdata-lebar, #tdata-jumlah, #tdata-muka, #tdata-hari, #tdata-kenaikan, #tdata-sdt_pandang, #tdata-tinggi').change(function () {
    var tarifrp = ($('#tayat-tarifrp').val() === '' || $('#tayat-tarifrp').val() === null ? 0 : parseFloat($('#tayat-tarifrp').val().replace(/\./g, '')));
    var tarifpr = ($('#tayat-tarifpr').val() === '' || $('#tayat-tarifpr').val() === null ? 0 : parseFloat($('#tayat-tarifpr').val()) / 100);
    var jml = 0;
    if ($('#kd_ayt').val() == '1108') {

       var npa = ($('#tdata-nilai').val() === '' || $('#tdata-nilai').val() === null ? 0 : parseFloat($('#tdata-nilai').val().replace(',', '.')));
       jml = npa*tarifrp*tarifpr;

    } else if ($('#kd_ayt').val() == '1104') {

    var panjang = ($('#tdata-panjang').val() === '' || $('#tdata-panjang').val() === null ? 0 : parseFloat($('#tdata-panjang').val().replace(',', '.')));
    var lebar = ($('#tdata-lebar').val() === '' || $('#tdata-lebar').val() === null ? 0 : parseFloat($('#tdata-lebar').val().replace(',', '.')));
    var luas = panjang * lebar;
    $('#tdata-luas').val((luas).toFixed(2).toString().replace('.', ','));


    var index = ($('#tdata-kenaikan').val() === '' || $('#tdata-kenaikan').val() === null ? 0 : parseFloat($('#tdata-kenaikan').val().replace(',', '.')));
    var tinggi = ($('#tdata-tinggi').val() === '' || $('#tdata-tinggi').val() === null ? 0 : parseFloat($('#tdata-tinggi').val().replace(',', '.')));
    var muka = ($('#tdata-muka').val() === '' || $('#tdata-muka').val() === null ? 0 : parseFloat($('#tdata-muka').val().replace(',', '.')));
    var jml_reklame = ($('#tdata-jumlah').val() === '' || $('#tdata-jumlah').val() === null ? 0 : parseFloat($('#tdata-jumlah').val().replace(',', '.')));
    var hari = ($('#tdata-hari').val() === '' || $('#tdata-hari').val() === null ? 0 : parseFloat($('#tdata-hari').val().replace(',', '.')));
    var sudut = ($('#tdata-sdt_pandang').val() === '' || $('#tdata-sdt_pandang').val() === null ? 0 : parseFloat($('#tdata-sdt_pandang').val().replace(',', '.')));

    var nilai = luas * tarifrp * index * tinggi * muka * jml_reklame * hari * sudut;
    jml = nilai * tarifpr;
    $('#nilai').val(nilai);
    }

    $('#tdata-jml_pr').val(jml.numberFormat(0, ',', '.'));
});

  $('#btn_simpan').click(function () {
        bootbox.dialog({
            message: "Apa Anda yakin ingin memproses data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                            label: "Ya",
                            className: "btn-success",
                            callback: function () {
                                $("#spinner").fadeIn("slow");
                                //var dataform = $("#form-pendataan").
                                var dataform = new FormData($("#form-pendataan")[0]);
                                $.ajax({
                                    type: "POST",
                                    url: "simpan",
                                    data: dataform,
                                    processData: false,
                                    contentType: false,
                                    success: function (data) {
                                        $("#spinner").fadeOut("slow");
                                        if (Math.floor(data) == data && $.isNumeric(data)) {
                                            $('#id_tetap').val(data);
                                            bootbox.dialog({
                                                message: "Proses data berhasil. Apakah Anda ingin melakukan proses penetapan?",
                                                title: "Notifikasi!",
                                                buttons: {
                                                    success: {
                                                        label: "Ya",
                                                        className: "btn-success",
                                                        callback: function () {
                                                            $('#my-content-panel-id').showLoading();
                                                            $.ajax({
                                                                url: "tetapkan",
                                                                data: {"id_tetap": $('#id_tetap').val(), "kd_ayt": $('#kd_ayt').val()},
                                                                type: 'POST',
                                                                dataType: 'json',
                                                                success: function (data) {
                                                                    $('#my-content-panel-id').hideLoading();
                                                                    bootbox.dialog({
                                                                        message: "Proses Penetapan berhasil?",
                                                                        title: "Notifikasi!",
                                                                        buttons: {
                                                                            //success: {
                                                                            //    label: "Ya",
                                                                            //    className: "btn-success",
                                                                            //    callback: function () {
                                                                            //        window.location.href = "<?php echo Url::toRoute('/pad/pendaataanofficial/cetak?id=') ?>" + $('#id_tetap').val();
                                                                            //    }
                                                                            //},
                                                                            danger: {
                                                                                label: "Ya",
                                                                                className: "btn-warning",
                                                                                callback: function () {
                                                                                    $("#btn_simpan").attr('style', 'display:none');
                                                                                    $("#form-pendataan :input").prop("disabled", true);
                                                                                }
                                                                            }
                                                                        }
                                                                    });
                                                                },
                                                                danger: {
                                                                    label: "Tidak",
                                                                    className: "btn-warning",
                                                                    callback: function () {
                                                                        $("#btn_simpan").attr('style', 'display:none');
                                                                        $("#form-pendataan :input").prop("disabled", true);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                    },
                                                    danger: {
                                                        label: "Tidak",
                                                        className: "btn-warning",
                                                        callback: function () {
                                                            $("#btn_simpan").attr('style', 'display:none');
                                                            $("#form-pendataan :input").prop("disabled", true);
                                                        }
                                                    }
                                                }
                                            });
                                        } else {
                                            bootbox.dialog({
                                                message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                                title: "Notifikasi!",
                                                buttons: {
                                                    success: {
                                                        label: "Tutup",
                                                        className: "btn-success",
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        },
                danger: {
                    label: "Tidak",
                    className: "btn-warning",
                    callback: function () {

                    }
                }
            }
        });
    });

JS;

$this->registerJs($js);
?>

<script>

</script>
