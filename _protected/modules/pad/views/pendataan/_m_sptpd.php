<?php

use app\modules\pad\models\TAyat;
use app\modules\pad\models\TCamat;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TLurah;
use kartik\grid\GridView;
use yii\helpers\Html;

?>

<div class="modal-content">   
    <hr style="border-color: #c7c7c7;">
    <div class="modal-body">
        <?php // Pjax::begin(['id' => 'grid'])?>
        <?php
        echo GridView::widget([
            'id' => 'gridSptpd',
            'dataProvider' => $data,
            //'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Tahun SPT',
                    'width' => '7%',
                    //'attribute' => 'th_spt',
                    'value' => function ($model) {
                        return $model->th_spt;
                    },
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Jenis Pajak',
                    //'attribute' => 'nm_wp',
                    'value' => function ($model) {
                        return TAyat::findOne(['id_ayt' => $model->id_ayt])->nm_ayt;
                    },
                        ],
                        [
                            //'class' => '\yii\grid\DataColumn',
                            'header' => 'Nama Wajib Pajak',
                            //'attribute' => 'nm_wp',
                            'value' => function ($model) {
                                return TDaftar::findOne(['npwpd' => $model->npwpd])->nm_wp;
                            },
                                ],
                                [
                                    //'class' => '\yii\grid\DataColumn',
                                    'header' => 'Periode',
                                    //'attribute' => 'masaspt',
                                    'value' => function ($model) {
                                        return date('d-m-Y', strtotime($model->tgl_awal)).' s/d '.date('d-m-Y', strtotime($model->tgl_akhir));
                                    },
                                ],
                                [
                                    //'class' => '\kartik\grid\DataColumn',
                                    'header' => 'Kecamatan',
                                    //'attribute' => 'masaspt',
                                    'value' => function ($model) {
                                        return TCamat::findOne(['kd_camat' => substr($model->npwpd, 9, 2)])->nm_camat;
                                    },
                                        ],
                                        [
                                            //'class' => '\kartik\grid\DataColumn',
                                            'header' => 'Kelurahan',
                                            //'attribute' => 'masaspt',
                                            'value' => function ($model) {
                                                return TLurah::findOne(['kd_camat' => substr($model->npwpd, 9, 2), 'kd_lurah' => substr($model->npwpd, 11, 3)])->nm_lurah;
                                            },
                                                ],
//                                                [
//                                                    'format' => 'raw',
//                                                    'header' => 'Lihat',
//                                                    //'vAlign' => 'middle',
//                                                    'hAlign' => 'center',
//                                                    'headerOptions' => ['class' => 'kartik-sheet-style'],
//                                                    'mergeHeader' => true,
//                                                    'value' => function ($model) {
//                                                return Html::a('<i class="glyphicon glyphicon-print"></i>', [
//                                                            '/pad/pendataan/lihat', 'no_self' => $model->no_data_self, 'tahun' => $model->th_spt
//                                                                ], [
//                                                            'class' => 'btn btn-sm btn-primary'
//                                                ]);
//                                            },
//                                                ],
                                                [
                                                    'format' => 'raw',
                                                    'header' => 'Cetak',
                                                    //'vAlign' => 'middle',
                                                    'hAlign' => 'center',
                                                    'headerOptions' => ['class' => 'kartik-sheet-style'],
                                                    'mergeHeader' => true,
                                                    'value' => function ($model) {
                                                        return Html::a('<i class="glyphicon glyphicon-print"></i>', [
                                                            '/pad/pendataan/cetaksptpd', 'no_self' => $model->no_data_self, 'tahun' => $model->th_spt,
                                                                ], [
                                                            'class' => 'btn btn-sm btn-success',
                                                ]);
                                                    },
                                                ],
                                            ],
                                            'toolbar' => [
                                            ],
                                            'export' => false,
                                            'pjax' => false,
                                            'pjaxSettings' => [
                                                'options' => [
                                                    'enablePushState' => false,
                                                ],
                                            ],
                                            'responsive' => true,
                                            'hover' => true,
                                            'panel' => [
                                                'type' => GridView::TYPE_SUCCESS,
                                                'heading' => '<i class="glyphicon glyphicon-book"></i>',
                                            ],
                                        ]);
                                        ?>
                                        <?php // Pjax::end()?>
    </div>	


