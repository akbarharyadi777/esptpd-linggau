/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$("#tdataself-nilai").maskMoney({thousands: '.', decimal: ',', precision: '0'});
$('#tdataself-no_spt').change(function () {
    $(this).val(padLeft($(this).val(), 7));
    $.ajax({
        url: "getwp",
        data: 'no_daftar=' + $(this).val(),
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            if (parseInt(data) === 1) {
                bootbox.dialog({
                    message: "Data WP tidak ditemukan!",
                    title: "Peringatan!",
                    buttons: {
                        success: {
                            label: "Tutup",
                            className: "btn-success"
                        }
                    }
                });
            } else {
                //console.log(data);
                $('#tdataself-npwpd').val(data.npwpd);
                $('#tdaftar-kel_wp').val(data.kelurahan);
                $('#tdaftar-kec_wp').val(data.kecamatan);
                $('#tdaftar-nm_wp').val(data.nm_wp);
                $('#tdaftar-alm_wp').val(data.alm_wp);
            }
            dapatkanNoRegister();
        }
    });
});

$('#m_ayat').on('show.bs.modal', function () {
    var modal = $(this);
    modal.find('.modal-body').html('');
    modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
    $.ajax({
        url: "listrekening",
        data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": $('#kd_ayt').val()},
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            //console.log(data);
            modal.find('.modal-body').html(data);
        }
    });
});

$("#tdataself-tgl_awal").change(function () {
    //console.log($(this).val());
    var parts = $(this).val().split('-');
    var now = new Date(parts[2], parts[1] - 1, parts[0]);
    var lastDayOfTheMonth = new Date(1900 + now.getYear(), now.getMonth() + 1, 0);
    if ($(this).val() != '') {
        $("#tdataself-tgl_akhir").val($.datepicker.formatDate('dd-mm-yy', lastDayOfTheMonth));
    }
    dapatkanNoRegister();
});

$("#tdataself-nilai").change(function () {
    var omset = $("#tdataself-nilai").val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    if (tarif == '') {
        bootbox.dialog({
            message: "Anda belum memilih Rekening!",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Tutup",
                    className: "btn-success"
                }
            }
        });
    } else {
        if ($('#kd_ayt').val() == '1105') {
            var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
            var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else if ($('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100))));
        }
    }
});

$('#tdataself-koef1, #tdataself-koef2').change(function () {
    var omset = $("#tdataself-nilai").val() == '' ? 1 : $('#tdataself-nilai').val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
    var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
    if (tarif == '') {
        bootbox.dialog({
            message: "Anda belum memilih Rekening!",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Tutup",
                    className: "btn-success"
                }
            }
        });
    } else {
        $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
    }
});


function getAyat(id_ayt, jn_ayt, kl_ayt, nm_ayt, tarifpr, tarifrp) {
    $('#id_ayt').val(id_ayt);
    $('#tdataself-kd_rek').val(jn_ayt.toString() + kl_ayt.toString());
    $('#tdataself-nm_ayt').val(nm_ayt);
    $('#tdataself-tarifpr').val(tarifpr);
    if ($('#kd_ayt').val() == '1105' || $('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
        $('#tdataself-tarifrp').val(numerik(tarifrp));
    }
    $('#m_ayat').modal('hide');
    var omset = $("#tdataself-nilai").val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    if (omset != '') {
        if ($('#kd_ayt').val() == '1105') {
            var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
            var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else if ($('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100))));
        }
    }
}

$('#tdataself-th_spt').change(function () {
    dapatkanNoRegister();
});

$('#btn_simpan').click(function () {
    bootbox.dialog({
        message: "Apa Anda yakin ingin memroses data ini?",
        title: "Peringatan!",
        buttons: {
            success: {
                label: "Ya",
                className: "btn-success",
                callback: function () {
                    $('#my-content-panel-id').showLoading();
                    //var dataform = $("#form-pendataan").
                    var dataform = new FormData($("#form-pendataan")[0]);
                    $.ajax({
                        type: "POST",
                        url: "simpan",
                        data: dataform,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $('#my-content-panel-id').hideLoading();
                            if (parseInt(data) === 1) {
                                bootbox.dialog({
                                    message: "Proses data berhasil.",
                                    title: "Notifikasi!",
                                    buttons: {
                                        success: {
                                            label: "Tutup",
                                            className: "btn-success",
                                            callback: function () {
                                                $("#btn_simpan").attr('style', 'display:none');
                                            }
                                        }
                                    }
                                });
                            } else {
                                bootbox.dialog({
                                    message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                    title: "Notifikasi!",
                                    buttons: {
                                        success: {
                                            label: "Tutup",
                                            className: "btn-success",
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            },
            danger: {
                label: "Tidak",
                className: "btn-warning",
                callback: function () {

                }
            }
        }
    });
});

function dapatkanNoRegister() {
    // $("#loader-noregister").show();
    $.ajax({
        url: "getregister",
        data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": $('#kd_ayt').val(), "status": 1, "tgl_awal": $('#tdataself-tgl_awal').val(), "no_pokok": $('#tdataself-no_spt').val(), "npwpd": $('#TDataSelf_npwpd').val()},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#tdataself-nmr_bayar').val(data);
            // $("#loader-noregister").hide();
        }
    });
}

$('#tdataself-tgl_awal').keypress(function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode == 9) {
        e.preventDefault();
        $('#tdataself-tgl_akhir').focus();

    }
});


$('#tdataself-th_spt').change(function () {
    if ($('#kd_ayt').val() == '1111' || $('#kd_ayt').val() == '1106') {
        if (parseInt($(this).val()) < 2016) {
            $('#kd_ayt').val('1111');
            $('#tdataself-kd_rek').parent().find('.input-group-addon').text('1111');
        } else {
            $('#kd_ayt').val('1106');
            $('#tdataself-kd_rek').parent().find('.input-group-addon').text('1106');
        }
    }
    dapatkanNoRegister()
});
