<?php

/**
 * @var ActiveForm2 $form
 * @var array $childRoles
 * @var array $allRoles
 * @var array $routes
 * @var array $currentRoutes
 * @var array $permissionsByGroup
 * @var array $currentPermissions
 * @var Role $role
 */

use app\modules\management\components\GhostHtml;
use app\modules\management\UserManagementModule;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\rbac\Role;
use yii\widgets\ActiveForm as ActiveForm2;

$this->title = 'Permissions untuk role:' . ' ' . $role->description;
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= GhostHtml::a(UserManagementModule::t('back', 'Kembali'), ['index'], ['class' => 'btn btn-sm btn-warning']) ?></h3>
    </div>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success text-center">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>




    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> <?= UserManagementModule::t('back', 'Child roles') ?>
                    </strong>
                </div>
                <div class="panel-body">

                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::to(['set-child-roles', 'id' => $role->name]),
                                'type' => ActiveForm::TYPE_HORIZONTAL,
                                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                    ]);
                    ?>
                    <?php //= Html::beginForm(['set-child-roles', 'id' => $role->name]) ?>

                    <?php foreach ($allRoles as $aRole): ?>
                        <label>
                            <?php $isChecked = in_array($aRole['name'], ArrayHelper::map($childRoles, 'name', 'name')) ? 'checked' : '' ?>
                            <input type="checkbox" <?= $isChecked ?> name="child_roles[]" value="<?= $aRole['name'] ?>">
                            <?= $aRole['description'] ?>
                        </label>

                        <?=
                        GhostHtml::a(
                                '<span class="glyphicon glyphicon-edit"></span>', ['/user-management/role/view', 'id' => $aRole['name']], ['target' => '_blank']
                        )
                        ?>
                        <br/>
                    <?php endforeach ?>


                    <hr/>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Save'), ['class' => 'btn btn-success btn-sm']
                    )
                    ?>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> <?= UserManagementModule::t('back', 'Permissions') ?>
                    </strong>
                </div>
                <div class="panel-body">
                    <?php //= Html::beginForm(['set-child-permissions', 'id' => $role->name]) ?>
                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::to(['set-child-permissions', 'id' => $role->name]),
                                'type' => ActiveForm::TYPE_HORIZONTAL,
                                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                    ]);
                    ?>
                    <div class="row">
                        <?php foreach ($permissionsByGroup as $groupName => $permissions): ?>
                            <div class="col-sm-6">
                                <fieldset>
                                    <legend><?= $groupName ?></legend>

                                    <?php foreach ($permissions as $permission): ?>
                                        <label>
                                            <?php $isChecked = in_array($permission->name, ArrayHelper::map($currentPermissions, 'name', 'name')) ? 'checked' : '' ?>
                                            <input type="checkbox" <?= $isChecked ?> name="child_permissions[]" value="<?= $permission->name ?>">
                                            <?= $permission->description ?>
                                        </label>

                                        <?=
                                        GhostHtml::a(
                                                '<span class="glyphicon glyphicon-edit"></span>', ['/user-management/permission/view', 'id' => $permission->name], ['target' => '_blank']
                                        )
                                        ?>
                                        <br/>
                                    <?php endforeach ?>

                                </fieldset>
                                <br/>
                            </div>


                        <?php endforeach ?>
                    </div>

                    <hr/>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Save'), ['class' => 'btn btn-success btn-sm']
                    )
                    ?>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS

$('.role-help-btn').off('mouseover mouseleave')
	.on('mouseover', function(){
		var _t = $(this);
		_t.popover('show');
	}).on('mouseleave', function(){
		var _t = $(this);
		_t.popover('hide');
	});
JS
);
?>