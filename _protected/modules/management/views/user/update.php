<?php

use app\modules\management\models\User;
use webvimark\extensions\BootstrapSwitch\BootstrapSwitch;
use app\modules\management\UserManagementModule;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\management\models\User $model
 */
$this->title = UserManagementModule::t('back', 'Edit user: ') . ' ' . $model->username;
?>
<div class="user-update">

    <div class="user-create">

        <?= $this->render('_form', compact('model')) ?>
    </div>
