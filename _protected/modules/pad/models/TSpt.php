<?php

namespace app\modules\pad\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_spt".
 *
 * @property integer $id_spt
 * @property string $npwpd
 * @property string $masaspt
 * @property string $no_spt
 * @property string $tg_data
 * @property string $kd_oper
 * @property string $st_so
 * @property string $st_lhp
 * @property string $tgl_awal
 * @property string $tgl_akhir
 * @property string $cr_byr
 * @property integer $jm_byr
 * @property string $thn
 * @property string $tg_terima
 * @property string $kd_wil_camat
 * @property string $kd_wil_lurah
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TSpt extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_spt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['npwpd'], 'required'],
            [['tg_data', 'tgl_awal', 'tgl_akhir', 'tg_terima', 'createdtime', 'updatedtime'], 'safe'],
            [['jm_byr', 'createdby', 'updatedby'], 'integer'],
           // [['npwpd'], 'string', 'max' => 14],
            [['masaspt'], 'string', 'max' => 6],
            [['no_spt'], 'string', 'max' => 15],
            [['kd_oper', 'kd_wil_camat'], 'string', 'max' => 2],
            [['st_so', 'st_lhp', 'cr_byr'], 'string', 'max' => 1],
            [['thn'], 'string', 'max' => 4],
            [['kd_wil_lurah'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_spt' => 'Id Spt',
            'npwpd' => 'NPWPD',
            'masaspt' => 'Masaspt',
            'no_spt' => 'No Spt',
            'tg_data' => 'Tanggal',
            'kd_oper' => 'Kd Oper',
            'st_so' => 'St So',
            'st_lhp' => 'St Lhp',
            'tgl_awal' => 'Tgl Awal',
            'tgl_akhir' => 'Tgl Akhir',
            'cr_byr' => 'Cr Byr',
            'jm_byr' => 'Jm Byr',
            'thn' => 'Thn',
            'tg_terima' => 'Tg Terima',
            'kd_wil_camat' => 'Kd Wil Camat',
            'kd_wil_lurah' => 'Kd Wil Lurah',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->createdtime = date('Y-m-d H:i:s');
                $this->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            } else {
                $this->updatedby = Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            }
            return true;
        }
    }
}
