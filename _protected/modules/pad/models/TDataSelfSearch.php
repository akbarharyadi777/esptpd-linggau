<?php

namespace app\modules\pad\models;

use app\modules\pad\models\TDataSelf;
use app\modules\pad\models\TempTDataSelf;
use app\modules\management\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TDataSelfSearch represents the model behind the search form about `app\modules\pad\models\TDataSelf`.
 */
class TDataSelfSearch extends TDataSelf {

    /**
     * @inheritdoc
     */
    public $TDaftar, $TAyat, $nm_wp;

    public function rules() {
        return [
            [['th_spt', 'keterangan', 'npwpd', 'masaspt', 'no_spt', 'tgl_awal', 'tgl_akhir', 'tahun', 'tgl_terima', 'nmr_bayar', 'flag', 'createdtime', 'updatedtime', 'tgl_jatuh_tempo', 'kd_ayt'], 'safe'],
            [['no_data_self', 'id_ayt', 'createdby', 'updatedby'], 'integer'],
            [['nilai', 'jml_pjk', 'jumlah', 'koef1', 'koef2', 'denda'], 'number'],
            [['TDaftar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {

        $session = Yii::$app->session;
        $this->kd_ayt = $session->get('kd_ayt');

        $query = TDataSelf::find();

        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        if (strlen($user->username) == 14){
            $query = $query->where(['npwpd' => $user->username]);
        }
        $query = $query->orderBy(['th_spt'=>SORT_DESC, 'no_data_self'=>SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        // $query->andFilterWhere([
        //     'no_data_self' => $this->no_data_self,
        //     'id_ayt' => $this->id_ayt,
        //     'nilai' => $this->nilai,
        //     'jml_pjk' => $this->jml_pjk,
        //     'jumlah' => $this->jumlah,
        //     'koef1' => $this->koef1,
        //     'koef2' => $this->koef2,
        //     'tgl_awal' => $this->tgl_awal,
        //     'tgl_akhir' => $this->tgl_akhir,
        //     'tgl_terima' => $this->tgl_terima,
        //     'createdby' => $this->createdby,
        //     'createdtime' => $this->createdtime,
        //     'updatedby' => $this->updatedby,
        //     'updatedtime' => $this->updatedtime,
        //     'denda' => $this->denda,
        //     'tgl_jatuh_tempo' => $this->tgl_jatuh_tempo,
        // ]);

        // $query->andFilterWhere(['like', 'th_spt', $this->th_spt])
        //         ->andFilterWhere(['like', 'keterangan', $this->keterangan])
        //         ->andFilterWhere(['like', 'npwpd', $this->npwpd])
        //         ->andFilterWhere(['like', 'masaspt', $this->masaspt])
        //         ->andFilterWhere(['like', 'no_spt', $this->no_spt])
        //         ->andFilterWhere(['like', 'tahun', $this->tahun])
        //         ->andFilterWhere(['like', 'nmr_bayar', $this->nmr_bayar])
        //         ->andFilterWhere(['like', 'flag', $this->flag])
        //         ->andFilterWhere(['like', 'kd_ayt', $this->kd_ayt]);

        return $dataProvider;
    }

    public function searchCetak($params) {

        $session = Yii::$app->session;
        $this->kd_ayt = $session->get('kd_ayt');
        $this->npwpd = $session->get('npwpd');

        $query = TDataSelf::find()->where("kd_ayt='" . $this->kd_ayt . "' and npwpd='" . $this->npwpd . "'")->orderBy('th_spt desc, no_data_self desc');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        return $dataProvider;
    }

}
