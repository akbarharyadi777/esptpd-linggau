<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TDataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tdata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'th_spt') ?>

    <?= $form->field($model, 'no_data') ?>

    <?= $form->field($model, 'id_spt') ?>

    <?= $form->field($model, 'id_tetap') ?>

    <?= $form->field($model, 'kode') ?>

    <?php // echo $form->field($model, 'st_spt') ?>

    <?php // echo $form->field($model, 'id_ayt') ?>

    <?php // echo $form->field($model, 'nilai') ?>

    <?php // echo $form->field($model, 'jml_pr') ?>

    <?php // echo $form->field($model, 'st') ?>

    <?php // echo $form->field($model, 'id_dt1') ?>

    <?php // echo $form->field($model, 'id_dt2') ?>

    <?php // echo $form->field($model, 'jml_pok') ?>

    <?php // echo $form->field($model, 'jumlah') ?>

    <?php // echo $form->field($model, 'banyak') ?>

    <?php // echo $form->field($model, 'judul') ?>

    <?php // echo $form->field($model, 'lokasi') ?>

    <?php // echo $form->field($model, 'hari') ?>

    <?php // echo $form->field($model, 'muka') ?>

    <?php // echo $form->field($model, 'panjang') ?>

    <?php // echo $form->field($model, 'lebar') ?>

    <?php // echo $form->field($model, 'tinggi') ?>

    <?php // echo $form->field($model, 'luas') ?>

    <?php // echo $form->field($model, 'kenaikan') ?>

    <?php // echo $form->field($model, 'sdt_pandang') ?>

    <?php // echo $form->field($model, 'bunga') ?>

    <?php // echo $form->field($model, 'peremajaan') ?>

    <?php // echo $form->field($model, 'createdby') ?>

    <?php // echo $form->field($model, 'createdtime') ?>

    <?php // echo $form->field($model, 'updatedby') ?>

    <?php // echo $form->field($model, 'updatedtime') ?>

    <?php // echo $form->field($model, 'nmr_bayar') ?>

    <?php // echo $form->field($model, 'flag') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
