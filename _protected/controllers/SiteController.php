<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\modules\management\models\User;
use app\modules\pad\models\TTetap;
use app\modules\pad\models\TData;
use yii\web\Response;

class SiteController extends Controller
{
    
    public function behaviors()
    {
        return [
                    'access' => [
                        'class' => AccessControl::className(),
                        'only' => ['logout'],
                        'rules' => [
                            [
                                'actions' => ['logout'],
                                'allow' => true,
                                'roles' => ['@'],
                            ],
                        ],
                    ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'logout' => ['post'],
                        ],
                    ],
                ];
    }
    
    public function actions()
    {
        return [
                    'error' => [
                        'class' => 'yii\web\ErrorAction',
                    ],
                    'captcha' => [
                        'class' => 'yii\captcha\CaptchaAction',
                        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                    ],
                ];
    }
    
    public function actionIndex()
    {
        if (\Yii::$app->user->isGuest) {
            //r			eturn $this->goHome();
            $this->redirect(\Yii::$app->urlManager->createUrl(['/pad/home/index']));
        } else {
            $this->redirect(\Yii::$app->urlManager->createUrl(['/pad/home/index']));
        }
        //r		eturn $this->redirect('site/login');
    }
    
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            //r			eturn $this->goHome();
            $this->redirect(\Yii::$app->urlManager->createUrl(['/pad/home/index']));
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                                    'model' => $model,
                        ]);
        }
    }
    
    public function actionGenerateUser()
    {
        $modelDaftar = \app\modules\pad\models\TDaftar::find()->all();
        $no = 1;
        ini_set('max_execution_time', 0);
        foreach ($modelDaftar as $daftar) {
            $model = \app\modules\management\models\User::find()->where(['username' => $daftar->npwpd])->one();
            if ($model == null) {
                $model = new \app\modules\management\models\User(['scenario' => 'newUser']);
                $model->username = $daftar->npwpd;
                $model->password_hash = 'password';
                $model->password = 'password';
                $model->repeat_password = 'password';
                if ($model->save()) {
                    \Yii::$app->db->createCommand("insert into public.auth_assignment values ('User', " . $model->id . ", " . time() . ")")
                                                    ->execute();
                } else {
                    print_r(\kartik\widgets\ActiveForm::validate($model));
                }
            }
            $no = $no + 1;
        }
        echo 'berhasil';
        echo '<br>';
        echo Html::a('Kembali ke Index', ['index'], ['class' => 'btn btn-primary']);
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        
        return $this->goHome();
    }
    
    public function actionNotifikasi()
    {
        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        $row = TTetap::find()
                    ->select("t_data.updatedtime, 
                t_data.verifikasi, 
                    t_data.st, 
                        t_tetap.st_persetujuan, 
                            t_tetap.st_pengesahan,
                                t_daftar.nm_wp,
                                    t_daftar.npwpd,
                                        t_ayat.nm_ayt,
                                            t_spt.tgl_awal,
                                                t_spt.tgl_akhir,
                                                    t_data.komentar,
														t_data.st_bukti_bayar,
                                                            t_data.nmr_bayar")
                                                ->joinWith(['tdata'])
                                                ->where("t_data.st_spt IN ('K', 'B', 'N') 
                                                AND t_tetap.st_set='0' and 
                                                t_data.online_offline=1 and t_daftar.npwpd='" . $user->username . "'")
                                                ->join('INNER JOIN', 'pad.t_spt', 'pad.t_spt.id_spt = pad.t_data.id_spt')
                                                ->join('INNER JOIN', 'pad.t_daftar', 'pad.t_daftar.npwpd = pad.t_spt.npwpd')
                                                ->join('INNER JOIN', 'pad.t_ayat', 'pad.t_ayat.id_ayt = pad.t_data.id_ayt')
                                                ->orderBy("t_data.updatedtime desc, t_tetap.tahun desc, no_tetap desc")->asArray()->all();
        echo json_encode($row);
    }
    
    public function actionUnduh($id)
    {
        $download = TData::find()->where(['nmr_bayar' => $id])->one();
        $path = Yii::getAlias('@uploads').'/'.$download->upload_file;
        
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    public function actionUnduhBukti($id)
    {
        $download = TData::find()->where(['nmr_bayar' => $id])->one();
        $path = Yii::getAlias('@uploads').'/'.$download->upload_bukti_bayar;
        
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        }
    }

    public function actionDisplayBukti($id)
    {
		$download = TData::find()->where(['nmr_bayar' => $id])->one();
        $path = Yii::getAlias('@uploads').'/'.$download->upload_bukti_bayar;
        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'image/jpeg');
        $response->format = Response::FORMAT_RAW;
        if (!is_resource($response->stream = fopen($path, 'r'))) {
                   throw new \yii\web\ServerErrorHttpException('file access failed: permission deny');
        }
        return $response->send();
    }
}
