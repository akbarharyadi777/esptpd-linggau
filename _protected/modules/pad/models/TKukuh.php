<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_kukuh".
 *
 * @property string $npwpd
 * @property string $st_pjk_hotel
 * @property string $st_pjk_restoran
 * @property string $st_pjk_hiburan
 * @property string $st_pjk_reklame
 * @property string $st_pjk_ppj
 * @property string $st_pjk_mblb
 * @property string $st_pjk_parkir
 * @property string $st_pjk_air_tanah
 * @property string $st_pjk_sbw
 * @property string $st_pjk_pbb
 * @property string $st_pjk_bphtb
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TKukuh extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_kukuh';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['npwpd'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['npwpd'], 'string', 'max' => 14],
            [['st_pjk_hotel', 'st_pjk_restoran', 'st_pjk_hiburan', 'st_pjk_reklame', 'st_pjk_ppj', 'st_pjk_mblb', 'st_pjk_parkir', 'st_pjk_air_tanah', 'st_pjk_sbw', 'st_pjk_pbb', 'st_pjk_bphtb'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'npwpd' => 'Npwpd',
            'st_pjk_hotel' => 'St Pjk Hotel',
            'st_pjk_restoran' => 'St Pjk Restoran',
            'st_pjk_hiburan' => 'St Pjk Hiburan',
            'st_pjk_reklame' => 'St Pjk Reklame',
            'st_pjk_ppj' => 'St Pjk Ppj',
            'st_pjk_mblb' => 'St Pjk Mblb',
            'st_pjk_parkir' => 'St Pjk Parkir',
            'st_pjk_air_tanah' => 'St Pjk Air Tanah',
            'st_pjk_sbw' => 'St Pjk Sbw',
            'st_pjk_pbb' => 'St Pjk Pbb',
            'st_pjk_bphtb' => 'St Pjk Bphtb',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
