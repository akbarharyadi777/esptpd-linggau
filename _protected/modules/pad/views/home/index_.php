<?php
/* @var $this View */
$this->title = 'Home Page E-SPTPD Lubuklinggau';
?>

<div class="box box-widget">
    <div class="row" style="background: rgba(230,241,249,1);
background: -moz-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(230,241,249,1)), color-stop(57%, rgba(88,182,237,1)), color-stop(62%, rgba(75,177,236,1)), color-stop(100%, rgba(243,248,252,1)));
background: -webkit-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -o-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: -ms-linear-gradient(top, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
background: linear-gradient(to bottom, rgba(230,241,249,1) 0%, rgba(88,182,237,1) 57%, rgba(75,177,236,1) 62%, rgba(243,248,252,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6f1f9', endColorstr='#f3f8fc', GradientType=0 );">
        <?php
        if (!Yii::$app->user->isGuest) {
            ?>
        <div class="col-md-7 hidden-xs hidden-sm">
            <h2 style="color: #FFF; padding-left: 50px; margin-top: 95px; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Selamat Datang</h2>
            <h1 style="color: #61e419; padding-left: 50px; font-size: 50px; text-shadow: 2px 1px 0 #265b0a, 4px 3px 0 rgba(0, 0, 0, 0.15);"><?= (empty($model) ? (empty($model2) ? $user->username : $model2->nm_pegawai) : $model->nm_wp); ?></h1>
            <span style="background: #fff; margin-left: 50px; padding: 3px 8px; border-radius: 4px; font-size: 16px; color: #125c88;"><?= (empty($model) ? (empty($model2) ? ' - ' : substr($model2->nip, 0, 8).' '.substr($model2->nip, 8, 14).' '.substr($model2->nip, 14, 15).' '.substr($model2->nip, 15, 18)) : substr($model->npwpd, 0, 1).'.'.substr($model->npwpd, 1, 1).'.'.substr($model->npwpd, 2, 7).'.'.substr($model->npwpd, 9, 2).'.'.substr($model->npwpd, 11, 3)); ?></span>
            <hr style="margin-left: 50px;">
            <h3 style="color: #FFF; padding-left: 50px; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Di e-SPTPD Kota Lubuklinggau</h3>
        </div>
        <div class="col-xs-12 hidden-lg hidden-md hidden-sm text-center">
            <h4 style="color: #FFF; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Selamat Datang</h4>
            <h3 style="color: #61e419; text-shadow: 2px 1px 0 #265b0a, 4px 3px 0 rgba(0, 0, 0, 0.15); margin-top: 0px;"><?= (empty($model) ? (empty($model2) ? $user->username : $model2->nm_pegawai) : $model->nm_wp); ?></h3>
            <span style="background: #fff; padding: 3px 8px; border-radius: 4px; font-size: 14px; color: #125c88;"><?= (empty($model) ? (empty($model2) ? ' - ' : substr($model2->nip, 0, 8).' '.substr($model2->nip, 8, 14).' '.substr($model2->nip, 14, 15).' '.substr($model2->nip, 15, 18)) : substr($model->npwpd, 0, 1).'.'.substr($model->npwpd, 1, 1).'.'.substr($model->npwpd, 2, 7).'.'.substr($model->npwpd, 9, 2).'.'.substr($model->npwpd, 11, 3)); ?></span>
            <h4 style="color: #FFF; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15); margin-top: 26px;">Di e-SPTPD Kabupaten Kubu Raya</h4>
        </div>
        <?php
        }?>
        <!--<div class="col-md-5"><img src="<?= Yii::getAlias('@images'); ?>/test_2.png" class="img-responsive"></div>-->
    </div>
</div>

<?php

use app\modules\management\models\User;
use kartik\widgets\ActiveForm;
use yii\web\View;

/*
 * @var View $this
 * @var User $model
 */
?>


<?php
$form = ActiveForm::begin([
            'id' => 'form-user',
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
        ]);
?>


<?php ActiveForm::end(); ?>


