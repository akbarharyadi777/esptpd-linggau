<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "t_lurah".
 *
 * @property string $kd_camat
 * @property string $kd_lurah
 * @property string $nm_lurah
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TLurah extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_lurah';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_camat', 'kd_lurah'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['kd_camat'], 'string', 'max' => 2],
            [['kd_lurah'], 'string', 'max' => 3],
            [['nm_lurah'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_camat' => 'Kd Camat',
            'kd_lurah' => 'Kd Lurah',
            'nm_lurah' => 'Nm Lurah',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
