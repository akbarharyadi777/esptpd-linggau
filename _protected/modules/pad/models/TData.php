<?php

namespace app\modules\pad\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_data".
 *
 * @property string $th_spt
 * @property integer $no_data
 * @property integer $id_spt
 * @property integer $id_tetap
 * @property string $kode
 * @property string $st_spt
 * @property integer $id_ayt
 * @property double $nilai
 * @property double $jml_pr
 * @property string $st
 * @property string $id_dt1
 * @property string $id_dt2
 * @property double $jml_pok
 * @property double $jumlah
 * @property integer $banyak
 * @property string $judul
 * @property string $lokasi
 * @property double $hari
 * @property double $muka
 * @property double $panjang
 * @property double $lebar
 * @property double $tinggi
 * @property double $luas
 * @property double $kenaikan
 * @property double $sdt_pandang
 * @property double $bunga
 * @property string $peremajaan
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property string $nmr_bayar
 * @property string $flag
 */
class TData extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $kd_rek, $nspr, $tgl_penerima, $penerima, $kd_ayt;
    
    public static function tableName()
    {
        return 'pad.t_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['th_spt', 'no_data', 'id_spt', 'id_tetap'], 'required'],
            [['no_data', 'id_spt', 'id_tetap', 'id_ayt', 'banyak', 'createdby', 'updatedby'], 'integer'],
            [['nilai', 'jml_pr', 'jml_pok', 'jumlah', 'hari', 'muka', 'panjang', 'lebar', 'tinggi', 'luas', 'kenaikan', 'sdt_pandang', 'bunga'], 'string', 'max' => 100],
            [['createdtime', 'updatedtime'], 'safe'],
            [['th_spt'], 'string', 'max' => 4],
            [['kode'], 'string', 'max' => 50],
            [['st_spt', 'st', 'flag'], 'string', 'max' => 1],
            [['id_dt1', 'id_dt2', 'judul', 'lokasi'], 'string', 'max' => 255],
            [['peremajaan', 'nmr_bayar'], 'string', 'max' => 20],
            [['upload_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, xls, xlsx, zip, rar'],
            [['upload_bukti_bayar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'jpg, gif, png, jepg'],
            [['upload_file', 'upload_bukti_bayar', 'koef1', 'koef2', 'jml_pok', 'nilai', 'nip', 'kode_unit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'th_spt' => 'Tahun',
            'no_data' => 'No Data',
            'id_spt' => 'Id Spt',
            'id_tetap' => 'Id Tetap',
            'kode' => 'Kode',
            'st_spt' => 'Status SPT',
            'id_ayt' => 'Id Ayt',
            'nilai' => 'Nilai',
            'jml_pr' => 'Jumlah Pajak',
            'st' => 'St',
            'id_dt1' => 'Id Dt1',
            'id_dt2' => 'Id Dt2',
            'jml_pok' => 'Jml Pok',
            'jumlah' => 'Jumlah Reklame',
            'banyak' => 'Banyak',
            'judul' => 'Judul Reklame',
            'lokasi' => 'Lokasi Reklame',
            'hari' => 'Jml Hari/Bulan/Tahun',
            'muka' => 'Jumlah sisi/muka',
            'panjang' => 'Panjang',
            'lebar' => 'Lebar',
            'tinggi' => 'Tinggi',
            'luas' => 'Luas',
            'kenaikan' => 'Kelas Jalan',
            'sdt_pandang' => 'Sudut Pandang',
            'bunga' => 'Bunga',
            'peremajaan' => 'Peremajaan',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'nmr_bayar' => 'Nomor Bayar',
            'flag' => 'Flag',
        ];
    }

    public function getTspt() {
        return $this->hasOne(TSpt::className(), ['id_spt' => 'id_spts']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->createdtime = date('Y-m-d H:i:s');
                $this->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->online_offline = 1;
                $this->updatedtime = date('Y-m-d H:i:s');
                $no_reg = TData::find()->where(['substring(nmr_bayar, 1, 18)' => substr($this->nmr_bayar, 0, 18)])->all();
                    foreach ($no_reg as $key) {
                        if($this->nmr_bayar == $key->nmr_bayar){
                        	$this->nmr_bayar = substr($this->nmr_bayar, 0, 18) . str_pad((string)(substr($this->nmr_bayar, -2) + 1), 3, "0", STR_PAD_LEFT);
                        }
                    }

            } else {
                $this->updatedby = Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
                    $this->nmr_bayar = TData::find()->where(['id_spt' => $this->id_spt])->one()->nmr_bayar;
            }
            return true;
        }
    }
}
