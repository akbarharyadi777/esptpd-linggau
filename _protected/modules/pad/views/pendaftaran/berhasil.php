<?php

use kartik\helpers\Html;

/* @var $this View */
$this->title = 'Success!';
?>
<div class="box box-widget" id="from_input">
  <div class="box-header with-border">
      <h3 class="box-title text-light-blue"><?=$this->title; ?></h3>
      <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
  </div>
  <div class="col-md-12">
      <div class="alert alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> Pendaftaran berhasil dilakukan.
        <p>Harap bersabar petugas kami akan verifikasi data terlebih dahulu.</p>
        <p>Info selanjutnya akan kami kirim lewat sms.</p>
        <p>Terima Kasih</p>
      </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
      <div class="box-footer">
        <?=Html::submitButton(' <i class="glyphicon glyphicon-ok"></i> Register', ['id' => 'btn_simpan', 'class' => 'btn btn-success']); ?>
        <?=Html::a(' <i class="glyphicon glyphicon-share-alt"></i> Login', ['/site/login'], ['id' => 'btn_simpan', 'class' => 'btn btn-primary']); ?>
      </div>
    </div>
  </div>