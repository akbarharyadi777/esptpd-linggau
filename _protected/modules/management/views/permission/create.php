<?php

/**
 *
 * @var View $this
 * @var ActiveForm $form
 * @var Permission $model
 */
use app\modules\management\models\rbacDB\Permission;
use app\modules\management\UserManagementModule;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

$this->title = UserManagementModule::t('back', 'Buat Permission');
?>

<?=

$this->render('_form', [
    'model' => $model,
])
?>