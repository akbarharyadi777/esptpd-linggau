<?php
/* @var $this yii\web\View */
$this->title = 'Home Page E-SPTPD Purwakarta';
?>
<!--<section class="content-header">
    <img src="images/logo_purwakarta_small.png"><br>
    <h4>Dinas Pengelolaan Keuangan dan Asset Daerah</h4>
    <h1>Pemerintah Kabupaten Purwakarta</h1>
</section>-->

<div class="box box-widget">
    <!--<div class="box-header with-border">
        <h3 class="box-title text-light-blue">Selamat  <i class="fa fa-angle-double-right box-title text-light-blue"></i></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <img alt="User Image" class="img-thumbnail" src="images/user-home-page.png">
    <h5>Sunusi Pardede</h5>
    <h6>NIK. 80008655008</h6>
    <small>e-SPTPD | Copyright © 2014-2015</small>-->
    <div class="row" style="background: url(<?= Yii::getAlias('@images') ?>bg-body-login_2.png) #4399cc;">
        <div class="col-md-7 hidden-xs hidden-sm">
            <h2 style="color: #FFF; padding-left: 50px; margin-top: 95px; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Selamat Datang</h2>
            <h1 style="color: #61e419; padding-left: 50px; font-size: 50px; text-shadow: 2px 1px 0 #265b0a, 4px 3px 0 rgba(0, 0, 0, 0.15);">Moh. Andriansyah</h1>
            <span style="background: #fff; margin-left: 50px; padding: 3px 8px; border-radius: 4px; font-size: 16px; color: #125c88;">P.2.3538999.55.376</span>
            <hr style="margin-left: 50px;">
            <h3 style="color: #FFF; padding-left: 50px; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Di e-SPTPD Kabupaten Purwakarta</h3>
        </div>
        <div class="col-xs-12 hidden-lg hidden-md hidden-sm text-center">
            <h4 style="color: #FFF; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15);">Selamat Datang</h4>
            <h3 style="color: #61e419; text-shadow: 2px 1px 0 #265b0a, 4px 3px 0 rgba(0, 0, 0, 0.15); margin-top: 0px;">Moh. Andriansyah</h3>
            <span style="background: #fff; padding: 3px 8px; border-radius: 4px; font-size: 14px; color: #125c88;">P.2.3538999.55.376</span>
            <h4 style="color: #FFF; text-shadow: 2px 1px 0 #0d2b5e, 4px 3px 0 rgba(0, 0, 0, 0.15); margin-top: 26px;">Di e-SPTPD Kabupaten Purwakarta</h4>
        </div>
        <div class="col-md-5"><img src="<?= Yii::getAlias('@images') ?>test_2.png" class="img-responsive"></div>
    </div>

</div>