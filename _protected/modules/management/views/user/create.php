<?php

use app\modules\management\UserManagementModule;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\management\models\User $model
 */
$this->title = UserManagementModule::t('back', 'Buat User');
?>
<div class="user-create">

    <?= $this->render('_form', compact('model')) ?>
</div>
