<?php
namespace app\modules\management\models\rbacDB\search;

class PermissionSearch extends AbstractItemSearch
{
	const ITEM_TYPE = self::TYPE_PERMISSION;
}