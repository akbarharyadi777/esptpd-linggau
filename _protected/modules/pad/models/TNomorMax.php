<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "t_nomor_max".
 *
 * @property string $tahun
 * @property integer $no_data
 * @property integer $id_lok
 * @property integer $id_res
 * @property integer $id_hot
 * @property integer $id_rek
 * @property integer $no_tetap
 * @property integer $no_setor
 * @property integer $no_tegur
 * @property integer $no_paksa
 * @property integer $no_mohon
 */
class TNomorMax extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_nomor_max';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tahun', 'no_data'], 'required'],
            [['no_data', 'id_res', 'id_hot', 'id_rek', 'no_tetap', 'no_setor', 'no_tegur', 'no_paksa', 'no_mohon'], 'integer'],
            [['tahun'], 'string', 'max' => 4]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tahun' => 'Tahun',
            'no_data' => 'No Data',
            'id_lok' => 'Id Lok',
            'id_res' => 'Id Res',
            'id_hot' => 'Id Hot',
            'id_rek' => 'Id Rek',
            'no_tetap' => 'No Tetap',
            'no_setor' => 'No Setor',
            'no_tegur' => 'No Tegur',
            'no_paksa' => 'No Paksa',
            'no_mohon' => 'No Mohon',
        ];
    }
}
