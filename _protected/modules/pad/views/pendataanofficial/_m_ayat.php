<?php

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>

<div class="modal-content">    
    <!--    <div style="border-color: #f39c12;padding: 15px;overflow: hidden;" class="box box-primary">
            <div class="tdata-self-search">
    <?php // Pjax::begin(['id' => 'search-form']) ?>
    <?php
//            $form = ActiveForm::begin([
//                        //'action' => ['index'],
//                        'method' => 'get',
    // ]);
    ?>
                <div class="col-md-12">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px;">Nama Rekening</label>
                            <div class="col-md-8">
    <?php
//                            echo $form->field($searchAyat, 'nm_ayt', [
//                                'addon' => [
//                                    'append' => [
//                                        'content' => Html::submitButton('Cari', ['class' => 'btn btn-success']),
//                                        'asButton' => true
//                                    ]
//                                ]
//                            ])->label(false)
    ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
    
    
                <div class="form-group">
    
                </div>
    <?php // ActiveForm::end(); ?>
    <?php // Pjax::end() ?>
            </div>
        </div>-->

    <hr style="border-color: #c7c7c7;">
    <div class="modal-body">
        <?php // Pjax::begin(['id' => 'grid']) ?>
        <?php
        echo GridView::widget([
            'id' => 'gridAyat',
            'dataProvider' => $dataAyat,
            'filterModel' => $searchAyat,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Kode Ayat',
                    'attribute' => 'kd_ayt',
                    'value' => function ($model) {
                        return $model->kd_ayt . '.' . $model->jn_ayt . '.' . $model->kl_ayt;
                    },
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Nama Ayat',
                    'attribute' => 'nm_ayt',
                ],
                [
                    //'class' => '\yii\grid\DataColumn',

                    'header' => 'Tarif',
                    'attribute' => 'tarifrp',
                    'value' => function ($model) {
                        return number_format($model->tarifrp, 0, '.', ',');
                    },
                ],
                [
                    //'class' => '\kartik\grid\DataColumn',
                    'header' => 'Nilai Sewa',
                    'attribute' => 'tarifpr',
                    'value' => function ($model) {
                        return $model->tarifpr * 100 . '%';
                    },
                ],
                [
                    'class' => '\yii\grid\ActionColumn',
                    'template' => '{pilih}',
                    'buttons' => [
                        'pilih' => function ($url, $model, $key) {
                            return Html::button("Pilih", ["id" => "buttonPilihPegawai", "class" => "btn btn-success",
                                        "id_ayt" => $model->id_ayt,
                                        "jn_ayt" => $model->jn_ayt,
                                        "kl_ayt" => $model->kl_ayt,
                                        "nm_ayt" => $model->nm_ayt,
                                        "tarifrp" => $model->tarifrp,
                                        "tarifpr" => $model->tarifpr * 100,
                                        "kd_ayt" => $model->kd_ayt,
                                        "onClick" => "getAyat($(this).attr('id_ayt'),$(this).attr('jn_ayt'),$(this).attr('kl_ayt'),$(this).attr('nm_ayt'),$(this).attr('tarifpr'),$(this).attr('tarifrp'),$(this).attr('kd_ayt'))"]);
                        }
                            ],
                        ]
                    ],
                    'toolbar' => [
                    ],
                    'export' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'options' => [
                            'enablePushState' => false,
                        ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => '<i class="glyphicon glyphicon-book"></i>',
                    ],
                ]);
                ?>
                <?php // Pjax::end() ?>
            </div>	


            <?php
            $js = <<<JS
            
            function getAyat(id_ayt, jn_ayt, kl_ayt, nm_ayt, tarifpr, tarifrp, kd_ayt) {
                if(kd_ayt == '1108'){
                    $('#tdata-id_ayt').val(id_ayt);
                    $('#tdata-kd_rek').val(jn_ayt.toString() + kl_ayt.toString());
                    $('#tayatsearch-nm_ayt').val(nm_ayt);
                    $('#tayatsearch-tarifpr').val(tarifpr);
                    $('#tayatsearch-tarifrp').val(numerik(tarifrp));
                
                } else if (kd_ayt == '1104') {
                    $('#id_ayt').val(id_ayt);
                    $('#tdata-kd_rek').val(jn_ayt.toString() + kl_ayt.toString());
                    $('#tayat-nm_ayt').val(nm_ayt);
                    $('#tayat-tarifpr').val(tarifpr);
                    $('#tayat-tarifrp').val(numerik(tarifrp));
                    $('#m_ayat').modal('hide');

                    var panjang = ($('#tdata-panjang').val() === '' || $('#tdata-panjang').val() === null ? 0 : parseFloat($('#tdata-panjang').val().replace(',', '.')));
                    var lebar = ($('#tdata-lebar').val() === '' || $('#tdata-lebar').val() === null ? 0 : parseFloat($('#tdata-lebar').val().replace(',', '.')));
                    var luas = panjang * lebar;
                    $('#tdata-luas').val((luas).toFixed(2).toString().replace('.', ','));
                    
                    
                    var index = ($('#tdata-kenaikan').val() === '' || $('#tdata-kenaikan').val() === null ? 0 : parseFloat($('#tdata-kenaikan').val().replace(',', '.')));
                    var tinggi = ($('#tdata-tinggi').val() === '' || $('#tdata-tinggi').val() === null ? 0 : parseFloat($('#tdata-tinggi').val().replace(',', '.')));
                    var muka = ($('#tdata-muka').val() === '' || $('#tdata-muka').val() === null ? 0 : parseFloat($('#tdata-muka').val().replace(',', '.')));
                    var jml_reklame = ($('#tdata-jumlah').val() === '' || $('#tdata-jumlah').val() === null ? 0 : parseFloat($('#tdata-jumlah').val().replace(',', '.')));
                    var hari = ($('#tdata-hari').val() === '' || $('#tdata-hari').val() === null ? 0 : parseFloat($('#tdata-hari').val().replace(',', '.')));
                    var sudut = ($('#tdata-sdt_pandang').val() === '' || $('#tdata-sdt_pandang').val() === null ? 0 : parseFloat($('#tdata-sdt_pandang').val().replace(',', '.')));
                    
                    var nilai = luas * tarifrp * index * tinggi * muka * jml_reklame * hari * sudut;
                    jml = nilai * tarifpr;
                    $('#nilai').val(nilai);
                }
                                
                //$('#tdata-jml_pr').val(jml.numberFormat(0, ',', '.'));

                $('#m_ayat').modal('hide');
            }

JS;

            $this->registerJs($js);
            ?>
