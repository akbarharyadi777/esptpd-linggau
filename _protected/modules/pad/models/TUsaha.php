<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_usaha".
 *
 * @property string $kd_usaha
 * @property string $nm_usaha
 * @property int $createdby
 * @property string $createdtime
 * @property int $updatedby
 * @property string $updatedtime
 */
class TUsaha extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_usaha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_usaha'], 'required'],
            [['createdby', 'updatedby'], 'default', 'value' => null],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['kd_usaha'], 'string', 'max' => 2],
            [['nm_usaha'], 'string', 'max' => 30],
            [['kd_usaha'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_usaha' => 'Kd Usaha',
            'nm_usaha' => 'Nm Usaha',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
