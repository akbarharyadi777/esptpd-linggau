<?php
/* @var $this View */
/* @var $content string */

use app\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

AppAsset::register($this);
?>
<style>
    #spinner
    {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?= Yii::getAlias('@images'); ?>/ajax-loader.gif) 50% 50% no-repeat #ede9df;
    }
</style>
<?php
//AdminLteAsset::register($this);
    ?>
    <?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language; ?>">
        <head>
            <meta charset="<?= Yii::$app->charset; ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags(); ?>
            <title><?= Html::encode($this->title); ?></title>
            <?php $this->head(); ?>
            <!--favicon-->
            <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::getAlias('@images'); ?>/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-16x16.png">
            <link rel="manifest" href="<?= Yii::getAlias('@images'); ?>/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <style>
                #navbar .panel-heading {
                    color: #286403;
                    background: #69df21;
                    border-bottom: 1px solid #95f55b;
                    box-shadow: 0 1px 0 0 #16597f;
                    margin: 0 0 1px 0;
                    font-size: 15px;
                }

                .panel-default {
                    border-color: transparent;
                }
                .panel {
                    margin-bottom: 0px;
                    background-color: transparent   ;
                    border: 0px transparent;
                    border-radius: 0px;
                    -webkit-box-shadow: 0 1px 0px rgba(0, 0, 0, .0);
                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0);
                }   
                .input-group-addon {
                    border-radius: 0px;
                    border-color: #d2d6de;
                    color: #000;
                    background-color: #ededed !important;
                }
                .btn-flat {
                    padding: 9px 12px !important;
                }
                
                table {
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif  !important;
                    font-size: 12px  !important;
                    line-height: 1.42857143  !important;
                    color: #333  !important;
                    background-color: #fff !important;
                }
                td {
                    vertical-align:middle  !important;
                }
            </style>
        </head>
        <body class="wysihtml5-supported skin-blue" >

            <?php $this->beginBody(); ?>
            <div class="wrapper" style='background: white;'>
                <div class="content-wrapper" style='margin-left: 0px;background: url(<?= Yii::getAlias('@images'); ?>/cloud_background.jpg) no-repeat center top;'>
                    <section class="content-header" style="background:none;">
                    <br />
                        <a id="image" class="logo" href="#">
                            <img src="<?= Yii::getAlias('@images'); ?>/logo-e-sptpd.png">
                        </a>
                        <h3>Badan Keuangan Daerah</h3>
                        <h1>Pemerintah Kota Lubuklinggau</h1>
                        <br />
                    </section>
                    <section class="container">
                        <?= $content; ?>
                    </section>
                </div>

                <footer class="main-footer" style='margin-left: 0px;margin-top: 30px;background: white;'>
                    <small>e-SPTPD | Copyright © <?= date('Y'); ?></small>
                </footer>

            </div>
            <div id="spinner"></div>
            <?php $this->endBody(); ?>
        </body>
    </html>
    <?php
    $this->endPage();
