<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\helpers\Url;

/* @var $this View */
$this->title = $judul;
?>
<div class="box box-widget" id="from_input">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title; ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'form-pendataan-hotel',
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
    ]);
    ?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div id="alert-no_pokok" class="alert alert-warning" style="display:none;">
                    <strong>Peringatan!</strong> No Pokok yang Anda masukan salah.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2 col-md-pull-0">
                        <?php
                        if (!$modelData->isNewRecord) {
                            $modelData->tgl_terima = date('d-m-Y', strtotime($modelData->tgl_terima));
                        }
                        echo $form->field($modelData, 'tgl_terima')->widget(datecontrol::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'widgetOptions' => [
                                'layout' => '{picker}{input}',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => '',
                                ],
                            ],
                        ]);
                        ?>
                    </div>

                    <div class="col-md-1 col-md-offset-2">
                        <?=
                        $form->field($modelData, 'th_spt')->widget(MaskedInput::ClassName(), [
                            //'name' => 'input-3',
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 4, 'greedy' => false],
                        ]);
                        ?>
                    </div>

                    <div class="col-md-2 col-md-offset-3">
                        <?php
                        if (strlen(Yii::$app->user->identity->username) == '14') {
                            $modelDaftar->no_pokok = substr(Yii::$app->user->identity->username, -7);
                            echo $form->field($modelDaftar, 'no_pokok')->textInput(['readOnly' => true]);
                        } else {
                            echo $form->field($modelDaftar, 'no_pokok', [
                                    'addon' => [
                                        'append' => [
                                            'content' => Html::a('<i class="fa fa-search"></i>', '#', [
                                                'class' => 'btn btn-success btn-flat button_wp',
                                            ]),
                                            'asButton' => true,
                                        ],
                                    ],
                                ])->textInput(['readOnly' => true]);
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                     <div class="col-md-3">
                        <?php
                        echo $form->field($modelData, 'kd_rek', [
                            'addon' => [
                                'prepend' => [
                                    'content' => $kode_rekening,
                                ],
                                'append' => [
                                    'content' => Html::a(
                                        '<i class="fa fa-search"></i>',
                                        '#',
                                        [
                                            'class' => 'btn btn-success btn-flat button_rek',
                                        ]),
                                    'asButton' => true,
                                ],
                            ],
                        ])->label('Kode Rekening')->textInput(['readOnly' => true]);
                         echo $form->field($modelData, 'id_ayt')->textInput(['class' => 'hidden'])->label(false);
                        ?>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <label for="exampleInputEmail1">Periode</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo DatePicker::widget([
                                        'model' => $modelData,
                                        'attribute' => 'tgl_awal',
                                        'attribute2' => 'tgl_akhir',
                                        'options' => ['placeholder' => 'tanggal awal'],
                                        'options2' => ['placeholder' => 'tanggal akhir'],
                                        'type' => DatePicker::TYPE_RANGE,
                                        'form' => $form,
                                        'layout' => '{input1}<span class="input-group-addon kv-field-separator">s/d</span>{input2}',
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy',
                                            'autoclose' => true,
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                    </div>
                </div>
            </div><!--end row-->
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?php
                        echo $form->field($modelData, 'nilai', ['addon' => [
                                // 'prepend' => [
                                //     'content' => 'Rp.',
                                // ],
                                'append' => [
                                    'content' => 'KVA',
                                ],
                            ],
                        ])->textInput(['style' => 'text-align:right;'])->label('Pemakaian Daya');
                        ?>
                    </div>
                    <div class="col-md-2 col-md-offset-1">
                        <?= $form->field($modelData, 'koef2')->textInput(['maxlength' => true, 'onkeyup' => "this.value=this.value.replace(/[^0-9,.]/g,'')"]); ?>
                    </div>
                    <div class="col-md-1 col-md-offset-1 hidden">
                        <?= $form->field($modelData, 'no_data_self')->textInput(['readOnly' => true]); ?>
                    </div>
                </div>
            </div>
            <?php if ($kode_rekening == '1102') {
                            ?>
                <!-- <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <?php //echo $form->field($modelData, 'keterangan')->textArea(['row' => 3]);?>
                        </div>
                    </div>
                </div> -->
            <?php
                        } ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?= $form->field($modelDaftar, 'npwpd')->textInput(['readOnly' => true]); ?>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <?php echo $form->field($modelDaftar, 'nm_wp')->textInput(['readOnly' => true]); ?>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($modelData, 'nmr_bayar')->textInput(['maxlength' => true, 'readOnly' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($modelDaftar, 'alm_wp')->textArea(['readOnly' => true, 'row' => 3]); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kel_wp', ['enableClientValidation' => false])->textInput(['readOnly' => true]); ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kec_wp', ['enableClientValidation' => false])->textInput(['readOnly' => true]); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelAyat, 'nm_ayt')->textInput(['readOnly' => true])->label('Nama Rekening'); ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                        echo $form->field($modelAyat, 'tarifpr', [
                            'addon' => [
                                'append' => [
                                    'content' => '<span class="bg-gray disabled color-palette">%</span>',
                                ],
                            ],
                        ])->textInput(['readOnly' => true, 'style' => 'text-align:right;'])->label('Tarif Persen');
                        ?>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <?php
                        echo $form->field($modelData, 'jml_pjk', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ],
                                'append' => [
                                    'content' => ',00',
                                ],
                            ],
                        ])->textInput([
                            'style' => 'text-align:right;',
                            'readOnly' => true,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="box-shadow: 0 0 31px 0 rgba(0, 0, 0, 0.3) inset; overflow:hidden;" class="alert">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?=
                            $form->field($modelData, 'upload_file')->label(false)->widget(FileInput::classname(), [
                                'name' => 'file',
                                'options' => ['accept' => '.xls, .xlsx, .pdf, .zip, .rar'],
                                'pluginOptions' => [
                                    'browseClass' => 'btn btn-success',
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'removeClass' => 'btn btn-danger',
                                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?= Html::button(' Simpan', ['id' => 'btn_simpan', 'class' => 'btn btn-success']); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<?php
    ActiveForm::end();
?>
<?php
$url_cetak = Url::to(['pendataan/cetak']);
$js = <<<JS

jQuery(document).ready(function(){
    // $('#tdataself-th_spt').change(function () {
    //     $.ajax({
    //         url: "no-res",
    //         data: 'tahun=' + $('#tdataself-th_spt').val(),
    //         type: 'POST',
    //         // dataType: 'json',
    //         success: function (data) {
    //             $('#tdataself-no_form').val(data + '.' + ((new Date()).getMonth() + 1).toString() + '.' + ((new Date()).getFullYear()).toString().substr(2,2));
    //             // console.log(data);
    //             $('#tdataself-no_spt').val(data + '/RES/' + $('#tdataself-th_spt').val().substr(2,2));
    //         }
    //     });
    //     if (parseInt($(this).val()) > parseInt(new Date().getFullYear()) || parseInt($(this).val()) < parseInt(new Date().getFullYear()) - 10) {
    //         $(this).val('');
    //     }
    // });

    $('#tdaftar-no_pokok').change(function(){
        $(this).val(padLeft($(this).val(), 6));
        $('#alert-no_pokok').hide();
        $.ajax({
            url: "getwp",
            data: {
                no_daftar: $(this).val(),
                kd_ayt: '$kode_rekening',
                jn_ayt: $('#tdataself-kd_rek').val().substr(0, 2),
            },
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                if (parseInt(data) === 1) {
                    $('#tdaftar-npwpd').val('');
                    $('#tdaftar-kel_wp').val('');
                    $('#tdaftar-kec_wp').val('');
                    $('#tdaftar-nm_wp').val('');
                    $('#tdaftar-alm_wp').val(''); 
                    if($('#tdaftar-no_pokok').val() != '000000'){
                        $('#alert-no_pokok').show();
                    }
                } else {
                    $('#tdaftar-npwpd').val(data.npwpd);
                    $('#tdaftar-kel_wp').val(data.kelurahan);
                    $('#tdaftar-kec_wp').val(data.kecamatan);
                    $('#tdaftar-nm_wp').val(data.nm_wp);
                    $('#tdaftar-alm_wp').val(data.alm_wp);
                    $('#tusahawp-nm_usaha').val(data.nm_usaha);
                }
            }
        });
        dapatkanNoRegister();
    }).trigger('change');

    $('.button_wp').on('click', function () {
        $('#modal-pop').modal('show');
        var modal = $('#modal-pop');
        modal.find('.modal-body').html('');
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
        $.ajax({
            url: "list-wp",
            //data: {"tahun": $('#tdata-th_spt').val(), "kd_ayt": '$kode_rekening'},
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        }); 
    });

    $('.button_rek').on('click', function () {
        $('#modal-pop').modal('show');
        var modal = $('#modal-pop');
        modal.find('.modal-body').html('');
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
        $.ajax({
            url: "listrekening",
            data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": '$kode_rekening'},
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
        dapatkanNoRegister();
    });


    $("#tdataself-tgl_awal").change(function () {
        var parts = $(this).val().split('-');
        var now = new Date(parts[2], parts[1] - 1, parts[0]);
        var lastDayOfTheMonth = new Date(1900 + now.getYear(), now.getMonth() + 1, 0);
        if ($(this).val() != '') {
            $("#tdataself-tgl_akhir").val($.datepicker.formatDate('dd-mm-yy', lastDayOfTheMonth));
        }
        dapatkanNoRegister();
    });

    $("#tdataself-nilai").maskMoney({thousands: '.', decimal: ',', precision: '0'});

    $('#tdataself-th_spt').change(function(){
        dapatkanNoRegister();
        $.ajax({
            url: "get-no-data",
            data: {
                tahun:$(this).val()
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                $("#tdataself-no_data").val(data);
            }
        });
    });
    
    $("#tdataself-nilai, #tdataself-koef2").change(function () {
        var omset = $("#tdataself-nilai").val().replace(/\./g, "");
        var koef = $("#tdataself-koef2").val().replace(',', '.');
        var tarif = $("#tayat-tarifpr").val();
        if (tarif == '') {
            tarif=0;
        }
        if(koef == '' || isNaN(koef)){
            koef = 1;
        }
        if (omset != '' && !isNaN(omset)){
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * parseFloat(koef) * (parseFloat(tarif) / 100))));
        }
    });

    $('#btn_simpan').click(function () {
        bootbox.dialog({
            message: "Apa Anda yakin ingin memproses data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Ya",
                    className: "btn-success",
                    callback: function () {
                        // $("#spinner").fadeIn("slow");
                        var dataform = new FormData($("#form-pendataan-hotel")[0]);
                        $.ajax({
                            type: "POST",
                            url: "save",
                            data: dataform,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                var data = data.split('/')
                                console.log(data);
                                $("#spinner").fadeOut("slow");
                                if (parseInt(data[0]) >= 1) {
                                    bootbox.dialog({
                                        message: "Proses data berhasil.",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                                callback: function () {
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                }
                                            },
                                            danger: {
                                                label: "Cetak SPTPD",
                                                className: "btn-info",
                                                callback: function () {
                                                    window.open('$url_cetak' + '?no_data_self=' + data[1] + '&tahun=' + data[0], '_blank');
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    bootbox.dialog({
                                        message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali <br>",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "Tidak",
                    className: "btn-warning",
                    callback: function () {

                    }
                }
            }
        });
    });

    function dapatkanNoRegister() {
        var th_spt = $('#tdataself-th_spt').val();
        var kd_ayt = '$kode_rekening';
        var status = 2;
        var tgl_awal = $("#tdataself-tgl_awal").val();
        var no_pokok = $("#tdaftar-no_pokok").val();
        var npwpd = $('#tdaftar-npwpd').val();
        $.ajax({
            url: "getregister",
            data: {
                "tahun": th_spt, 
                "kd_ayt": kd_ayt, 
                "status": status, 
                "tgl_awal": tgl_awal, 
                "no_pokok": no_pokok, 
                "npwpd": npwpd
                },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $("#tdataself-nmr_bayar").val(data);
            }
        });
    };

});

JS;

$this->registerJs(
    $js,
    View::POS_END,
    'js-page'
);

?>