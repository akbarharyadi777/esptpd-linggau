<?php

namespace app\modules\pad\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "temp_tdataself".
 *
 * @property string $th_spt
 * @property integer $no_data_self
 * @property integer $id_ayt
 * @property double $nilai
 * @property double $jml_pjk
 * @property double $jumlah
 * @property string $keterangan
 * @property double $koef1
 * @property double $koef2
 * @property string $npwpd
 * @property string $masaspt
 * @property string $no_spt
 * @property string $tgl_awal
 * @property string $tgl_akhir
 * @property string $tahun
 * @property string $tgl_terima
 * @property string $nmr_bayar
 * @property string $flag
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property double $denda
 * @property string $tgl_jatuh_tempo
 * @property string $kd_ayt
 */
class TempTDataSelf extends ActiveRecord {

    /**
     * @inheritdoc
     */

    public static function tableName() {
        return 'public.temp_tdataself';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['th_spt', 'no_data_self', 'id_ayt', 'npwpd', 'masaspt', 'no_spt', 'tgl_awal', 'tgl_akhir', 'tgl_terima', 'nmr_bayar', 'tgl_jatuh_tempo'], 'required'],
            [['no_data_self', 'id_ayt', 'createdby', 'updatedby'], 'integer'],
            [['jumlah', 'denda'], 'number'],
            [['tgl_awal', 'tgl_akhir', 'tgl_terima', 'createdtime', 'updatedtime', 'tgl_jatuh_tempo'], 'safe'],
            [['th_spt', 'tahun', 'kd_ayt'], 'string', 'max' => 4],
            [['keterangan'], 'string', 'max' => 255],
            [['masaspt'], 'string', 'max' => 6],
            [['no_spt'], 'string', 'max' => 7],
            [['nmr_bayar'], 'string', 'max' => 20],
            [['flag'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'th_spt' => 'Tahun',
            'no_data_self' => 'No Data Self',
            'id_ayt' => 'Id Ayt',
            'nilai' => 'Field ini',
            'jml_pjk' => 'Jumlah Pajak',
            'jumlah' => 'Jumlah',
            'keterangan' => 'Keterangan',
            'koef1' => 'Koef1',
            'koef2' => 'Koef2',
            'npwpd' => 'NPWPD',
            'masaspt' => 'Masaspt',
            'no_spt' => 'No. Pokok',
            'tgl_awal' => 'Tgl Awal',
            'tgl_akhir' => 'Tgl Akhir',
            'tahun' => 'Tahun',
            'tgl_terima' => 'Tanggal',
            'nmr_bayar' => 'Nomor Bayar',
            'flag' => 'Flag',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'denda' => 'Denda',
            'tgl_jatuh_tempo' => 'Tgl Jatuh Tempo',
            'kd_ayt' => 'Kd Ayt',
        ];
    }

    public function getTAyat() {
        return $this->hasOne(TAyat::className(), ['id_ayt' => 'id_ayt']);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
			if(empty($this->nilai)){
				$this->nilai = 0;
				$this->jml_pjk = 0;
			}
            if ($this->isNewRecord) {
                $this->createdtime = date('Y-m-d H:i:s');
                $this->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            } else {
                $this->updatedby = Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
            }
            return true;
        }
    }

}
