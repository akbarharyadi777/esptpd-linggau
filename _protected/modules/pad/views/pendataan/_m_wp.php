<?php

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<?php
        echo GridView::widget([
            'id' => 'gridAyat',
            'dataProvider' => $data,
            'filterModel' => $search,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'NPWPD',
                    'attribute' => 'npwpd',
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Nama Wajib Pajak',
                    'attribute' => 'nm_wp',
                ],
                [
                    'class' => '\yii\grid\ActionColumn',
                    'template' => '{pilih}',
                    'buttons' => [
                        'pilih' => function ($url, $model, $key) {
                            return Html::button("Pilih", ["id" => "buttonPilihPokok", "class" => "btn btn-success",
                                        "no_pokok" => $model->no_pokok,
                                        "onClick" => "
                                            $('#tdaftar-no_pokok').val($(this).attr('no_pokok').substring(1));
                                            $('#modal-pop').modal('hide');
                                            $('#tdaftar-no_pokok').trigger('change');
                                        "]);
                        }
                            ],
                        ]
                    ],
                    'toolbar' => [
                    ],
                    'export' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'options' => [
                            'enablePushState' => false,
                        ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => '<i class="glyphicon glyphicon-book"></i> List Wajib Pajak',
                    ],
                ]);
                ?>