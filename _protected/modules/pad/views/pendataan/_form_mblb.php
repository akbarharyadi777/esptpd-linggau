<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use app\modules\pad\models\TKet;
use app\modules\pad\models\Pegawai;
use app\modules\pad\models\TUnitusaha;
use kartik\datetime\DateTimePicker;
use app\modules\management\models\User;
use kartik\widgets\Select2;
use yii\helpers\Url;

/* @var $this View */
$this->title = $judul;
?>

<div class="box box-widget" id="from_input">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'form-pendataan-hotel',
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
    ]);
    ?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div id="alert-no_pokok" class="alert alert-warning" style="display:none;">
                    <strong>Peringatan!</strong> No Pokok yang Anda masukan salah.
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2 col-md-pull-0">
                        <?php
                        if (!$modelData->isNewRecord) {
                            $modelData->tgl_terima = date('d-m-Y', strtotime($modelData->tgl_terima));
                        }
                        echo $form->field($modelData, 'tgl_terima')->widget(datecontrol::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'widgetOptions' => [
                                'layout' => '{picker}{input}',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => ''
                                ]
                            ]
                        ]);
                        ?>
                    </div>

                    <div class="col-md-1 col-md-offset-2">
                        <?=
                        $form->field($modelData, 'th_spt')->widget(MaskedInput::ClassName(), [
                            //'name' => 'input-3',
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 4, 'greedy' => false]
                        ]);
                        ?>
                    </div>

                    <div class="col-md-2 col-md-offset-3">
                        <?php
                        if (strlen(Yii::$app->user->identity->username) == '13' && filter_var((int)Yii::$app->user->identity->username, FILTER_VALIDATE_INT)){
                            $modelDaftar->no_pokok = substr(Yii::$app->user->identity->username,1,6);
                            echo $form->field($modelDaftar, 'no_pokok')->textInput(['readOnly' => true]);
                        } else {
                            echo $form->field($modelDaftar, 'no_pokok', [
                                    'addon' => [    
                                        'append' => [
                                            'content' => Html::a('<i class="fa fa-search"></i>', '#', [
                                                'class' => 'btn btn-success btn-flat button_wp'
                                            ]),
                                            'asButton' => true
                                        ]
                                    ]
                                ])->textInput(['readOnly' => true]);
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <label for="exampleInputEmail1">Periode</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo DatePicker::widget([
                                        'model' => $modelData,
                                        'attribute' => 'tgl_awal',
                                        'attribute2' => 'tgl_akhir',
                                        'options' => ['placeholder' => 'tanggal awal'],
                                        'options2' => ['placeholder' => 'tanggal akhir'],
                                        'type' => DatePicker::TYPE_RANGE,
                                        'form' => $form,
                                        'layout' => '{input1}<span class="input-group-addon kv-field-separator">s/d</span>{input2}',
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy',
                                            'autoclose' => true,
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                    </div>

                    <div class="col-md-3">
                        <?= $form->field($modelData, 'nmr_bayar')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
                    </div>
                    <div class="col-md-2 col-md-offset-1">
                        <?= $form->field($modelData, 'no_spt')->textInput(['readOnly' => true]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= $form->field($modelData, 'no_form')->textInput(['readOnly' => true]) ?>
                    </div>
                    <div class="col-md-1 col-md-offset-1 hidden">
                        <?= $form->field($modelData, 'no_data_self')->textInput(['readOnly' => true]) ?>
                    </div>
                </div>
            </div><!--end row-->
            <hr style="width: 100%;"/>
            <div id="pribadi-badan" class="span-reklame show">
                <div class="col-md-11" style="text-align: right;">
                    <?= Html::button('<i class="fa fa-times" aria-hidden="true"></i>', ['id' => 'hps-usaha', 'style' => 'margin-right: -40px;', 'class' => 'btn btn-danger remove-mblb']) ?>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label class="control-label">
                                    Kode Rekening
                                </label>
                                <input type="hidden" name="TDataSelfMblb[id_ayt][]" id="TDataSelfMblb_id_ayt_0" class="kdrek">
                                <div class="help-block"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <?php echo $form->field($modelDataMblb, 'volume', [
                                'addon' => [    
                                    'append' => [
                                        'content' => '<span style="padding-left:8px;" id="satuan_0" class="satuan">m<sup>3</sup></span>'
                                    ]
                                ]
                            ])->textInput([
                                'id' => 'TDataSelfMblb_volume_0', 
                                'size' => 7, 
                                'maxlength' => 15, 
                                "class" => "kdrek", 
                                "onkeyup" => "this.value=this.value.replace(/[^0-9.,]/g,'')", 
                                "name" => "TDataSelfMblb[volume][]"
                            ])->label('Volume'); ?> 
                        </div>
                        <div class="col-md-3 col-md-offset-1">
                            <?php echo $form->field($modelAyat, 'tarifrp', [
                                'addon' => [
                                    'prepend' => [
                                            'content' => 'Rp.'
                                        ],
                                    'append' => [
                                        'content' => ',00'
                                    ]
                                ]
                            ])->textInput([
                                'id' => 'TAyat_rupiah_0', 
                                "class" => 'kdrek',
                                "style" => "text-align:right;", 
                                "readOnly" => true
                            ])->label('Harga Pasar'); ?> 
                        </div>
                        <div class="col-md-2 col-md-offset-1">
                            <?php echo $form->field($modelAyat, 'tarifrp', [
                                'addon' => [
                                    'append' => [
                                        'content' => '%'
                                    ]
                                ]
                            ])->textInput([
                                'id' => 'TAyat_persen_0', 
                                'class' => 'kdrek',
                                "style" => "text-align:right;", 
                                "readOnly" => true
                            ])->label('Tarif Persen'); ?> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <?php echo $form->field($modelDataMblb, 'jml_pengenaan', [
                                'addon' => [    
                                    'prepend' => [
                                        'content' => 'Rp.'
                                    ],
                                    'append' => [
                                        'content' => ',00'
                                    ]
                                ]
                            ])->textInput([
                                'id' => 'TDataSelfMblb_jml_pengenaan_0',
                                "name" => "TDataSelfMblb[jml_pengenaan][]",  
                                "style" => "text-align:right;", 
                                "readOnly" => true,
                                "class" => 'kdrek'
                            ])->label('Dasar Pengenaan'); ?> 
                        </div>
                        <div class="col-md-3 col-md-offset-1">
                            <?php echo $form->field($modelDataMblb, 'jml_bayar', [
                                'addon' => [    
                                    'prepend' => [
                                        'content' => 'Rp.'
                                    ],
                                    'append' => [
                                        'content' => ',00'
                                    ]
                                ]
                            ])->textInput([
                                'id' => 'TDataSelfMblb_jml_bayar_0', 
                                'name' => 'TDataSelfMblb[jml_bayar][]', 
                                "style" => "text-align:right;", 
                                "readOnly" => true,
                                'class' => 'kdrek'
                            ])->label('Jumlah Bayar'); ?> 
                        </div>
                    </div>
                </div>
            </div>
            <div id="reklames" class="span-reklame">

            </div>
            <div class="span-reklame col-md-12">
                <?= Html::button(
                    '<i class="fa fa-plus" aria-hidden="true"></i> Tambah Pajak', 
                    [
                        'id' => 'btn-add-rek',
                        'class' => 'btn btn-success'
                    ]) 
                ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <hr id="line-reklame" class="span-reklame" style="width: 100%;"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($modelData, 'keterangan')->textArea(['row' => 3]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?= $form->field($modelDaftar, 'npwpd')->textInput(['readOnly' => true]) ?>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <?php echo $form->field($modelDaftar, 'nm_wp')->textInput(['readOnly' => true]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($modelDaftar, 'alm_wp')->textArea(['readOnly' => true, 'row' => 3]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kel_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kec_wp')->textInput(['readOnly' => true]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-md-12">
            <div class="col-md-3 hide">
                        <?php
                        echo $form->field($modelData, 'nilai', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ],
                                'append' => [
                                    'content' => ',00',
                                ]
                            ]
                        ])->textInput([
                            'style' => 'text-align:right;',
                            'readOnly' => true
                        ]);
                        ?>
                    </div>
                    <div class="col-md-3">
                        <?php
                        echo $form->field($modelData, 'jml_pjk', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ],
                                'append' => [
                                    'content' => ',00',
                                ]
                            ]
                        ])->textInput([
                            'style' => 'text-align:right;',
                            'readOnly' => true
                        ]);
                        ?>
                    </div>
                    </div>
                    </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="box-shadow: 0 0 31px 0 rgba(0, 0, 0, 0.3) inset; overflow:hidden;" class="alert">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?=
                            $form->field($modelData, 'upload_file')->label(false)->widget(FileInput::classname(), [
                                'name' => 'file',
                                'options' => ['accept' => '.xls, .xlsx, .pdf, .zip, .rar'],
                                'pluginOptions' => [
                                    'browseClass' => 'btn btn-success',
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'removeClass' => 'btn btn-danger',
                                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> '
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?= Html::button(' Simpan', ['id' => 'btn_simpan', 'class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    <?php
    ActiveForm::end();
    ?>
$url_cetak = Url::to(['pendataan/cetak']);
<?php
$js = <<<JS

jQuery(document).ready(function(){
    var current_id = 1; // langsung set 1 karena harus ada minimal 1 pajak MBLB
    $.ajax({
        url: "get-kode-rekening-search",
        data: {th_spt: $('#tdataself-th_spt').val()},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (parseInt(data) !== 1) {
                $("#TDataSelfMblb_id_ayt_0").select2({
                    placeholder: 'Pilih Kode Rekening',
                    data: data,
                    width: 500,
                });
            }
        }
    });
    
    $('#tdataself-th_spt').change(function () {
        $.ajax({
            url: "no-res",
            data: 'tahun=' + $('#tdataself-th_spt').val(),
            type: 'POST',
            // dataType: 'json',
            success: function (data) {
                $('#tdataself-no_form').val(data + '.' + ((new Date()).getMonth() + 1).toString() + '.' + ((new Date()).getFullYear()).toString().substr(2,2));
                // console.log(data);
                $('#tdataself-no_spt').val(data + '/RES/' + $('#tdataself-th_spt').val().substr(2,2));
            }
        });
        if (parseInt($(this).val()) > parseInt(new Date().getFullYear()) || parseInt($(this).val()) < parseInt(new Date().getFullYear()) - 10) {
            $(this).val('');
        }
    });

    $('#tdaftar-no_pokok').change(function(){
        $(this).val(padLeft($(this).val(), 6));
        $('#alert-no_pokok').hide();
        $.ajax({
            url: "getwp",
            data: 'no_daftar=' + $(this).val(),
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                if (parseInt(data) === 1) {
                    $('#tdaftar-npwpd').val('');
                    $('#tdaftar-kel_wp').val('');
                    $('#tdaftar-kec_wp').val('');
                    $('#tdaftar-nm_wp').val('');
                    $('#tdaftar-alm_wp').val('');  
                    if($('#tdaftar-no_pokok').val() != '000000'){
                        $('#alert-no_pokok').show();
                    }
                } else {
                    $('#tdaftar-npwpd').val(data.npwpd);
                    $('#tdaftar-kel_wp').val(data.kelurahan);
                    $('#tdaftar-kec_wp').val(data.kecamatan);
                    $('#tdaftar-nm_wp').val(data.nm_wp);
                    $('#tdaftar-alm_wp').val(data.alm_wp);
                }
            }
        });
        dapatkanNoRegister();
    }).trigger('change');

    $('.button_wp').on('click', function () {
        $('#modal-pop').modal('show');
        var modal = $('#modal-pop');
        modal.find('.modal-body').html('');
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
        $.ajax({
            url: "list-wp",
            //data: {"tahun": $('#tdata-th_spt').val(), "kd_ayt": '$kode_rekening'},
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        }); 
    });

    $('.button_rek').on('click', function () {
        $('#modal-pop').modal('show');
        var modal = $('#modal-pop');
        modal.find('.modal-body').html('');
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
        $.ajax({
            url: "listrekening",
            data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": '$kode_rekening'},
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
        dapatkanNoRegister();
    });


    $("#tdataself-tgl_awal").change(function () {
        var parts = $(this).val().split('-');
        var now = new Date(parts[2], parts[1] - 1, parts[0]);
        var lastDayOfTheMonth = new Date(1900 + now.getYear(), now.getMonth() + 1, 0);
        if ($(this).val() != '') {
            $("#tdataself-tgl_akhir").val($.datepicker.formatDate('dd-mm-yy', lastDayOfTheMonth));
        }
        dapatkanNoRegister();
    });

    $("#tdataself-nilai").maskMoney({thousands: '.', decimal: ',', precision: '0'});

    $('#tdataself-th_spt').change(function(){
        dapatkanNoRegister();
        $.ajax({
            url: "get-no-data",
            data: {
                tahun:$(this).val()
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                $("#tdataself-no_data").val(data);
            }
        });
    });
    
    $("#tdataself-nilai").change(function () {
        var omset = $("#tdataself-nilai").val().replace(/\./g, "");
        var tarif = $("#tayat-tarifpr").val();
        if (tarif == '') {
            tarif=0;
        }
        if (omset != '' && !isNaN(omset)){
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100))));
        }
    });

    $('#btn_simpan').click(function () {
        bootbox.dialog({
            message: "Apa Anda yakin ingin memproses data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Ya",
                    className: "btn-success",
                    callback: function () {
                        $("#spinner").fadeIn("slow");
                        var dataform = new FormData($("#form-pendataan-hotel")[0]);
                        $.ajax({
                            type: "POST",
                            url: "save",
                            data: dataform,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                $("#spinner").fadeOut("slow");
                                if (parseInt(data) === 1) {
                                    bootbox.dialog({
                                        message: "Proses data berhasil.",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                                callback: function () {
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                }
                                            },
                                            danger: {
                                                label: "Cetak SPTPD",
                                                className: "btn-info",
                                                callback: function () {
                                                    window.open('$url_cetak' + '?no_data_self=' + data[1] + '&tahun=' + data[0], '_blank');
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    bootbox.dialog({
                                        message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "Tidak",
                    className: "btn-warning",
                    callback: function () {

                    }
                }
            }
        });
    });

    $('#btn-add-rek').click(function () {
        var x = nextElement($('#pribadi-badan'));
        $('#pribadi-badan_' + x).fadeIn('slow');
        $('#pribadi-badan_' + x).find('#TDataReklame_jml_rek').focus();
        $('#TDataSelfMblb_kode_rekening_' + x).val('1106');

        current_id = current_id + 1;
    });

    $(document).on('click', "button.remove-mblb", function (e) {
        e.preventDefault();
        console.clear();
        console.log($(this).parent().parent().attr('id'));
        if ($(this).parent().parent().attr('id') != 'pribadi-badan') {
            $(this).parent().parent().remove();
        }
    });

    $(document).on('change', "input.kdrek", function (e) {
        e.preventDefault();
        var id = e.currentTarget.id;
        var count = id.replace(/\D*/, '');

        $.ajax({
            url: "get-detail-ayat",
            data: {id_ayt: $('#TDataSelfMblb_id_ayt_' + count).val()},
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                if(data !== 1){
                    console.log(data);
                    $('#TAyat_rupiah_' + count).val(parseFloat(data.tarifrp).numberFormat(0, ',', '.'));
                    $('#TAyat_persen_' + count).val(data.tarifpr * 100);
                    $('#TAyat_nama_' + count).val(data.nm_ayt);

                    if (data.jn_ayt == '23' && data.kl_ayt == '08') {
                        $("#satuan_" + count).html('Ton');
                    } else {
                        $("#satuan_" + count).html('m<sup>3</sup>');
                    }
                }
                
            }
        });
        hitung_mblb(id, count);
    });

    Number.prototype.numberFormat = function (decimals, dec_point, thousands_sep) {
        dec_point = typeof dec_point !== 'undefined' ? dec_point : ',';
        thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : '.';

        var parts = this.toFixed(decimals).split(',');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);

        return parts.join(dec_point);
    };

    $('.span-reklame').on('change', 'input', function (e) {
        var id = e.currentTarget.id;
        var count = id.replace(/\D*/, '');
        console.log('count ' + count);
        hitung_mblb(id, count);
    });

    function hitung_mblb(id, count) {
        console.clear();
        var classHapus = $('button.remove-mblb');
        var id = '#' + id;
        var idDiv = $(id).parent().parent().parent().parent().parent().attr('id');
        if (typeof(idDiv) === 'undefined') {
            idDiv = $(id).parent().parent().parent().parent().parent().parent().attr('id');
        }
        console.log(idDiv);
        idDiv = $('#' + idDiv);

        var jumlah = (idDiv.find('#TDataSelfMblb_volume_' + count).val() === '' || idDiv.find('#TDataSelfMblb_volume_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataSelfMblb_volume_' + count).val().replace(',', '.')));
        var tarifpr = (idDiv.find('#TAyat_persen_' + count).val() === '' || idDiv.find('#TAyat_persen_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TAyat_persen_' + count).val().replace(',', '.')));
        var tarifrp = (idDiv.find('#TAyat_rupiah_' + count).val() === '' || idDiv.find('#TAyat_rupiah_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TAyat_rupiah_' + count).val().replace(/\./g, '')));

        var pajak_keseluruhan = 0, nilai_keseluruhan = 0, pajak_mblb = 0, nilai_mblb = 0, pajak, nilai;

        pajak = jumlah * tarifrp * (tarifpr / 100);
        nilai = jumlah * tarifrp;

        if (typeof(pajak) === 'undefined' || isNaN(pajak))
            pajak = 0;
        idDiv.find('#TDataSelfMblb_jml_bayar_' + count).val(numerik(Math.ceil(pajak.toFixed(2))));

        if (typeof(nilai) === 'undefined' || isNaN(nilai))
            nilai = 0;
        // idDiv.find('#TDataSelfMblb_jml_pengenaan_' + count).val(numerik(Math.ceil(nilai.toFixed(2))));
        idDiv.find('#TDataSelfMblb_jml_pengenaan_' + count).val(numerik(Math.ceil(nilai.toFixed(2))));

        $.each(classHapus, function (index) {
            idParent = $(this).parent().parent().attr('id');
            countParent = idParent.replace(/\D*/, '') === '' || idParent.replace(/\D*/, '') === null ? 0 : idParent.replace(/\D*/, '');
            pajak_mblb = pajak_mblb + parseFloat($(document).find('#TDataSelfMblb_jml_bayar_' + countParent).val().replace(/\./g, ''));
            // nilai_mblb = nilai_mblb + parseFloat($(document).find('#TDataSelfMblb_jml_pengenaan_' + countParent).val().replace(/\./g, ''));
            nilai_mblb = nilai_mblb + parseFloat($(document).find('#TDataSelfMblb_jml_pengenaan_' + countParent).val().replace(/\./g, ''));
            console.log('pajak mblb ' + pajak_mblb);
            console.log('nilai mblb ' + nilai_mblb);
        });
        console.log('total pajak ' + pajak_mblb);
        console.log('total nilai ' + nilai_mblb);
        if (typeof(pajak_mblb) === 'undefined')
            pajak_mblb = 0;
        if (typeof(nilai_mblb) === 'undefined')
            nilai_mblb = 0;
        pajak_keseluruhan = pajak_mblb;
        nilai_keseluruhan = nilai_mblb;
        if (typeof(pajak_keseluruhan) === 'undefined')
            pajak_keseluruhan = 0;
        if (isNaN(pajak_keseluruhan))
            pajak_keseluruhan = 0;
        if (typeof(nilai_keseluruhan) === 'undefined')
            nilai_keseluruhan = 0;
        if (isNaN(nilai_keseluruhan))
            nilai_keseluruhan = 0;
        console.log('pajak_keseluruhan ' + pajak_keseluruhan);
        console.log('nilai_keseluruhan ' + nilai_keseluruhan);
        $('#tdataself-jml_pjk').val(numerik(pajak_keseluruhan));
        $('#tdataself-nilai').val(numerik(nilai_keseluruhan));

        console.log('jumlah: ' + jumlah);
        console.log('pajak: ' + pajak);
        console.log('tarifpr: ' + tarifpr);
        console.log('tarifrp: ' + tarifrp);
    }

    function nextElement(element) {
        var newElement = element.clone();
        var id = current_id;
        id_span_select2 = newElement.find('#select2-TDataSelfMblb_id_ayt_0-container');
        id_span_select2.parent().parent().parent().remove();

        $.ajax({
            url: "get-kode-rekening-search",
            data: {th_spt: $('#tdataself-th_spt').val()},
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                if (parseInt(data) !== 1) {
                    $("#TDataSelfMblb_id_ayt_" + id).select2({
                        // minimumInputLength: 2,
                        placeholder: 'Pilih Kode Rekening',
                        data: data,
                        width: 500,
                    });
                }
            }
        });
        newElement.attr("id", element.attr("id").split("_")[0] + "_" + id);
        var input = newElement.find('input');
        var select = newElement.find('select');
        var span = newElement.find('.satuan');
        $.each(span, function () {
            $(this).attr("id", $(this).attr("id").replace(/\d*/g, '') + id);
        });
        $.each(input, function () {
            $(this).attr("id", $(this).attr("id").replace(/\d*/g, '') + id);
        });
        $.each(select, function () {
            $(this).attr("id", $(this).attr("id").replace(/\d*/g, '') + id);
        });
        newElement.find('input, select').val(''); //clear all values
        newElement.appendTo($("#reklames"));
        return id;
    }

    function dapatkanNoRegister() {
        var th_spt = $('#tdataself-th_spt').val();
        var kd_ayt = '$kode_rekening';
        var status = 1;
        var tgl_awal = $("#tdataself-tgl_awal").val();
        var no_pokok = $("#tdaftar-no_pokok").val();
        var npwpd = $('#tdaftar-npwpd').val();
        $.ajax({
            url: "getregister",
            data: {
                "tahun": th_spt, 
                "kd_ayt": kd_ayt, 
                "status": status, 
                "tgl_awal": tgl_awal, 
                "no_pokok": no_pokok, 
                "npwpd": npwpd
                },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $("#tdataself-nmr_bayar").val(data);
            }
        });
    };

});

JS;

$this->registerJs(
    $js,
    View::POS_END,
    'js-page'
);

?>