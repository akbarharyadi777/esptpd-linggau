<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_data_reklame".
 *
 * @property integer $id_data_reklame
 * @property integer $id_ayt
 * @property integer $jml_rek
 * @property integer $hari
 * @property double $panjang
 * @property double $lebar
 * @property double $luas
 * @property double $tinggi
 * @property double $lokasi
 * @property double $sdt_pandang
 * @property double $p_rokok
 * @property integer $no_data
 * @property string $th_spt
 * @property double $nilai_media
 * @property double $nilai_strategis
 * @property double $nilai_sewa
 * @property double $pajak
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TDataReklame extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_data_reklame';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_ayt', 'jml_rek', 'panjang', 'lebar', 'luas', 'tinggi', 'lokasi', 'sdt_pandang', 'no_data', 'th_spt', 'nilai_media', 'nilai_strategis', 'nilai_sewa', 'pajak'], 'required'],
            [['id_ayt', 'jml_rek', 'hari', 'no_data', 'createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['th_spt'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_data_reklame' => 'Id Data Reklame',
            'id_ayt' => 'Id Ayt',
            'jml_rek' => 'Jumlah Rekening',
            'hari' => 'Jumlah Hari',
            'panjang' => 'Panjang',
            'lebar' => 'Lebar',
            'luas' => 'Luas',
            'tinggi' => 'Tinggi',
            'lokasi' => 'Lokasi',
            'sdt_pandang' => 'Sudut Pandang',
            'p_rokok' => 'P Rokok',
            'no_data' => 'No Data',
            'th_spt' => 'Th Spt',
            'nilai_media' => 'Nilai Media',
            'nilai_strategis' => 'Nilai Strategis',
            'nilai_sewa' => 'Nilai Sewa',
            'pajak' => 'Pajak',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
