<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_data_burung_walet".
 *
 * @property integer $id
 * @property integer $no_data
 * @property string $th_spt
 * @property integer $id_ayt
 * @property double $volume
 * @property double $jumlah
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 *
 * @property PadTData $noData
 */
class TDataBurungWalet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_data_burung_walet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_data', 'th_spt', 'id_ayt'], 'required'],
            [['no_data', 'id_ayt', 'createdby', 'updatedby'], 'integer'],
            [['volume', 'jumlah'], 'number'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['th_spt'], 'string', 'max' => 4],
            [['no_data', 'th_spt'], 'exist', 'skipOnError' => true, 'targetClass' => TData::className(), 'targetAttribute' => ['no_data' => 'no_data', 'th_spt' => 'th_spt']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_data' => 'No Data',
            'th_spt' => 'Th Spt',
            'id_ayt' => 'Id Ayt',
            'volume' => 'Volume',
            'jumlah' => 'Jumlah',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoData()
    {
        return $this->hasOne(TData::className(), ['no_data' => 'no_data', 'th_spt' => 'th_spt']);
    }
}
