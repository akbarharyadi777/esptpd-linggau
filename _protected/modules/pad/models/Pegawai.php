<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pegawai".
 *
 * @property string $nip
 * @property string $nm_pegawai
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property string $tmpt_lahir
 * @property string $tgl_lahir
 * @property integer $id_gol_pangkat
 *
 * @property Users $createdby0
 * @property Users $updatedby0
 * @property Users[] $users
 */
class Pegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wewenang.pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip'], 'required'],
            [['createdby', 'updatedby', 'id_gol_pangkat'], 'integer'],
            [['createdtime', 'updatedtime', 'tgl_lahir'], 'safe'],
            [['nip'], 'string', 'max' => 18],
            [['nm_pegawai'], 'string', 'max' => 100],
            [['tmpt_lahir'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nip' => 'Nip',
            'nm_pegawai' => 'Nm Pegawai',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'tmpt_lahir' => 'Tmpt Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'id_gol_pangkat' => 'Id Gol Pangkat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(Users::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedby0()
    {
        return $this->hasOne(Users::className(), ['id' => 'updatedby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['nip' => 'nip']);
    }
}
