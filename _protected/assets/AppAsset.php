<?php

/**
 * -----------------------------------------------------------------------------
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 * -----------------------------------------------------------------------------
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

// set @themes alias so we do not have to update baseUrl every time we change themes
Yii::setAlias('@themes', Yii::$app->view->theme->baseUrl);

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * 
 * @since 2.0
 *
 * Customized by Nenad Živković
 */
class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@themes';
    public $css = [
        'css/_all-skins.css',
        'css/AdminLTE.css',
        'css/font-awesome.css',
        'css/select2.min.css'
            //'css/login.css',
            //'css/bootstrap.min.css',
    ];
    public $js = [
        //'js/jQuery-2.1.3.min.js',
        'js/jquery-ui.js',
        //'js/bootstrap.min.js',
        'js/morris.min.js',
        'js/money.js',
        'js/jquery.sparkline.min.js',
        'js/jquery-jvectormap-1.2.2.min.js',
        'js/jquery-jvectormap-world-mill-en.js',
        'js/jquery.knob.js',
        'js/moment.js',
        'js/bootstrap-datepicker.js',
        'js/bootstrap3-wysihtml5.all.min.js',
        'js/icheck.min.js',
        'js/jquery.slimscroll.min.js',
        'js/fastclick.min.js',
        'js/app.min.js',
        //'js/demo.js',
        'js/jsutama.js',
        'js/main.js',
        'js/bootbox.min.js',
        'js/select2.full.min.js'
        //'pdf/src/pdf.js'
            // 'js/jsRekamPendataan.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
