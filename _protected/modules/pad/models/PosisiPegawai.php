<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "wewenang.posisi_pegawai".
 *
 * @property string $kd_kanwil
 * @property string $kd_kantor
 * @property string $nip
 * @property string $kd_seksi
 * @property string $tgl_awal_berlaku
 * @property string $tgl_akhir_berlaku
 * @property string $kd_wewenang
 * @property string $kd_jabatan
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property string $kd_bidang
 *
 * @property WewenangUsers $createdby0
 * @property WewenangUsers $updatedby0
 */
class PosisiPegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wewenang.posisi_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kanwil', 'kd_kantor', 'nip', 'kd_jabatan'], 'required'],
            [['tgl_awal_berlaku', 'tgl_akhir_berlaku', 'createdtime', 'updatedtime'], 'safe'],
            [['createdby', 'updatedby'], 'integer'],
            [['kd_kanwil', 'kd_kantor', 'kd_seksi', 'kd_wewenang', 'kd_jabatan', 'kd_bidang'], 'string', 'max' => 2],
            [['nip'], 'string', 'max' => 18],
            [['createdby'], 'exist', 'skipOnError' => true, 'targetClass' => WewenangUsers::className(), 'targetAttribute' => ['createdby' => 'id']],
            [['updatedby'], 'exist', 'skipOnError' => true, 'targetClass' => WewenangUsers::className(), 'targetAttribute' => ['updatedby' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_kanwil' => 'Kd Kanwil',
            'kd_kantor' => 'Kd Kantor',
            'nip' => 'Nip',
            'kd_seksi' => 'Kd Seksi',
            'tgl_awal_berlaku' => 'Tgl Awal Berlaku',
            'tgl_akhir_berlaku' => 'Tgl Akhir Berlaku',
            'kd_wewenang' => 'Kd Wewenang',
            'kd_jabatan' => 'Kd Jabatan',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'kd_bidang' => 'Kd Bidang',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedby0()
    {
        return $this->hasOne(WewenangUsers::className(), ['id' => 'createdby']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedby0()
    {
        return $this->hasOne(WewenangUsers::className(), ['id' => 'updatedby']);
    }
}
