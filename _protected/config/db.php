<?php

$databaseName = 'padlinggau';
$userName = 'postgres';
$password = 'password';
$serverAddress = '127.0.0.1';
$databaseAddress = 'localhost';
$port = 5432;

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host='.$databaseAddress.';dbname='.$databaseName.';port='.$port,
    'username' => $userName,
    'password' => $password,
    'charset' => 'utf8',
    'schemaMap' => [
        'pgsql' => [
            'class' => 'yii\db\pgsql\Schema',
            'defaultSchema' => 'public',
        ],
    ],
];
