CREATE TABLE pad.t_daftar_file
(
    id serial NOT NULL,
    nama_file character varying(200) NOT NULL,
    file character varying(200) NOT NULL,
    npwpd character varying(14) NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE pad.t_daftar_file
    OWNER to postgres;
COMMENT ON TABLE pad.t_daftar_file
    IS 'Untuk menyimpan hasil upload file dari pendaftaran';