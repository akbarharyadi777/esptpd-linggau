<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pad\models\TDataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tdatas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tdata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tdata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'th_spt',
            'no_data',
            'id_spt',
            'id_tetap',
            'kode',
            // 'st_spt',
            // 'id_ayt',
            // 'nilai',
            // 'jml_pr',
            // 'st',
            // 'id_dt1',
            // 'id_dt2',
            // 'jml_pok',
            // 'jumlah',
            // 'banyak',
            // 'judul',
            // 'lokasi',
            // 'hari',
            // 'muka',
            // 'panjang',
            // 'lebar',
            // 'tinggi',
            // 'luas',
            // 'kenaikan',
            // 'sdt_pandang',
            // 'bunga',
            // 'peremajaan',
            // 'createdby',
            // 'createdtime',
            // 'updatedby',
            // 'updatedtime',
            // 'nmr_bayar',
            // 'flag',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
