/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

yii.confirm = function (message, ok, cancel) {

    bootbox.confirm(
            {
                message: message,
                buttons: {
                    confirm: {
                        label: "Ya",
                        className: "btn-success",
                    },
                    cancel: {
                        label: "Batal",
                        className: "btn-warning",
                    }
                },
                callback: function (confirmed) {
                    if (confirmed) {
                        !ok || ok();
                    } else {
                        !cancel || cancel();
                    }
                }
            }
    );
    // confirm will always return false on the first call
    // to cancel click handler
    //return false;
}

