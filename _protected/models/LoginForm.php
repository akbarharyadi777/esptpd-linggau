<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\modules\pad\models\TDaftar;
use app\modules\management\models\User;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $rememberMe = false;
    public $status; // holds the information about user status

    /**
     * @var \app\models\User
     */
    private $_user = false;

    /**
     * Returns the validation rules for attributes.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'email'],
            ['password', 'validatePassword'],
            ['rememberMe', 'boolean'],
            // username and password are required on default scenario
            [['username', 'password'], 'required', 'on' => 'default'],
            // email and password are required on 'lwe' (login with email) scenario
            [['email', 'password'], 'required', 'on' => 'lwe'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array  $params    the additional name-value pairs
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                // if scenario is 'lwe' we use email, otherwise we use username
                $field = ($this->scenario === 'lwe') ? 'email' : 'username';

                $this->addError($attribute, $field.' or password yang Anda masukan salah.');
            }
        }
    }

    /**
     * Returns the attribute labels.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'email' => Yii::t('app', 'Email'),
            'rememberMe' => Yii::t('app', 'Remember me'),
        ];
    }

    /**
     * Logs in a user using the provided username|email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        $pre_fix = array('P1', 'P2', 'R1', 'R2');
        $val_pre_fix = substr($this->username, 0, 2);
        if (in_array($val_pre_fix, $pre_fix)) {
            $daftar = TDaftar::find()->where(['npwpd' => $this->username])->one();
            $model = User::find()->where(['username' => $this->username])->one();

            if (empty($daftar)) {
                return false;
            }

            if (empty($model)) {
                $model = new User(['scenario' => 'newUser']);
                $model->username = $daftar->npwpd;
                $model->password_hash = 'password';
                $model->password = 'password';
                $model->repeat_password = 'password';
                if ($model->save()) {
                    \Yii::$app->db->createCommand("insert into public.auth_assignment values ('User', ".$model->id.', '.time().')')
                        ->execute();
                } else {
                    print_r(\kartik\widgets\ActiveForm::validate($model));
                }
            }
            if ($daftar->active_inactive == '1') {
                return false;
            }
        }

        if ($this->validate()) {
            $user = $this->getUser();
            // get user status if user exists, otherwise assign not active as default
            $this->status = ($user) ? $user->status : User::STATUS_NOT_ACTIVE;

            // if we have active and valid user log him in
            if ($this->status === User::STATUS_ACTIVE) {
                return Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0);
            } else {
                return false; // user is not active
            }
        } else {
            return false;
        }
    }

    /**
     * Finds user by username or email in 'lwe' scenario.
     *
     * @return User|null|static
     */
    public function getUser()
    {
        if ($this->_user === false) {
            // in 'lwe' scenario we find user by email, otherwise by username
            if ($this->scenario === 'lwe') {
                $this->_user = User::findByEmail($this->email);
            } else {
                $this->_user = User::findByUsername($this->username);
            }
        }

        return $this->_user;
    }
}
