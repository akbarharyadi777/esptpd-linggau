<?php

namespace app\modules\pad\controllers;

use app\controllers\AppController;
use app\modules\pad\models\TData;
use app\modules\pad\models\TAyat;
use app\modules\pad\models\TTetap;
use app\modules\pad\models\TDaftar;
use Yii;
use yii\web\UploadedFile;

class BuktiPembayaranController extends AppController {
	
	public function actionIndex(){
		$modelData = new TData();
		$modelTetap = new TTetap();
		// var_dump($_POST['TData']['nmr_bayar']);die();	
    if (Yii::$app->request->post()){
      $data = TData::find()->where("st_bukti_bayar != '1' and nmr_bayar='" . $_POST['TData']['nmr_bayar'] . "'")->one();
      if (empty($data)){
				Yii::$app->session->setFlash('bukti_pembayaran', "Nomor bayar tidak ditemukan atau sudah melakukan upload pembayaran.");
      } else {
				$tetap = TTetap::find()->where('id_tetap=' . $data->id_tetap)->one();
				if($data->st != '2'){
					Yii::$app->session->setFlash('bukti_pembayaran', "Belum ditetapkan.");
				} elseif($tetap->st_persetujuan != '1') {
					Yii::$app->session->setFlash('bukti_pembayaran', "Belum disetujui.");
				} elseif($tetap->st_pengesahan != '1') {
					Yii::$app->session->setFlash('bukti_pembayaran', "Belum disahkan.");
				} else {
					$files = UploadedFile::getInstance($data, 'upload_bukti_bayar');
					if ($files != null) {
						$data->upload_bukti_bayar = 'BK-' . $data->no_data . '-' . $data->th_spt . '.' . $files->extension;
					} else {
						$data->upload_bukti_bayar = null;
					}
					$data->st_bukti_bayar = '1';
					if($data->save(false)){
						if ($files != null) {
							if (!file_exists('uploads/')) {
								mkdir('uploads/', 0777, true);
							}
								$files->saveAs('uploads/' . $data->upload_bukti_bayar);
							}
							Yii::$app->session->setFlash('bukti_pembayaran_berhasil', "Berhasil upload bukti pembayaran.");
					} else {
						Yii::$app->session->setFlash('bukti_pembayaran', "Gagal upload bukti pembayaran.");
					}
				}
			}
			return $this->redirect('index');
    }
		return $this->render('index', [
		      'modelData' => $modelData,
		      'modelTetap' => $modelTetap,
		    ]);
	}
	
}
