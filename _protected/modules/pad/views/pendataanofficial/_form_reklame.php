<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;

use kartik\select2\Select2Asset;
use yii\bootstrap\Modal;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;
use app\modules\pad\models\Pegawai;
use app\modules\pad\models\TKet;
use kartik\widgets\Select2;


/* @var $this View */
$this->title = 'Pendataan Pajak Reklame Official Assessment';

Select2Asset::register($this);

?>
<link href="/themes/e-sptpd/css/select2.min.css" rel="stylesheet" />
<style type="text/css">
  .select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    border: 1px solid #aaa;
    border-radius: 4px;
    display: block;
    height: 28px;
    user-select: none;
    -webkit-user-select: none;
  }

</style>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'form-pendataan-reklame',
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-2 col-md-pull-0">
                  <label for="exampleInputEmail1">Tetap/Isidentil</label>
                  <div class="controls">
                      <?php
                      $z = array(1 => 'Tetap', 2 => 'isidentil');
                      echo $form->field($modelAyat, 'status')->dropDownList($z, ['prompt' => '', 'value' => '0'])->label(false);
                      ?>
                  </div>
                </div>
                <div class="col-md-2 col-md-offset-2">
                    <?php
                    if (!$model->isNewRecord) {
                        $modelSpt->tg_data = date('d-m-Y', strtotime($modelSpt->tg_data));
                    }
                    echo $form->field($modelSpt, 'tg_data')->widget(datecontrol::classname(), [
                        'type' => DateControl::FORMAT_DATE,
                        'ajaxConversion' => false,
                        'widgetOptions' => [
                                'layout' => '{picker}{input}',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => ''
                                ]
                            ]
                    ]);
                    ?>
                </div>
                <div class="col-md-2 col-md-offset-2">
                    <?=
                    $form->field($model, 'th_spt')->widget(MaskedInput::ClassName(), [
                        //'name' => 'input-3',
                        'mask' => '9',
                        'clientOptions' => ['repeat' => 4, 'greedy' => false]
                    ]);
                    ?>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <div class="col-md-2 col-md-offset-0">
                        <?php
                        echo $form->field($modelDaftar, 'no_pokok')->widget(MaskedInput::ClassName(), [
                            'name' => 'input-3',
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 7, 'greedy' => false]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-2 col-md-offset-2">
                        <?= 
                        $form->field($model, 'st_spt')
                            ->dropDownList(
                                    ArrayHelper::map(TKet::find()->where(['kd_spt' => $model->st_spt])->asArray()->all(), 'kd_spt', 'singk')
                                    )
                        ?>    
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <form role="form">
                            <label for="exampleInputEmail1">Periode</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo DatePicker::widget([
                                        'model' => $modelSpt,
                                        'attribute' => 'tgl_awal',
                                        'attribute2' => 'tgl_akhir',
                                        'options' => ['placeholder' => 'tanggal awal'],
                                        'options2' => ['placeholder' => 'tanggal akhir'],
                                        'type' => DatePicker::TYPE_RANGE,
                                        'form' => $form,
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy',
                                            'autoclose' => true,
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <hr />
            <div id="detail-reklame" class="span-reklame">
              <div class="span11" style="text-align: right;">
                <a id="remove" class="remove btn btn-default">
                  <i class="fa fa-remove"></i>
                </a>
              </div>
              
              <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4 col-md-pull-0">
                      <label>Kode Rekening</label>
                      <input name="TDataReklame[id_ayt]" id="TDataReklame_id_ayt_0" class="span-6 kdrek form-control">
                    </div>
                  </div>
              </div>
              <br>
              <div class="row">
                  <div class="col-md-12">

                      <div class="col-md-2">
                          <?php
                          echo $form->field($modelReklame, 'jml_rek')->textInput(['id' => 'TDataReklame_jml_rek_0', 'name' => 'TDataReklame[jml_rek][]', 'size' => 7, 'maxlength' => 5, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                          ?>
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                          <?php
                          echo $form->field($modelReklame, 'hari')->textInput(['id' => 'TDataReklame_hari_0', 'name' => 'TDataReklame[hari][]', 'size' => 7, 'maxlength' => 5, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                          ?>
                      </div>

                  </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="col-md-2">
                      <?php
                      echo $form->field($modelReklame, 'panjang', ['addon' => [
                              'append' => [
                                  'content' => 'm',
                              ]
                          ]
                      ])->textInput(['id' => 'TDataReklame_panjang_0', 'name' => 'TDataReklame[panjang][]', 'size' => 7, 'maxlength' => 5, 'style' => 'text-align:right;', "onkeyup" => "this.value=this.value.replace(/[^0-9.,]/g,'')"]);
                      ?>
                  </div>
                  <div class="col-md-2 col-md-offset-2">
                      <?php
                      echo $form->field($modelReklame, 'lebar', ['addon' => [
                              'append' => [
                                  'content' => 'm',
                              ]
                          ]
                      ])->textInput(['id' => 'TDataReklame_lebar_0', 'name' => 'TDataReklame[lebar][]', 'size' => 7, 'maxlength' => 5, 'style' => 'text-align:right;', "onkeyup" => "this.value=this.value.replace(/[^0-9.,]/g,'')"]);
                      ?>
                  </div>
                  <div class="col-md-2 col-md-offset-2">
                      <?php
                      echo $form->field($modelReklame, 'luas', ['addon' => [
                              'append' => [
                                  'content' => 'm<sup>2</sup>',
                              ]
                          ]
                      ])->textInput(['id' => 'TDataReklame_luas_0', 'name' => 'TDataReklame[luas][]', 'size' => 7, 'maxlength' => 5, 'readOnly' => true, 'style' => 'text-align:right;', "onkeyup" => "this.value=this.value.replace(/[^0-9.,]/g,'')"]);
                      ?>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-md-12">

                      <div class="col-md-2">
                          <?php
                            $x = array(
                                10 => 'Kawasan 1',
                                8 => 'Kawasan 2',
                                6 => 'Kawasan 3',
                                4 => 'Kawasan 4',
                                2 => 'Kawasan 5'
                            );
                            echo $form->field($modelReklame, 'lokasi')->dropDownList($x, ['prompt' => 'Pilih Kawasan', 'id' => 'TDataReklame_lokasi_0', 'name' => 'TDataReklame[lokasi][]']);
                          ?>
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                          <?php
                            $c = array(
                                10 => '> dari 4 arah',
                                8 => '4 arah',
                                6 => '3 arah',
                                4 => '2 arah',
                                2 => '1 arah'
                            );
                            echo $form->field($modelReklame, 'sdt_pandang')->dropDownList($c, ['prompt' => 'Pilih Sudut Pandang', 'id' => 'TDataReklame_sdt_pandang_0', 'name' => 'TDataReklame[sdt_pandang][]']);
                          ?>
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                          <?php
                          echo $form->field($modelReklame, 'tinggi', ['addon' => [
                                  'append' => [
                                      'content' => 'm',
                                  ]
                              ]
                          ])->textInput(['id' => 'TDataReklame_tinggi_0', 'name' => 'TDataReklame[tinggi][]', 'size' => 7, 'maxlength' => 5, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                          ?>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">
                      <div class="col-md-2">
                        <?php echo $form->field($modelAyat, 'tarifrp')->hiddenInput(['id' => 'TAyat_tarifrp_0', 'size' => 7, 'maxlength' => 15, "readOnly" => true])->label(false); ?>
                        <?php echo $form->field($modelAyat, 'tarifpr')->hiddenInput(['id' => 'TAyat_tarifpr_0', 'size' => 7, 'maxlength' => 15, "readOnly" => true])->label(false); ?>
                        <?php
                          echo $form->field($modelReklame, 'nilai_media', ['addon' => [
                                  'prepend' => [
                                      'content' => 'Rp.',
                                  ]
                              ]
                          ])->textInput(['id' => 'TDataReklame_nilai_media_0', 'name' => 'TDataReklame[nilai_media][]', 'size' => 7, 'maxlength' => 15, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                          ?>
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                        <?php
                        echo $form->field($modelReklame, 'nilai_strategis', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ]
                            ]
                        ])->textInput(['id' => 'TDataReklame_nilai_strategis_0', 'name' => 'TDataReklame[nilai_strategis][]', 'size' => 7, 'maxlength' => 15, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                        ?>
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                        <?php
                        echo $form->field($modelReklame, 'nilai_sewa', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ]
                            ]
                        ])->textInput(['id' => 'TDataReklame_nilai_sewa_0', 'name' => 'TDataReklame[nilai_sewa][]', 'size' => 7, 'maxlength' => 15, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                        ?>
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-12">

                      <div class="col-md-2">
                        <label>
                          <?= Html::checkbox('rokok[]', false, ['id' => 'rokok_0', 'value' => '1', 'uncheckValue' => '0']) ?>
                          Tarif Rokok
                        </label>
                          <?php
                          echo $form->field($modelReklame, 'p_rokok',['addon' => [
                                  'append' => [
                                      'content' => '%.',
                                  ]
                              ]
                          ])->textInput(['id' => 'TDataReklame_p_rokok_0', 'name' => 'TDataReklame[p_rokok][]', 'size' => 7, 'maxlength' => 5, "class" => "form-control p-rokok", "readOnly" => 'true', 'value' => 21,"onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"])->label(false);
                          ?>
                          <input type="hidden" name="tariflama[]" id="tariflama_0">
                          <input type="hidden" name="tarifpr[]" id="tarifpr_0">
                      </div>
                      <div class="col-md-2 col-md-offset-2">
                        <?php
                        echo $form->field($modelReklame, 'pajak', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ]
                            ]
                        ])->textInput(['id' => 'TDataReklame_pajak_0', 'name' => 'TDataReklame[pajak][]', 'size' => 7, 'maxlength' => 15, "onkeyup" => "this.value=this.value.replace(/[^0-9]/g,'')"]);
                        ?>
                      </div>

                  </div>
              </div>
            </div>
            <div id="reklames" class="span-reklame">

            </div>
            <br>
            <div class="col-md-12">
              <?= Html::button('Tambah Reklame', ['id' => 'btn-add-rek', 'class' => 'btn btn-success']) ?>
            </div>
            <br><br>
            <hr />
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?= $form->field($model, 'nmr_bayar')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($model, 'lokasi')->textArea(['row' => 3]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kel_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kec_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-2">
                        <?= $form->field($modelSpt, 'npwpd')->textInput(['readOnly' => true]) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'nm_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-1">
                        <?php echo $form->field($modelData, 'no_data')->textInput(array('maxlength' => 15, "class" => "span-2", 'readonly' => 'readonly')); ?>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($modelDaftar, 'alm_wp')->textArea(['readOnly' => true, 'row' => 3]) ?>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-12">
                  <div class="col-md-4 col-md-pull-0">
                    <label>Pajak Keseluruhan</label>
                    <div class="controls">
                        <?php
                        echo $form->field($modelData, 'jml_pr', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp',
                                ],
                                'append' => [
                                    'content' => Html::checkbox('TData[pembulatan]', false, ['id' => 'pembulatan', 'value' => '1', 'uncheckValue' => '0']) . " Dibulatkan?"
                                ]
                            ]
                        ])->textInput(array('size' => 7, 'maxlength' => 15, "class" => "form-control", "style" => "text-align:right;", "readOnly" => true))->label(false);
                        ?>

                    </div>

                  </div>
                </div>
                <!-- <div class="col-md-12">
                  <div class="col-md-4 col-md-pull-0">
                    <label>Petugas Penerima</label>
                    <div class="controls">
                        <?php
                        //echo $form->field($modelData, 'penerima')->textInput(array('size' => 7, 'maxlength' => 15, "class" => "span-5", "readOnly" => true, "value" => Yii::$app->user->identity->username))->label(false);
                        ?>

                    </div>

                  </div>
                  <div class="col-md-2 col-md-offset-2">
                    <label>Tanggal Terima</label>
                    <div class="controls">
                        <?php
                          //echo $form->field($modelData, 'tgl_penerima', ['addon' => [
                                  //'prepend' => [
                                      //'content' => '<i class="glyphicon glyphicon-calendar"></i>',
                                  //]
                              //]
                          //])->textInput(array('size' => 7, 'maxlength' => 15, "class" => "form-control", "readOnly" => true, 'value' => date('d-m-Y')))->label(false);
                        ?>
                    </div>

                  </div>

                </div> -->
                <!-- <div class="col-md-12">
                  <div class="col-md-4 col-md-pull-0">
                    <label>Petugas Pemungut</label>
                    <div class="controls">
                        <?php
                          //$datapetugas = Pegawai::find()->all();
                          //$arrayPetugas = array();
                          //foreach($datapetugas as $key){
                              //$arrayPetugas[$key->nip] = $key->nm_pegawai;
                          //}
                          //echo $form->field($modelData, 'nip')->widget(Select2::classname(), [
                              //'data' => $arrayPetugas,
                              //'options' => ['placeholder' => 'Pilih Petugas ...'],
                              //'pluginOptions' => [
                                  //'allowClear' => true
                              //],
                          //])->label(false);
                        ?>
                    </div>

                  </div>
                </div> -->
            </div>
            <input id="id_ayt" type="hidden" name="id_ayt">
            <input id="kd_ayt" type="hidden" value="<?= $id ?>" name="kd_ayt">
            <input id="nilai" type="hidden" value="<?= $model->nilai ?>" name="nilai">
            <input id="id_tetap" type="hidden" value="" name="id_tetap">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?= Html::button($model->isNewRecord ? ' Simpan' : ' Edit', ['id' => 'simpanPendataan', 'class' => 'btn btn-success']) ?>
                        <!--<button class="btn btn-warning" type="submit">Batal</button>-->
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>
    <?php
    Modal::begin([
        'id' => 'm_ayat',
        'header' => 'Data Rekening',
    ]);
//    $searchAyat = new TAyatSearch();
//    $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
//    $dataAyat->pagination->pageSize = 5;
    echo $this->render('_m_ayat', [
        'searchAyat' => $searchAyat,
        'dataAyat' => $dataAyat,
    ]);
    ?>

    <?php
    Modal::end();
    ?>

</div>
<?php
$js = <<< JS
  var dataRekening;
  var current_id = 1;

  $('#tayat-status').change(function () {
      var tahun = $("#tdata-th_spt").val();
      var status = $("#tayat-status").val();
      var kd_rek = '1104';
      $.ajax({
          url: "get-kode-rekening-search",
          data: 'tahun=' + tahun + '&kd_rek=' + kd_rek + '&status=' + status,
          type: 'POST',
          dataType: 'json',
          success: function (data) {
              console.log(data);
              if (parseInt(data) !== 1) {
                  $("#TDataReklame_id_ayt_0").select2({
                      theme: "classic",
                      minimumInputLength: 2,
                      placeholder: 'Pilih Kode Rekening',
                      data: data,
                      width: 500,
                  });
              }
          }
      });
  });

  $("#TDataReklame_id_ayt_0").select2({
      minimumInputLength: 2,
      placeholder: 'Pilih Tetap/Isidentil Terlebih Dahulu',
      width: 500,
      data: dataRekening
  });

  $('#pembulatan').change(function (){
      var jml_pr_awal = $('#jml_pr_asli').val();
      var jml_pr_bulat = parseFloat($('#TData_jml_pr').val().replace(/\./g, '').replace(',','.'));
      if ($(this).prop('checked')) {
          $('#TData_jml_pr').val(jml_pr_bulat.numberFormat(0, ',', '.'));
      } else {
          $('#TData_jml_pr').val(jml_pr_awal);
      }
  });

  $('#btn-add-rek').click(function () {
      console.log('asdf');
      var status = $('#tayat-status').val();
      if (status === '' || status === null) {
          return false;
      }
      var x = nextElement($('#detail-reklame'));
      $('#detail-reklame_' + x).fadeIn('slow');
      $('#detail-reklame_' + x).find('#TDataReklame_id_ayt_' + x).focus();
      current_id = current_id + 1;
  });

  //variable masih manual
  $(document).on('click', "a.remove", function (e) {
      var variable = 'variable';
      console.clear();
      console.log($(this).parent().parent().attr('id'));
      console.log('variable ' + variable);
      if($(this).parent().parent().attr('id') != 'detail-reklame'){
        if(variable != 1){
            $(this).parent().parent().remove();
        }
      }
  });

  function getRekening(id){
      var tahun = $('#tdata-th_spt').val();
      var status = $('#tayat-status').val();
      var kd_rek = '1104';
      $.ajax({
          url: "get-kode-rekening-search",
          data: 'tahun=' + tahun + '&kd_rek=' + kd_rek + '&status=' + status,
          type: 'POST',
          dataType: 'json',
          success: function (data) {
              console.log(data);
              if (parseInt(data) !== 1) {
                  $("#TDataReklame_id_ayt_"+id).select2({
                      minimumInputLength: 2,
                      placeholder: 'Pilih Kode Rekening',
                      data: data,
                      width: 500,
                  });
              }
          }
      });
  }

  function nextElement(element) {
      var newElement = element.clone();
      var id = current_id;
      id_span_select2 = newElement.find('#select2-TDataReklame_id_ayt_0-container');
      id_span_select2.parent().parent().parent().remove();
      getRekening(id);
      newElement.attr("id", element.attr("id").split("_")[0] + "_" + id);
      var input = newElement.find('input');
      var select = newElement.find('select');
      $.each(input, function(index) {
          if(typeof($(this).attr("id")) !== 'undefined'){
              $(this).attr("id", $(this).attr("id").replace(/\d*/g, '') + id);
          }
      });
      $.each(select, function() {
          $(this).attr("id", $(this).attr("id").replace(/\d*/g, '') + id);
      });
      newElement.find('input, select').val(''); //clear all values
      newElement.find('#TDataReklame_p_rokok_'+id).attr('readOnly', true);
      newElement.find('#rokok_'+id).prop('checked', false);
      newElement.find('#TDataReklame_hari_'+id).val(1);
      newElement.appendTo($("#reklames"));
      return id;
  }


  
  $('.span-reklame').on('change', 'input, select', function(e){
      var id = e.currentTarget.id;
      var count = id.replace(/\D*/, '');
      var status = $('#tayat-status').val();
      console.log('count ' + count);
      console.log('status ' + status);
      id_ayt = $('#TDataReklame_id_ayt_' + count).val();
      if(id === 'TDataReklame_id_ayt_' + count){
          $.ajax({
              url: "get-detail-ayat",
              data: 'id_ayt=' + id_ayt,
              type: 'POST',
              dataType: 'json',
              success: function (data) {
                  if (parseInt(data) !== 1) {
                      $("#TAyat_tarifrp_"+count).val(data.tarifrp);
                      $("#TAyat_tarifpr_"+count).val(data.tarifpr*100);
                      $("#tarifpr_" + count).val(data.tarifpr*100);
                      $("#TDataReklame_p_rokok_" + count).val(data.tarifpr*100);
                  }
              }
          });
          hitung_reklame(id, count);
      }
      if (parseInt(status) == 1) {
          $('#TDataReklame_tinggi_'+count).removeAttr('disabled');
          $('#TDataReklame_sdt_pandang_'+count).removeAttr('disabled');
      } else if (parseInt(status) == 2) {
          $('#TDataReklame_tinggi_'+count).attr('disabled', 'disabled');
          $('#TDataReklame_sdt_pandang_'+count).attr('disabled', 'disabled');
      }
      if(id === 'rokok_' + count){
          if ($('#rokok_' + count).prop('checked')) {
              if (status === '' || status === null) {
                  return false;
              }
              $("#TDataReklame_p_rokok_" + count).attr('readOnly', false);
              if (id !== 'TDataReklame_p_rokok_'+count && json == null){
                  $("#TDataReklame_p_rokok_" + count).val(parseFloat(21));
              }
              $('#tariflama_'+count).val($('#tarifpr_'+count).val());
              $('#tarifpr_'+count).val($('#TDataReklame_p_rokok_' + count).val());
          } else {
              if (status === '' || status === null) {
                  return false;
              }
              $("#TDataReklame_p_rokok_" + count).attr('readOnly', true);
              $("#TDataReklame_p_rokok_" + count).val($('#tariflama_'+count).val());
              $('#tariflama_'+count).val($('#tarifpr_'+count).val());
              $('#tarifpr_'+count).val($('#TAyat_tarifpr_'+count).val());
          }
      }
      hitung_reklame(id, count);
  });

  function hitung_reklame(id, count) {
      console.clear();
      console.log(id+count);
      var classHapus = $('a.remove');
      var id = '#'+id;
      var idDiv = $(id).parent().parent().parent().parent().parent().parent().attr('id');
      if (typeof(idDiv) === 'undefined') {
          idDiv = $(id).parent().parent().parent().parent().parent().parent().parent().attr('id');
      }
      idDiv = $('#' + idDiv);
      console.log("iddiv"+idDiv);
      var panjang = (idDiv.find('#TDataReklame_panjang_' + count).val() === '' || idDiv.find('#TDataReklame_panjang_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_panjang_' + count).val().replace(',', '.')));
      var lebar = (idDiv.find('#TDataReklame_lebar_' + count).val() === '' || idDiv.find('#TDataReklame_lebar_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_lebar_' + count).val().replace(',', '.')));
      var nilai_media = (idDiv.find('#TAyat_tarifrp_' + count).val() === '' || idDiv.find('#TAyat_tarifrp_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TAyat_tarifrp_' + count).val().replace(/\./g, '')));
      var tinggi = (idDiv.find('#TDataReklame_tinggi_' + count).val() === '' || idDiv.find('#TDataReklame_tinggi_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_tinggi_' + count).val().replace(',', '.')));
      var skor_lokasi = (idDiv.find('#TDataReklame_lokasi_' + count).val() === '' || idDiv.find('#TDataReklame_lokasi_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_lokasi_' + count).val().replace(',', '.')));
      var skor_sudut_pandang = (idDiv.find('#TDataReklame_sdt_pandang_' + count).val() === '' || idDiv.find('#TDataReklame_sdt_pandang_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_sdt_pandang_' + count).val().replace(',', '.')));
      var skor_ketinggian = hitung_ketinggian(tinggi);
      var tarifpr = (idDiv.find('#tarifpr_' + count).val() === '' || idDiv.find('#tarifpr_' + count).val() === null ? 0 : parseFloat(idDiv.find('#tarifpr_' + count).val().replace(',', '.')));
      var hari = (idDiv.find('#TDataReklame_hari_' + count).val() === '' || idDiv.find('#TDataReklame_hari_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_hari_' + count).val().replace(',', '.')));
      var jumlah_reklame = (idDiv.find('#TDataReklame_jml_rek_' + count).val() === '' || idDiv.find('#TDataReklame_jml_rek_' + count).val() === null ? 0 : parseFloat(idDiv.find('#TDataReklame_jml_rek_' + count).val().replace(',', '.')));
      var luas = (panjang * lebar).toFixed(2);
      var pajak_seluruh_reklame = 0, pajak_reklame = 0, total_nilai_strategis, total_media_reklame, nilai_sewa_reklame;

      idDiv.find('#TDataReklame_luas_' + count).val(luas.toString().replace('.', ','));
      if (parseInt($('#tayat-status').val()) === 1) {
          var nilai_luas = luas * nilai_media;
          console.log("nilai_luas123"+nilai_luas);
          var nilai_ketinggian = 140000 * tinggi;
          console.log("nilai_ketinggian123"+nilai_ketinggian);
          var total_nilai_media_reklame = nilai_luas + nilai_ketinggian;
          var jml_skor_lokasi = skor_lokasi * 50 / 100;
          var jml_skor_sudut_pandang = skor_sudut_pandang * 20 / 100;
          var jml_skor_ketinggian = skor_ketinggian * 30 / 100;
          total_nilai_strategis = (jml_skor_lokasi + jml_skor_sudut_pandang + jml_skor_ketinggian) * tentukan_satuan_nilai_stategis(luas) * hari;
          nilai_sewa_reklame = total_nilai_media_reklame + total_nilai_strategis;
          pajak_reklame = tarifpr / 100 * nilai_sewa_reklame;
          total_media_reklame = total_nilai_media_reklame;
      } else if (parseInt($('#tayat-status').val()) === 2) {
          total_media_reklame = nilai_media * hari * luas;
          total_nilai_strategis = skor_lokasi * 1000 * hari * luas;
          pajak_reklame = tarifpr / 100 * (total_media_reklame + total_nilai_strategis);
          nilai_sewa_reklame = total_media_reklame + total_nilai_strategis;
      }
      if (typeof (total_nilai_strategis) === 'undefined' || isNaN(total_nilai_strategis))
          total_nilai_strategis = 0;
      $('#TDataReklame_nilai_strategis_' + count).val(parseFloat(total_nilai_strategis).numberFormat(2, ',', '.'));
      if (typeof (nilai_sewa_reklame) === 'undefined' || isNaN(nilai_sewa_reklame))
          nilai_sewa_reklame = 0;
      $('#TDataReklame_nilai_sewa_' + count).val(parseFloat(nilai_sewa_reklame).numberFormat(2, ',', '.'));
      if (typeof (total_media_reklame) === 'undefined' || isNaN(total_media_reklame))
          total_media_reklame = 0;
      $('#TDataReklame_nilai_media_' + count).val(parseFloat(total_media_reklame).numberFormat(2, ',', '.'));
      var pajak_jml_reklame = pajak_reklame * jumlah_reklame;
      if (typeof (pajak_jml_reklame) === 'undefined' || isNaN(pajak_jml_reklame))
          pajak_jml_reklame = 0;
      $('#TDataReklame_pajak_' +count).val(parseFloat(pajak_jml_reklame).numberFormat(2, ',', '.'));

      $.each(classHapus, function(index){
          idParent = $(this).parent().parent().attr('id');
          countParent = idParent.replace(/\D*/, '') === '' || idParent.replace(/\D*/, '') === null ? 0 : idParent.replace(/\D*/, '');
          pajak_seluruh_reklame = pajak_seluruh_reklame + parseFloat($(document).find('#TDataReklame_pajak_' + countParent).val().replace(/\./g, '').replace(',', '.'));
          console.log('pajak seluruh reklame ' + index + ' --> ' + pajak_seluruh_reklame);
      });

      if(typeof(pajak_seluruh_reklame) === 'undefined' || isNaN(pajak_seluruh_reklame))
          pajak_seluruh_reklame = 0;
      $('#TData_jml_pr').val(parseFloat(pajak_seluruh_reklame).numberFormat(2, ',', '.'));
      $('#jml_pr_asli').val(parseFloat(pajak_seluruh_reklame).numberFormat(2, ',', '.'));

      console.log('luas ' + luas);
      console.log('tarifpr ' + tarifpr);
      console.log('pajak_reklame ' + pajak_reklame);
      console.log('nilai_media ' + nilai_media);
      console.log('nilai_luas ' + nilai_luas);
      console.log('nilai_ketinggian ' + nilai_ketinggian);
      console.log('total_nilai_media_reklame ' + total_nilai_media_reklame);
      console.log('satuan_nilai_strategis ' + tentukan_satuan_nilai_stategis(luas));
      console.log('total_nilai_strategis ' + total_nilai_strategis);
  }

  function hitung_ketinggian(i) {
      var x;
      if (parseFloat(i) > 15) {
          x = 10;
      } else if (parseFloat(i) >= 10 && parseFloat(i) <= 14.99) {
          x = 8;
      } else if (parseFloat(i) >= 6 && parseFloat(i) <= 9.99) {
          x = 6;
      } else if (parseFloat(i) >= 3 && parseFloat(i) <= 5.99) {
          x = 4;
      } else if (parseFloat(i) <= 2.99) {
          x = 2;
      }
      return x;
  }

  function tentukan_satuan_nilai_stategis(l) {
      var nilai_strategis;
      if (parseFloat(l) >= 0.25 && parseFloat(l) <= 6.99) {
          nilai_strategis = 600000;
      } else if (parseFloat(l) >= 7 && parseFloat(l) <= 15.99) {
          nilai_strategis = 1200000;
      } else if (parseFloat(l) >= 16 && parseFloat(l) <= 29.99) {
          nilai_strategis = 1525000;
      } else if (parseFloat(l) >= 30 && parseFloat(l) <= 40) {
          nilai_strategis = 2100000;
      } else if (parseFloat(l) > 40) {
          nilai_strategis = 3600000;
      }
      return nilai_strategis;
  }

  $('#tdata-th_spt').change(function () {
      dapatkanNoData1("get-no-data");
  }).trigger('change');


  function dapatkanNoData1(url) {
    $("#loader-tahun").show();
    $.ajax({
        url: url,
        data: 'tahun=' + $('#tdata-th_spt').val(),
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#tdata-no_data').val(data);
            $("#loader-tahun").hide();
        }
    });
}

$("#simpanPendataan").click(function (e) {
    e.preventDefault();
    bootbox.dialog({
            message: "Apa Anda yakin ingin memproses data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Ya",
                    className: "btn-success",
                    callback: function () {
                        $("#spinner").fadeIn("slow");
                        var dataform = new FormData($("#form-pendataan-reklame")[0]);
                        $.ajax({
                            type: "POST",
                            url: "simpan-pendataan-reklame",
                            data: dataform,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                $("#spinner").fadeOut("slow");
                                if (parseInt(data) === 1) {
                                    bootbox.dialog({
                                        message: "Proses data berhasil.",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                                callback: function () {
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                    //$(#form-pendataan :input").attr("disabled", true);
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    bootbox.dialog({
                                        message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "Tidak",
                    className: "btn-warning",
                    callback: function () {

                    }
                }
            }
        });
});

JS;

$this->registerJs($js);
?>
