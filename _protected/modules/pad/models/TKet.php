<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_ketspt".
 *
 * @property string $kd_spt
 * @property string $nm_spt
 * @property string $singk
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property string $singk_report
 */
class TKet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_ketspt';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_spt'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['kd_spt'], 'string', 'max' => 1],
            [['nm_spt'], 'string', 'max' => 50],
            [['singk'], 'string', 'max' => 10],
            [['singk_report'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_spt' => 'Kd Spt',
            'nm_spt' => 'Nm Spt',
            'singk' => 'Singk',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'singk_report' => 'Singk Report',
        ];
    }
}
