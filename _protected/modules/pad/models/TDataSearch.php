<?php

namespace app\modules\pad\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\pad\models\TData;

/**
 * TDataSearch represents the model behind the search form about `app\modules\pad\models\TData`.
 */
class TDataSearch extends TData
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['th_spt', 'kode', 'st_spt', 'st', 'id_dt1', 'id_dt2', 'judul', 'lokasi', 'peremajaan', 'createdtime', 'updatedtime', 'nmr_bayar', 'flag'], 'safe'],
            [['no_data', 'id_spt', 'id_tetap', 'id_ayt', 'banyak', 'createdby', 'updatedby'], 'integer'],
            [['nilai', 'jml_pr', 'jml_pok', 'jumlah', 'hari', 'muka', 'panjang', 'lebar', 'tinggi', 'luas', 'kenaikan', 'sdt_pandang', 'bunga'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TData::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'no_data' => $this->no_data,
            'id_spt' => $this->id_spt,
            'id_tetap' => $this->id_tetap,
            'id_ayt' => $this->id_ayt,
            'nilai' => $this->nilai,
            'jml_pr' => $this->jml_pr,
            'jml_pok' => $this->jml_pok,
            'jumlah' => $this->jumlah,
            'banyak' => $this->banyak,
            'hari' => $this->hari,
            'muka' => $this->muka,
            'panjang' => $this->panjang,
            'lebar' => $this->lebar,
            'tinggi' => $this->tinggi,
            'luas' => $this->luas,
            'kenaikan' => $this->kenaikan,
            'sdt_pandang' => $this->sdt_pandang,
            'bunga' => $this->bunga,
            'createdby' => $this->createdby,
            'createdtime' => $this->createdtime,
            'updatedby' => $this->updatedby,
            'updatedtime' => $this->updatedtime,
        ]);

        $query->andFilterWhere(['like', 'th_spt', $this->th_spt])
            ->andFilterWhere(['like', 'kode', $this->kode])
            ->andFilterWhere(['like', 'st_spt', $this->st_spt])
            ->andFilterWhere(['like', 'st', $this->st])
            ->andFilterWhere(['like', 'id_dt1', $this->id_dt1])
            ->andFilterWhere(['like', 'id_dt2', $this->id_dt2])
            ->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'lokasi', $this->lokasi])
            ->andFilterWhere(['like', 'peremajaan', $this->peremajaan])
            ->andFilterWhere(['like', 'nmr_bayar', $this->nmr_bayar])
            ->andFilterWhere(['like', 'flag', $this->flag]);

        return $dataProvider;
    }
}
