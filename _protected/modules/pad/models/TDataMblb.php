<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "pad.t_data_mblb".
 *
 * @property integer $id
 * @property string $th_spt
 * @property integer $no_data
 * @property integer $id_ayt
 * @property double $volume
 * @property double $jml_pengenaan
 * @property double $jml_bayar
 */
class TDataMblb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_data_mblb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['th_spt', 'no_data', 'id_ayt', 'volume', 'jml_pengenaan', 'jml_bayar'], 'required'],
            [['no_data', 'id_ayt'], 'integer'],
            [['volume', 'jml_pengenaan', 'jml_bayar'], 'number'],
            [['th_spt'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'th_spt' => 'Th Spt',
            'no_data' => 'No Data',
            'id_ayt' => 'Id Ayt',
            'volume' => 'Volume',
            'jml_pengenaan' => 'Jml Pengenaan',
            'jml_bayar' => 'Jml Bayar',
        ];
    }
}
