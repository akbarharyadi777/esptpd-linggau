<?php

use kartik\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
` 
<div class="modal-content">    
    <!--    <div style="border-color: #f39c12;padding: 15px;overflow: hidden;" class="box box-primary">
            <div class="tdata-self-search">
    <?php // Pjax::begin(['id' => 'search-form']) ?>
    <?php
//            $form = ActiveForm::begin([
//                        //'action' => ['index'],
//                        'method' => 'get',
    // ]);
    ?>
                <div class="col-md-12">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label col-md-4" style="margin-top: 5px;">Nama Rekening</label>
                            <div class="col-md-8">
    <?php
//                            echo $form->field($searchAyat, 'nm_ayt', [
//                                'addon' => [
//                                    'append' => [
//                                        'content' => Html::submitButton('Cari', ['class' => 'btn btn-success']),
//                                        'asButton' => true
//                                    ]
//                                ]
//                            ])->label(false)
    ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
    
    
                <div class="form-group">
    
                </div>
    <?php // ActiveForm::end(); ?>
    <?php // Pjax::end() ?>
            </div>
        </div>-->

    <hr style="border-color: #c7c7c7;">
    <div class="modal-body">
        <?php // Pjax::begin(['id' => 'grid']) ?>
        <?php
        echo GridView::widget([
            'id' => 'gridAyat',
            'dataProvider' => $dataAyat,
            'filterModel' => $searchAyat,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'attribute' => 'kd_ayt',
                    'value' => function ($model) {
                        return $model->kd_ayt . '.' . $model->jn_ayt . '.' . $model->kl_ayt;
                    },
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'attribute' => 'nm_ayt',
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'attribute' => 'tarifrp',
                    'value' => function ($model) {
                        return number_format($model->tarifrp, 0, '.', ',');
                    },
                ],
                [
                    //'class' => '\kartik\grid\DataColumn',
                    'attribute' => 'tarifpr',
                    'value' => function ($model) {
                        return $model->tarifpr * 100 . '%';
                    },
                ],
                [
                    'class' => '\yii\grid\ActionColumn',
                    'template' => '{pilih}',
                    'buttons' => [
                        'pilih' => function ($url, $model, $key) {
                            return Html::button("Pilih", ["id" => "buttonPilihPegawai", "class" => "btn btn-success",
                                        "id_ayt" => $model->id_ayt,
                                        "jn_ayt" => $model->jn_ayt,
                                        "kl_ayt" => $model->kl_ayt,
                                        "nm_ayt" => $model->nm_ayt,
                                        "tarifrp" => $model->tarifrp,
                                        "tarifpr" => $model->tarifpr * 100,
                                        "onClick" => "getAyat($(this).attr('id_ayt'),$(this).attr('jn_ayt'),$(this).attr('kl_ayt'),$(this).attr('nm_ayt'),$(this).attr('tarifpr'),$(this).attr('tarifrp'))"]);
                        }
                            ],
                        ]
                    ],
                    'toolbar' => [
                    ],
                    'export' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'options' => [
                            'enablePushState' => false,
                        ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => '<i class="glyphicon glyphicon-book"></i>',
                    ],
                ]);
                ?>
                <?php // Pjax::end() ?>
            </div>	


            <?php
            $js = <<<JS
function getAyat(id_ayt, jn_ayt, kl_ayt, nm_ayt, tarifpr, tarifrp) {
    $('#id_ayt').val(id_ayt);
    $('#tdataself-kd_rek').val(jn_ayt.toString() + kl_ayt.toString());
    $('#tdataself-nm_ayt').val(nm_ayt);
    $('#tdataself-tarifpr').val(tarifpr);
    if ($('#kd_ayt').val() == '1105' || $('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
        $('#tdataself-tarifrp').val(numerik(tarifrp));
    }
    $('#m_ayat').modal('hide');
    var omset = $("#tdataself-nilai").val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    if (omset != '') {
        if ($('#kd_ayt').val() == '1105') {
            var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
            var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else if ($('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100))));
        }
    }
}
JS;

            $this->registerJs($js);
            ?>
