<?php

use app\modules\management\models\forms\ChangeOwnPasswordForm;
use app\modules\management\UserManagementModule;
use kartik\widgets\ActiveForm;
use nenad\passwordStrength\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ChangeOwnPasswordForm $model
 */
$this->title = 'Ganti Password';
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'form-pendataan',
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
    ]);
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success text-center">
                            <?= 'Password berhasil diubah.' ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>


            <?php if ($model->scenario != 'restoreViaEmail'): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'current_password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                                'pluginOptions' => [
                                    'showMeter' => false,
                                    'toggleMask' => true
                                ]
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?=
                        $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                            'pluginOptions' => [
                                'showMeter' => true,
                                'toggleMask' => true
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?=
                        $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])->widget(PasswordInput::classname(), [
                            'pluginOptions' => [
                                'showMeter' => true,
                                'toggleMask' => true
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?=
                        Html::submitButton(
                                '<span class="glyphicon glyphicon-ok"></span> ' . UserManagementModule::t('back', 'Ganti'), ['class' => 'btn btn-success']
                        )
                        ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

