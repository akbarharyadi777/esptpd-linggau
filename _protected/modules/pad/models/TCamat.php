<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "t_camat".
 *
 * @property string $kd_camat
 * @property string $nm_camat
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 */
class TCamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_camat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_camat'], 'required'],
            [['createdby', 'updatedby'], 'integer'],
            [['createdtime', 'updatedtime'], 'safe'],
            [['kd_camat'], 'string', 'max' => 2],
            [['nm_camat'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'kd_camat' => 'Kd Camat',
            'nm_camat' => 'Nm Camat',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
        ];
    }
}
