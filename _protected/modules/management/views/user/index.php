<?php

use app\modules\management\components\GhostHtml;
use app\modules\management\models\rbacDB\Role;
use app\modules\management\models\search\UserSearch;
use app\modules\management\models\User;
use app\modules\management\UserManagementModule;
use kartik\grid\GridView;
use webvimark\extensions\GridPageSize\GridPageSize;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */
$this->title = UserManagementModule::t('back', 'Tabel User');
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <!--            <div class="row">
                    <div class="col-sm-6">
                        <p>
                          
                        </p>
                    </div>
    
                    <div class="col-sm-6 text-right">
    <?= GridPageSize::widget(['pjaxId' => 'user-grid-pjax']) ?>
                    </div>
                </div>-->


    <?php
    Pjax::begin([
        'id' => 'user-grid-pjax',
    ])
    ?>

    <?=
    GridView::widget([
        'id' => 'user-grid',
        'dataProvider' => $dataProvider,
        'pager' => [
            'options' => ['class' => 'pagination pagination-sm'],
            'hideOnSinglePage' => true,
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:10px']],
            [
                'class' => 'webvimark\components\StatusColumn',
                'attribute' => 'superadmin',
                'visible' => Yii::$app->user->isSuperadmin,
            ],
            [
                'attribute' => 'username',
                'value' => function(User $model) {
                    return Html::a($model->username, ['update', 'id' => $model->id], ['data-pjax' => 0]);
                },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'email',
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
//                            [
//                                'class' => 'webvimark\components\StatusColumn',
//                                'attribute' => 'email_confirmed',
//                                'visible' => User::hasPermission('viewUserEmail'),
//                            ],
                    [
                        'attribute' => 'gridRoleSearch',
                        'filter' => ArrayHelper::map(Role::getAvailableRoles(Yii::$app->user->isSuperAdmin), 'name', 'description'),
                        'value' => function(User $model) {
                            return implode(', ', ArrayHelper::map($model->roles, 'name', 'description'));
                        },
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUserRoles'),
                    ],
//                            [
//                                'attribute' => 'registration_ip',
//                                'value' => function(User $model) {
//                                    return Html::a($model->registration_ip, "http://ipinfo.io/" . $model->registration_ip, ["target" => "_blank"]);
//                                },
//                                        'format' => 'raw',
//                                        'visible' => User::hasPermission('viewRegistrationIp'),
//                                    ],
                    [
                        'value' => function(User $model) {
                            return GhostHtml::a(
                                            UserManagementModule::t('back', 'Roles and permissions'), ['/user-management/user-permission/set', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary', 'data-pjax' => 0]);
                        },
                                'format' => 'raw',
                                'visible' => User::canRoute('/user-management/user-permission/set'),
                                'options' => [
                                    'width' => '10px',
                                ],
                            ],
                            [
                                'value' => function(User $model) {
                                    return GhostHtml::a(
                                                    UserManagementModule::t('back', 'Change password'), ['change-password', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-pjax' => 0]);
                                },
                                        'format' => 'raw',
                                        'options' => [
                                            'width' => '10px',
                                        ],
                                    ],
                                    [
                                        'class' => 'webvimark\components\StatusColumn',
                                        'attribute' => 'status',
                                        'optionsArray' => [
                                            [User::STATUS_ACTIVE, UserManagementModule::t('back', 'Active'), 'success'],
                                            [User::STATUS_INACTIVE, UserManagementModule::t('back', 'Inactive'), 'warning'],
                                            [User::STATUS_BANNED, UserManagementModule::t('back', 'Banned'), 'danger'],
                                        ],
                                    ],
//                                                    ['class' => 'yii\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                                    [
                                        'class' => 'kartik\grid\ActionColumn',
                                        'template' => '{update} {delete}',
                                        'contentOptions' => ['style' => 'width:70px; text-align:center;'],
                                    ],
                                ],
                                'containerOptions' => ['style' => 'overflow: auto'],
                                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                                'pjax' => true,
                                'pjaxSettings' => [
                                    'options' => [
                                        'enablePushState' => false,
                                    ],
                                ],
                                'toolbar' => [
                                    ['content' =>
                                        Html::a('Buat User', ['create'], ['class' => 'btn btn-success pull-right'])
                                    ],
                                    '{toggleData}',
                                ],
                                'export' => [
                                    'fontAwesome' => true
                                ],
                                'striped' => true,
                                'bordered' => true,
                                'condensed' => true,
                                'responsiveWrap' => true,
                                'perfectScrollbar' => true,
                                'panel' => [
                                    'type' => GridView::TYPE_SUCCESS,
                                ],
                                'persistResize' => false,
                            ]);
                            ?>

                            <?php Pjax::end() ?>

</div>
