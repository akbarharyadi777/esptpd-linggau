<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TDataSelfSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tdata-self-search">

    <div class="col-md-12">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="form-group">
                    <label class="control-label col-md-2" style="margin-top: 5px;">Pencarian</label>
                    <div class="col-md-10">
                        <?=
                        $form->field($model, 'nm_ayt', [
                            'addon' => [
                                'append' => [
                                    'content' => Html::submitButton('Cari', ['class' => 'btn btn-succsess']),
                                    'asButton' => true
                                ]
                            ]
                        ])->label(false)
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>


        <?php // echo $form->field($model, 'tgl_terima') ?>

        <?php // echo $form->field($model, 'ket_kasus') ?>

        <?php // echo $form->field($model, 'id_status')   ?>

        <div class="form-group">

        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>

<hr style="border-color: #c7c7c7;margin: 10px 0;">
