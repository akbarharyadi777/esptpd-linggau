<?php

use kartik\datecontrol\Module;
use webvimark\modules\UserManagement\models\UserVisitLog;

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'e-sptpd',
    'language' => 'id',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'app\components\Aliases'],
    'modules' => [
        'pad' => [
            'class' => 'app\modules\pad\Pad',
        ],
        'user-management' => [
            'class' => 'app\modules\management\UserManagementModule'
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
//            'i18n' => [
//                'class' => 'yii\i18n\PhpMessageSource',
//                'basePath' => '@kvgrid/messages',
//                'forceTranslation' => true
//            ]
        ],
        'datecontrol' => [
            'class' => 'kartik\datecontrol\Module',
            // format settings for displaying each date attribute (ICU format example)
            'displaySettings' => [
                Module::FORMAT_DATE => 'dd-MM-yyyy',
                Module::FORMAT_TIME => 'HH:mm:ss a',
                Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a',
            ],
            // format settings for saving each date attribute (PHP format example)
            'saveSettings' => [
                Module::FORMAT_DATE => 'php:Y-m-d',
                Module::FORMAT_TIME => 'php:H:i:s',
                Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
            ],
            'autoWidget' => true,
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => ['type' => 2, 'pluginOptions' => ['autoclose' => true]], // example
                Module::FORMAT_DATETIME => [], // setup if needed
                Module::FORMAT_TIME => [], // setup if needed
            ],
        ],
//        'widgetSettings' => [
//            Module::FORMAT_DATE => [
//                'class' => 'yii\jui\DatePicker', // example
//                'options' => [
//                    'dateFormat' => 'php:d-M-Y',
//                    'options' => ['class' => 'form-control'],
//                ]
//            ]
//        ]
    ],
    'components' => [
        'user' => [
            'class' => 'app\modules\management\components\UserConfig',
            // Comment this if you don't want to record user logins
            'on afterLogin' => function($event) {
                UserVisitLog2::newVisitor($event->identity->id);
            }
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) 
            // - this is required by cookie validation
            'cookieValidationKey' => '6DTsyiBQ_u0KEPza6Rg6b58UnSrRKNqH',
        ],
        // you can set your theme here 
        // - template comes with: 'default', 'slate', 'spacelab' and 'cerulean'
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@webroot/themes/e-sptpd/views'],
                'baseUrl' => '@web/themes/e-sptpd',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                // we will use bootstrap css from our theme
//                'yii\bootstrap\BootstrapAsset' => [
//                    'css' => [], // do not use yii default one
//                ],
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue-light',
                ],
            // // use bootstrap js from CDN
            // 'yii\bootstrap\BootstrapPluginAsset' => [
            //     'sourcePath' => null,   // do not use file from our server
            //     'js' => [
            //         'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js']
            // ],
            // // use jquery from CDN
            // 'yii\web\JqueryAsset' => [
            //     'sourcePath' => null,   // do not use file from our server
            //     'js' => [
            //         'ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js',
            //     ]
            // ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'user' => [
            'identityClass' => 'app\models\UserIdentity',
            'enableAutoLogin' => false,
        ],
        'session' => [
            'class' => 'yii\web\Session',
            'savePath' => '@app/runtime/session'
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'user' => [
            'class' => 'app\modules\management\components\UserConfig',
            // Comment this if you don't want to record user logins
            'on afterLogin' => function($event) {
                UserVisitLog::newVisitor($event->identity->id);
            }
        ],
//        'i18n' => [
//            'translations' => [
//                'app*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@app/translations',
//                    'sourceLanguage' => 'id',
//                ],
//                'yii' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@app/translations',
//                    'sourceLanguage' => 'en'
//                ],
//            ],
//        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
    // set allias for our uploads folder 
    // @appRoot alias is definded in config/bootstrap.php file
//    'aliases' => [
//        '@themes' => '@appRoot/themes/e-sptpd',
//        '@images' => '@appRoot/themes/images',
//    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
