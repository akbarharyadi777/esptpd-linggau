<?php

/**
 *
 * @var View $this
 * @var ActiveForm $form
 * @var Role $model
 */

use app\modules\management\models\rbacDB\Role;
use app\modules\management\UserManagementModule;
use yii\helpers\Html;
use yii\web\View;
use kartik\widgets\ActiveForm;

$this->title = UserManagementModule::t('back', 'Buat Role');
?>


<div class="role-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>
</div>