<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\pad\models\TDataSelfSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekam Pendataan Hotel';
?>
<div class="tdata-self-index" style="width: 100%">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="col-md-12">
        <?= Html::a('Rekam Pendataan Hotel', ['create', 'id' => '1101'], ['class' => 'btn btn-success pull-right']) ?>
    </div>
    <div class="col-md-12" style="margin-top: 10px; margin-bottom: 10px;">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                'th_spt',
                'no_data_self',
                'id_ayt',
                'nilai',
                'jml_pjk',
                ['class' => 'kartik\grid\ActionColumn',
                    'template' => '{update} {delete}'],
            ],
            'containerOptions' => ['style' => 'overflow: auto'],
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false,
                ],
            ],
            'toolbar' => [
            ],
            'showPageSummary' => $pageSummary,
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'heading' => $heading,
            ],
            'persistResize' => false,
            'exportConfig' => $exportConfig,
        ]);
        ?>
    </div>
</div>
