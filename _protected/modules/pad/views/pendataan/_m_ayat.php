<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

?>
        <?php
        echo GridView::widget([
            'id' => 'gridAyat',
            'dataProvider' => $dataAyat,
            //'filterModel' => $searchAyat,
            'layout' => "{items}\n{pager}",
            'columns' => [
                ['class' => '\yii\grid\SerialColumn'],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Kode Ayat',
                    'attribute' => 'kd_ayt',
                    'value' => function ($model) {
                        return $model->kd_ayt.'.'.$model->jn_ayt.'.'.$model->kl_ayt;
                    },
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Nama Ayat',
                    'attribute' => 'nm_ayt',
                ],
                [
                    //'class' => '\yii\grid\DataColumn',
                    'header' => 'Tarif',
                    'attribute' => 'tarifrp',
                    'value' => function ($model) {
                        return number_format($model->tarifrp, 0, '.', ',');
                    },
                ],
                [
                    //'class' => '\kartik\grid\DataColumn',
                    'header' => 'Nilai Sewa',
                    'attribute' => 'tarifpr',
                    'value' => function ($model) {
                        return $model->tarifpr * 100 .'%';
                    },
                ],
                [
                    'class' => '\yii\grid\ActionColumn',
                    'template' => '{pilih}',
                    'buttons' => [
                        'pilih' => function ($url, $model, $key) {
                            return Html::button('Pilih', ['id' => 'buttonPilihPegawai', 'class' => 'btn btn-success',
                                        'id_ayt' => $model->id_ayt,
                                        'jn_ayt' => $model->jn_ayt,
                                        'kl_ayt' => $model->kl_ayt,
                                        'nm_ayt' => $model->nm_ayt,
                                        'tarifrp' => number_format($model->tarifrp, 0, ',', '.'),
                                        'tarifpr' => $model->tarifpr * 100,
                                        'kd_ayt' => $model->kd_ayt,
                                        'onClick' => "
                                          $('#tdataself-id_ayt').val($(this).attr('id_ayt'));
                                          $('#tdataself-kd_rek').val($(this).attr('jn_ayt').toString() + $(this).attr('kl_ayt').toString());
                                          $('#tayat-nm_ayt').val($(this).attr('nm_ayt'));
                                          $('#tayat-tarifpr').val($(this).attr('tarifpr'));
                                          $('#tayat-tarifrp').val($(this).attr('tarifrp'));
                                          $('#modal-pop').modal('hide');
                                          $('#tdata-nilai').trigger('change');
                                          $('#tdaftar-no_pokok').trigger('change');
                                        ", ]);
                        },
                            ],
                        ],
                    ],
                    'toolbar' => [
                    ],
                    'export' => false,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'options' => [
                            'enablePushState' => false,
                        ],
                    ],
                    'responsive' => true,
                    'hover' => true,
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => '<i class="glyphicon glyphicon-book"></i> List Rekening',
                    ],
                ]);
                ?>