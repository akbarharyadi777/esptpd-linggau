<?php

/**
 * @var View $this
 * @var array $permissionsByGroup
 * @var User $user
 */
//use yii\bootstrap\BootstrapPluginAsset;


use app\modules\management\components\GhostHtml;
use app\modules\management\models\rbacDB\Role;
use app\modules\management\models\User;
use app\modules\management\UserManagementModule;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

//BootstrapPluginAsset::register($this);
$this->title = UserManagementModule::t('back', 'Role dan Permission untuk:') . ' ' . $user->username;
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success text-center">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> <?= UserManagementModule::t('back', 'Roles') ?>
                    </strong>
                </div>
                <div class="panel-body">

                    <?php
                    $form = ActiveForm::begin([
                                'id' => $user->id,
                                'action' => Url::to(['set-roles', 'id' => $user->id]),
                                'type' => ActiveForm::TYPE_HORIZONTAL,
                                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                    ]);
                    ?>

                    <?php //= Html::beginForm(['set-roles', 'id' => $user->id]) ?>

                    <?php foreach (Role::getAvailableRoles() as $aRole): ?>
                        <label>
                            <?php $isChecked = in_array($aRole['name'], ArrayHelper::map(Role::getUserRoles($user->id), 'name', 'name')) ? 'checked' : '' ?>

                            <?php if (Yii::$app->getModule('user-management')->userCanHaveMultipleRoles): ?>
                                <input type="checkbox" <?= $isChecked ?> name="roles[]" value="<?= $aRole['name'] ?>">

                            <?php else: ?>
                                <input type="radio" <?= $isChecked ?> name="roles" value="<?= $aRole['name'] ?>">

                            <?php endif; ?>

                            <?= $aRole['description'] ?>
                        </label>

                        <?=
                        GhostHtml::a(
                                '<span class="glyphicon glyphicon-edit"></span>', ['/user-management/role/view', 'id' => $aRole['name']], ['target' => '_blank']
                        )
                        ?>
                        <br/>
                    <?php endforeach ?>

                    <br/>

                    <?php if (Yii::$app->user->isSuperadmin OR Yii::$app->user->id != $user->id): ?>

                        <?=
                        Html::submitButton(UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success btn-sm']
                        )
                        ?>
                    <?php else: ?>
                        <div class="alert alert-warning well-sm text-center">
                            <?= UserManagementModule::t('back', 'Anda tidak memiliki akses untuk fungsi ini!') ?>
                        </div>
                    <?php endif; ?>

                    <?php ActiveForm::end(); ?>
                    <?php // Html::endForm() ?>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> <?= UserManagementModule::t('back', 'Permissions') ?>
                    </strong>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <?php foreach ($permissionsByGroup as $groupName => $permissions): ?>

                            <div class="col-sm-6">
                                <fieldset>
                                    <legend><?= $groupName ?></legend>

                                    <ul>
                                        <?php foreach ($permissions as $permission): ?>
                                            <li>
                                                <?= $permission->description ?>

                                                <?=
                                                GhostHtml::a(
                                                        '<span class="glyphicon glyphicon-edit"></span>', ['/user-management/permission/view', 'id' => $permission->name], ['target' => '_blank']
                                                )
                                                ?>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </fieldset>

                                <br/>
                            </div>

                        <?php endforeach ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">

                <?=
                Html::a(
                        UserManagementModule::t('back', 'Kembali'), ['/user-management/user/index'], ['class' => 'btn btn-warning btn-sm']
                )
                ?>

            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS

$('.role-help-btn').off('mouseover mouseleave')
	.on('mouseover', function(){
		var _t = $(this);
		_t.popover('show');
	}).on('mouseleave', function(){
		var _t = $(this);
		_t.popover('hide');
	});
JS
);
?>