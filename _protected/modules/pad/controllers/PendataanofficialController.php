<?php

namespace app\modules\pad\controllers;

use app\controllers\AppController;
use app\modules\management\models\rbacDB\Route;
use app\modules\management\models\User;
use app\modules\pad\models\TAyat;
use app\modules\pad\models\TAyatSearch;
use app\modules\pad\models\TCamat;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TData;
use app\modules\pad\models\TDataReklame;
use app\modules\pad\models\TDataSelf;
use app\modules\pad\models\TLurah;
use app\modules\pad\models\TNomorMax;
use app\modules\pad\models\TSpt;
use app\modules\pad\models\TTetap;
use app\modules\pad\models\TDataAirTanah;
use JasperPHP\JasperPHP;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use dosamigos\qrcode\QrCode;
use yii\db\Query;
use yii\db\Exception;

/**
 * PendataanOfficialController implements the CRUD actions for TData model.
 */
class PendataanofficialController extends AppController {
    /**
     * Lists all TData models.
     * @return mixed
     */
//    public function actionIndex()
//    {
//        $searchModel = new TDataSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Displays a single TData model.
     * @param string $th_spt
     * @param integer $no_data
     * @return mixed
     */
//    public function actionView($th_spt, $no_data)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($th_spt, $no_data),
//        ]);
//    }

    /**
     * Creates a new TData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        $model = new TData();
        $modelReklame = new TDataReklame();
        $modelSpt = new TSpt();
        $modelDaftar = new TDaftar();
        $modelAyat = new TAyat();
        $modelData = new TData();

        $model->th_spt = date('Y');
        $model->st_spt = 'B';
        $modelSpt->tg_data = date('Y-m-d');

        $searchAyat = new TAyatSearch();
        $session = Yii::$app->session;
        $session->set('kd_ayt', $id);
        $session->set('tahun', date('Y'));
        $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
        $dataAyat->pagination->pageSize = 5;

        if ($id == '1101') {
            //$form = '_form_hotel';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1102') {
            //$form = '_form_restoran';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1103') {
            //$form = '_form_hiburan';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1104') {
            $form = '_form_reklame';
        } else if ($id == '1105') {
            //$form = '_form_penerangan_jalan';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1107') {
            //$form = '_form_parkir';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1106') {
            //$form = '_form_mblb';
            throw new NotFoundHttpException('The requested page does not exist.');
        } else if ($id == '1108') {
            //$form = '_form_air_tanah';
        } else if ($id == '1109') {
            //$form = '_form_walet';
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('create', [
                    'model' => $model,
                    'modelSpt' => $modelSpt,
                    'modelDaftar' => $modelDaftar,
                    'id' => $id,
                    'searchAyat' => $searchAyat,
                    'dataAyat' => $dataAyat,
                    'form' => $form,
                    'modelAyat' => $modelAyat,
                    'modelReklame' => $modelReklame,
                    'modelData' => $modelData
        ]);
    }

    public function actionCreateAirTanah() {
        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        $modelData = new TData();
        $modelSpt = new TSpt();
        $modelDaftar = new TDaftar();

        $session = Yii::$app->session;
        $searchAyat = new TAyatSearch();
        $session->set('kd_ayt', $id);
        $session->set('tahun', date('Y'));
        $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
        $dataAyat->pagination->pageSize = 5;
        $kode_rekening = '1108';
        $modelData->st_spt = 'B';
        $judul = 'Pendataan Pajak Air Tanah Official Assessment';

        return $this->render('_form_air_tanah', [
            'modelData' => $modelData,
            'modelSpt' => $modelSpt,
            'user' => $user,
            'modelDaftar' => $modelDaftar,
            'searchAyat' => $searchAyat,
            'dataAyat' => $dataAyat,
            'kode_rekening' => $kode_rekening,
            'judul' => $judul
        ]);
    }

    public function actionGetwp() {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $no_daftar = filter_input(INPUT_POST, 'no_daftar');
        try {
            $dataWP = TDaftar::findOne(['no_pokok' => $no_daftar]);
            if ($dataWP != null) {
                $dataWP->npwpd = $dataWP->npwpd;
                //$dataWP->npwpd = substr($dataWP->npwpd, 0, 1) . '.' . substr($dataWP->npwpd, 1, 1) . '.' . substr($dataWP->npwpd, 2, 7) . '.' . substr($dataWP->npwpd, 9, 2) . '.' . substr($dataWP->npwpd, 11, 3);
                $lurah = TLurah::findOne(['kd_camat' => $dataWP->kec_wp, 'kd_lurah' => $dataWP->kel_wp]);
                $camat = TCamat::findOne(['kd_camat' => $dataWP->kec_wp]);
                $json = array('npwpd' => $dataWP->npwpd, 'nm_wp' => $dataWP->nm_wp, 'alm_wp' => $dataWP->alm_wp, 'kecamatan' => $camat->nm_camat, 'kelurahan' => $lurah->nm_lurah);
                echo json_encode($json);
            } else {
                echo '12';
            }
        } catch (Exception $ex) {
            $ex = '13';
            echo $ex;
        }
    }

    public function actionListrekening() {
        //Yii::$app->clientScript->registerCoreScript("jquery")
        //Yii::$app->clientScript->scriptMap['jquery.js'] = false;
        //Yii::$app->clientScript->scriptMap['bootstrap.js'] = false;
        $searchAyat = new TAyatSearch();
        $session = Yii::$app->session;
        $kdayt = filter_input(INPUT_POST, 'kd_ayt');
        $tahun = filter_input(INPUT_POST, 'tahun');
        if (!empty($tahun)){
        	$session->set('kd_ayt', $kdayt);
        	$session->set('tahun', $tahun);
        }
//         $session->set('kd_ayt', $kdayt);
//         $session->set('tahun', $tahun);
        //$searchAyat->kd_ayt = ;
        //$searchAyat->tahun = ;
        //echo $searchAyat->kd_ayt . '/' . $searchAyat->tahun;
        $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
        $dataAyat->pagination->pageSize = 5;
        return $this->renderAjax('_m_ayat', [
                    'searchAyat' => $searchAyat,
                    'dataAyat' => $dataAyat,
        ]);
    }

    public function actionGetregister() {
        $no_register = 0;
        $tgl_awal = filter_input(INPUT_POST, 'tgl_awal');
        $no_pokok = filter_input(INPUT_POST, 'no_pokok');
        $npwpd = str_replace('.', '', filter_input(INPUT_POST, 'npwpd'));
        $no_register = filter_input(INPUT_POST, 'status') . filter_input(INPUT_POST, 'kd_ayt');
        $no_register .= substr($tgl_awal, 3, 2) . substr($tgl_awal, -4) . $no_pokok;
        $banyakbayar = count(TData::find()->where("substring(nmr_bayar, 1, 17)= '" . substr($no_register, 0, 17) . "'")->all()) + 1;
        $count = str_pad($banyakbayar, 3, "0", STR_PAD_LEFT);
        //$banyakbayar = count(TDataSelf::findAll("npwpd='$npwpd' and masaspt='" . substr($tgl_awal, -4) . substr($tgl_awal, 3, 2) . "'")) + 1;
        //$count = str_pad($banyakbayar, 2, "0", STR_PAD_LEFT);
        $no_register .= $count;
        echo json_encode(str_replace(' ', '', $no_register));
        //echo json_encode($no_register);
    }
    
    public function actionGetNoData(){
        echo TNomorMax::findOne(['tahun' => filter_input(INPUT_POST, 'tahun')])->no_data + 1;
    }

    //save Pendataan Air Tanah
    public function actionSave() {

        $modelSpt = new TSpt();
        $modelTetap = new TTetap();
        $modelData = new TData();
        $modelDaftar = new TDaftar();
        $modelAyat = new TAyat();
        $connection = \Yii::$app->db;
        
        $transaction = $connection->beginTransaction();

        $modelData->load(Yii::$app->request->post());
        $modelSpt->load(Yii::$app->request->post());
        $modelDaftar->load(Yii::$app->request->post());
        $modelAyat->load(Yii::$app->request->post());

        try {
            $modelSpt->thn = $modelData->th_spt;
            $modelSpt->npwpd = str_replace('.', '', $modelDaftar->npwpd);
            $modelSpt->masaspt = substr($modelSpt->tgl_awal, -4) . substr($modelSpt->tgl_awal, 3, 2);
            $modelSpt->no_spt = substr($modelSpt->npwpd, 2, 7);
            $modelSpt->tg_data = date('Y-m-d', strtotime($modelSpt->tg_data));
            $modelSpt->tg_terima = $modelSpt->tg_data;
            $modelSpt->tgl_awal = date('Y-m-d', strtotime($modelSpt->tgl_awal));
            $modelSpt->tgl_akhir = date('Y-m-d', strtotime($modelSpt->tgl_akhir));
            if (!$modelSpt->save()) {
                $transaction->rollback();
                echo json_encode(ActiveForm::validate($modelSpt));
                die();
            }

            $modelTetap->tg_jtempo = $this->hitungJatuhTempo($modelSpt->tg_data);
            $modelTetap->sisa_pajak = str_replace('.', '', $modelData->jml_pr);
            $modelTetap->tahun = $modelData->th_spt;
            if (!$modelTetap->save()) {
                $transaction->rollback();
                echo json_encode(ActiveForm::validate($modelTetap));
                die();
            }

            $modelData->id_spt = $modelSpt->id_spt;
            $modelData->id_tetap= $modelTetap->id_tetap;
            $modelData->jml_pok = str_replace('.', '', $modelData->jml_pr);
            $modelData->jml_pr_awal = str_replace('.', '', $modelData->jml_pr);
            $modelData->jml_pr = str_replace('.', '', $modelData->jml_pr);
            $modelData->nilai = str_replace('.', '', $modelData->nilai);
            $files = UploadedFile::getInstance($modelData, 'upload_file');
            if($_POST['TData']['kd_ayt'] == '1108'){
                $modelData->id_ayt = TAyat::find()->where("jn_ayt::int = 0 and kl_ayt::int = 0 and kd_ayt='" . $_POST['TData']['kd_ayt'] . "' and tahun=" . $modelData->th_spt)->one()->id_ayt;
                $modelData->judul = $_POST['TData']['judul'];
            } 

            if ($files != null) {
                $modelData->upload_file = $modelData->no_data . '-' . $modelData->th_spt . '.' . $files->extension;
            } else {
                $modelData->upload_file = null;
            }
            if ($modelData->save(false)){  
                if($_POST['TData']['kd_ayt'] == '1108'){
                    $dataAirTanah = filter_input(INPUT_POST, 'row', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
                    TDataAirTanah::deleteAll("th_spt='$modelData->th_spt' and no_data=" . $modelData->no_data);

                    foreach ($dataAirTanah as $key) {
                        $data = explode('_', $key);
                        $model = new TDataAirTanah();
                        if ((int) $key[0] != 0) {
                            $model->id = $data[0];
                        }
                        $model->no_data = $modelData->no_data;
                        $model->th_spt = $modelData->th_spt;
                        $model->id_ayt = $data[1];
                        $model->volume = $data[2];
                        $model->npa = str_replace('.', '', $data[3]);
                        //var_dump($model->npa);die;
                        $model->jumlah = $data[4];
                        if (!$model->save()) {
                            $transaction->rollback();
                            echo json_encode(ActiveForm::validate($model));
                        }
                    }
                }

                if ($files != null) {
                    if (!file_exists('uploads/')) {
                        mkdir('uploads/', 0777, true);
                    }
                    $files->saveAs('uploads/' . $modelData->upload_file);
                }
                $tmax = TNomorMax::findOne(['tahun' => $modelData->th_spt]);

                
                if ($tmax == null) {
                    $no = new TNomorMax();
                    $no->no_data = $modelData->no_data;
                    $no->id_lok = 0;
                    $no->id_res = 0;
                    $no->id_hot = 0;
                    $no->id_rek = 0;
                    $no->no_tetap = 0;
                    $no->no_setor = 0;
                    $no->no_tegur = 0;
                    $no->no_paksa = 0;
                    $no->no_mohon = 0;
                    $no->tahun = $modelData->th_spt;
                    $no->save();
                } else {
                    $tmax->no_data = $modelData->no_data;
                    $tmax->update();
                }

                $transaction->commit();
                echo 1;
            }else {
                echo json_encode(ActiveForm::validate($modelData));
            }
        } catch (Exception $ex) {
            echo json_encode($ex);
        }
    }
    //end save pendataan air tanah

    public function actionSimpan() {
        $transaction = \Yii::$app->db->beginTransaction();
        $kd_ayt = filter_input(INPUT_POST, 'kd_ayt');
        try {
            $model = new TData();
            $modelSpt = new TSpt();
            $modelDaftar = new TDaftar();
            $modelAyat = new TAyat();
            $modelTetap = new TTetap();
            $model->load(Yii::$app->request->post());
            $modelSpt->load(Yii::$app->request->post());
            $modelDaftar->load(Yii::$app->request->post());
            $modelAyat->load(Yii::$app->request->post());
            //simpan SPT
            $modelSpt->thn = $model->th_spt;
            $modelSpt->npwpd = str_replace('.', '', $modelSpt->npwpd);
            $modelSpt->masaspt = substr($modelSpt->tgl_awal, -4) . substr($modelSpt->tgl_awal, 3, 2);
            $modelSpt->no_spt = substr($modelSpt->npwpd, 2, 7);
            $modelSpt->tg_data = date('Y-m-d', strtotime($modelSpt->tg_data));
            $modelSpt->tg_terima = $modelSpt->tg_data;
            $modelSpt->tgl_awal = date('Y-m-d', strtotime($modelSpt->tgl_awal));
            $modelSpt->tgl_akhir = date('Y-m-d', strtotime($modelSpt->tgl_akhir));
            if (!$modelSpt->save()) {
                print_r(ActiveForm::validate($modelSpt));
                $transaction->rollBack();
                die;
            }
            //simpan tetap
            $modelTetap->tg_jtempo = $this->hitungJatuhTempo($modelSpt->tg_data);
            $modelTetap->sisa_pajak = str_replace('.', '', $model->jml_pr);
            $modelTetap->tahun = $model->th_spt;
            if (!$modelTetap->save()) {
                print_r(ActiveForm::validate($modelTetap));
                $transaction->rollBack();
                die;
            }
            $model->jml_pok = str_replace('.', '', $model->jml_pr);
            $model->jml_pr = str_replace('.', '', $model->jml_pr);
            if ($kd_ayt == '1104') {
                $model->nilai = str_replace('.', '', filter_input(INPUT_POST, 'nilai'));
                $model->panjang = str_replace(',', '.', $model->panjang);
                $model->lebar = str_replace(',', '.', $model->lebar);
                $model->luas = str_replace(',', '.', $model->luas);
                $model->kenaikan = str_replace(',', '.', $model->kenaikan);
                $model->tinggi = str_replace(',', '.', $model->tinggi);
                $model->banyak = $model->jumlah;
            } else if ($kd_ayt == '1108') {
                $model->nilai = str_replace('.', '', $model->nilai);
                $model->jumlah = str_replace(',', '.', $model->nilai);
            }
            $model->id_spt = $modelSpt->id_spt;
            $model->id_tetap = $modelTetap->id_tetap;
            $model->id_ayt = filter_input(INPUT_POST, 'id_ayt');
            $model->flag = '2';
            $model->no_data = TNomorMax::findOne(['tahun' => $model->th_spt])->no_data + 1;
            if (!$model->save()) {
                print_r(ActiveForm::validate($model));
                $transaction->rollBack();
                die;
            } else {
                $tmax = TNomorMax::findOne(['tahun' => $model->th_spt]);
                if ($tmax == null) {
                    $no = new TNomorMax();
                    $no->no_data = $model->no_data;
                    $no->id_lok = 0;
                    $no->id_res = 0;
                    $no->id_hot = 0;
                    $no->id_rek = 0;
                    $no->no_tetap = 0;
                    $no->no_setor = 0;
                    $no->no_tegur = 0;
                    $no->no_paksa = 0;
                    $no->no_mohon = 0;
                    $no->tahun = $model->th_spt;
                    $no->save();
                } else {
                    $tmax->no_data = $model->no_data;
                    $tmax->update();
                }
            }
            $transaction->commit();
            echo $model->id_tetap;
        } catch (Exception $ex) {
            $transaction->rollBack();
            echo json_encode($ex);
        }
    }

    public function actionSimpanPendataanReklame(){
      $transaction = \Yii::$app->db->beginTransaction();

      try{
        $modelSpt = new TSpt();
        $modelTetap = new TTetap();
        $modelData = new TData();
        $modelDataReklame = new TDataReklame();
        $modelDaftar = new TDaftar();
        $modelAyat = new TAyat();

        $modelData->attributes = filter_input(INPUT_POST, 'TData', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $modelDataReklame->attributes = filter_input(INPUT_POST, 'TDataReklame', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $modelSpt->attributes = filter_input(INPUT_POST, 'TSpt', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $modelDaftar->attributes = filter_input(INPUT_POST, 'TDaftar', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $modelAyat->attributes = filter_input(INPUT_POST, 'TAyat', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        $hasilSimpanSpt = $this->simpanSpt($modelSpt, $modelData, $modelDaftar, $transaction);

        $hasilSimpanTetap = $this->simpanTetap($modelTetap, $modelSpt, $modelData, $transaction);

        $modelData->id_spt = $hasilSimpanSpt->id_spt;
        $modelData->id_tetap = $hasilSimpanTetap->id_tetap;
        $this->simpanPendataan($modelData, $transaction);
        $this->simpanPendataanReklame($modelData, $modelAyat, $modelDataReklame, $transaction);

        $tmax = TNomorMax::findOne(['tahun' => $modelData->th_spt]);
        if ($tmax == null) {
            $no = new TNomorMax();
            $no->no_data = $modelData->no_data;
            $no->id_lok = 0;
            $no->id_res = 0;
            $no->id_hot = 0;
            $no->id_rek = 0;
            $no->no_tetap = 0;
            $no->no_setor = 0;
            $no->no_tegur = 0;
            $no->no_paksa = 0;
            $no->no_mohon = 0;
            $no->tahun = $modelData->th_spt;
            $no->save();
        } else {
            $tmax->no_data = $modelData->no_data;
            $tmax->update();
        }

        $transaction->commit();
      }catch(Exception $e){
        $transaction->rollback();
      }
    }

    public function simpanSpt($modelSpt, $modelData, $modelDaftar, $transaction) {
        $modelSpt->thn = $modelData->th_spt;
        $modelSpt->npwpd = str_replace('.', '', $modelSpt->npwpd);
        $modelSpt->masaspt = substr($modelSpt->tgl_awal, -4) . substr($modelSpt->tgl_awal, 3, 2);
        $modelSpt->no_spt = $modelDaftar->no_pokok;//substr($modelSpt->npwpd, 2, 7);
        $modelSpt->tg_data = date('Y-m-d', strtotime($modelSpt->tg_data));
        $modelSpt->tg_terima = $modelSpt->tg_data;
        $modelSpt->tgl_awal = date('Y-m-d', strtotime($modelSpt->tgl_awal));
        $modelSpt->tgl_akhir = date('Y-m-d', strtotime($modelSpt->tgl_akhir));
        if ($modelSpt->save()) {
            return $modelSpt;
        }else{
          echo json_encode(ActiveForm::validate($modelSpt));
        }
    }

    public function simpanTetap($modelTetap, $modelSpt, $modelData, $transaction) {

        //$modelTetap->tg_terima = date('Y-m-d', strtotime($modelSpt->tg_data));
        //$modelTetap->tg_tetap = date('Y-m-d', strtotime($modelSpt->tg_data));
        //$modelTetap->tg_setor = date('Y-m-d', strtotime($modelSpt->tg_data));
        $modelTetap->tg_jtempo = $this->hitungJatuhTempo($modelSpt->tg_data);
        $modelTetap->sisa_pajak = str_replace(',', '.', str_replace('.', '', $modelData->jml_pr));
        $modelTetap->tahun = $modelData->th_spt;
        if ($modelTetap->save()) {
            return $modelTetap;
        }else{
          echo json_encode(ActiveForm::validate($modelTetap));
        }
    }

    private function hitungJatuhTempo($tgl) {
        $tglJTempo = strtotime('+30 day', strtotime(date("Y-m-d", strtotime($tgl))));
        $tglJTempo1 = date('Y-m-d', $tglJTempo);
        return $tglJTempo1;
    }

    public function simpanPendataan($modelData, $transaction) {
      print_r($modelData);
        $modelData->jml_pok = (string) str_replace(',', '.', str_replace('.', '', $modelData->jml_pr));
        $modelData->jml_pr_awal = (float) str_replace(',', '.', str_replace('.', '', $modelData->jml_pr_awal));
        $modelData->jml_pr = (string) str_replace(',', '.', str_replace('.', '', $modelData->jml_pr));
        $modelData->id_ayt = TAyat::findOne(["tahun" => (int)$modelData->th_spt,"kd_ayt"=>'1104', "jn_ayt"=> '00',"kl_ayt"=>'00'])->id_ayt;
        $modelData->nip = $modelData->nip;
        $modelData->createdby = Yii::$app->user->identity->id;
        if ($modelData->save()) {
            return $modelData;
        }else{
          echo json_encode(ActiveForm::validate($modelData));
        }
    }

    public function simpanPendataanReklame($modelData, $modelAyat, $modelDataReklame, $transaction){
        try {
            for ($i=0; $i < count($modelDataReklame->panjang); $i++) {
                $modelDataReklame1 = new TDataReklame();

                $modelDataReklame1->th_spt = $modelData->th_spt;
                $modelDataReklame1->no_data = $modelData->no_data;
                $modelDataReklame1->id_ayt = (int) $modelDataReklame['id_ayt'][$i];
                $modelDataReklame1->jml_rek = (int) str_replace(',', '.', $modelDataReklame['jml_rek'][$i]);
                $modelDataReklame1->panjang = (float) str_replace(',', '.', $modelDataReklame['panjang'][$i]);
                $modelDataReklame1->lebar = (float) str_replace(',', '.', $modelDataReklame['lebar'][$i]);
                $modelDataReklame1->luas = (float) str_replace(',', '.', $modelDataReklame['luas'][$i]);
                $modelDataReklame1->tinggi = (float) str_replace(',', '.', $modelDataReklame['tinggi'][$i]);
                $modelDataReklame1->sdt_pandang = (float) str_replace(',', '.', $modelDataReklame['sdt_pandang'][$i]);
                $modelDataReklame1->hari = (int) $modelDataReklame['hari'][$i];
                $modelDataReklame1->lokasi = (float) str_replace(',', '.', $modelDataReklame['lokasi'][$i]);
                $modelDataReklame1->nilai_media = (float) str_replace(',', '.', str_replace('.', '', $modelDataReklame['nilai_media'][$i]));
                $modelDataReklame1->nilai_strategis = (float) str_replace(',', '.', str_replace('.', '', $modelDataReklame['nilai_strategis'][$i]));
                $modelDataReklame1->nilai_sewa = (float) str_replace(',', '.', str_replace('.', '', $modelDataReklame['nilai_sewa'][$i]));
                $modelDataReklame1->pajak = (float) str_replace(',', '.', str_replace('.', '', $modelDataReklame['pajak'][$i]));
                if ((int) $modelDataReklame['p_rokok'][$i] == (int) $modelAyat->tarifpr) {
                    $modelDataReklame1->p_rokok = (int) $modelAyat->tarifpr;
                }else{
                    $modelDataReklame1->p_rokok = (float) str_replace(',', '.', $modelDataReklame['p_rokok'][$i]);
                }
                if($modelDataReklame1->save()){
                    continue;
                }else{
                    $transaction->rollback();
                    echo '5';
                    echo CActiveForm::validate($modelDataReklame1);
                }
            }
            $transaction->commit();
            echo '1';
            //Yii::app()->end();
        } catch (Exception $e) {
            // $transaction->rollback();
            echo '6';
            echo $e->getMessage();
            //Yii::app()->end();
        }
    }


    public function actionTetapkan() {
        try {
            $id_tetap = filter_input(INPUT_POST, 'id_tetap');
            $tetap = TTetap::findOne(['id_tetap' => $id_tetap]);
            $tetap->tg_tetap = date('Y-m-d h:i:s');
            $tetap->no_tetap = $this->no_tetap(date('Y'));
            $tetap->tg_jtempo = date('Y-m-d h:i:s', strtotime($this->hitungJatuhTempo(date('d-m-Y'))));
            $tetap->st_tegur = '1';
            $tetap->st_stpd = '1';
            $tetap->st_lain = '1';
            if (!$tetap->save()) {
                print_r(ActiveForm::validate($tetap));
                die;
            } else {
                $data = TData::findOne(['id_tetap' => $id_tetap]);
                $data->st = '2';
                $data->update();
            }
            echo 1;
        } catch (Exception $ex) {
            print_r(ActiveForm::validate($tetap));
        }
    }

    public function no_tetap($y) {
        $no = $this->cekTNomorMax($y);
        $no->no_tetap = $no->no_tetap + 1;
        $no->save();
        return $no->no_tetap;
    }

    public function cekTNomorMax($x) {
        $no = TNomorMax::findOne(['tahun' => $x]);
        if ($no == null) {
            $no = new TNomorMax();
            $no->no_data = 0;
            $no->id_lok = 0;
            $no->id_res = 0;
            $no->id_hot = 0;
            $no->id_rek = 0;
            $no->no_tetap = 0;
            $no->no_setor = 0;
            $no->no_tegur = 0;
            $no->no_paksa = 0;
            $no->no_mohon = 0;
            $no->tahun = $x;
        }
        return $no;
    }

    public function actionMenucetakskpd() {
        $session = Yii::$app->session;
        $modelData = new TData();
        $modelAyat = new TAyat();
        $modelTetap = new TTetap();
        $modelDaftar = new TDaftar();
        
        //$dataSkpd2 = $modelDaftar->search(Yii::$app->request->queryParams);
        // $session->set('kd_ayt', null);
        // $session->set('tg_tetap', null);\
        $dataSkpd = $modelTetap->searchSkpd();
        $dataSkpd->pagination->pageSize = 10;
        return $this->render('menu_cetak', [
                    'modelData' => $modelData,
                    'modelAyat' => $modelAyat,
                    'modelTetap' => $modelTetap,
                    'modelDaftar' => $modelDaftar,
                    'dataSkpd' => $dataSkpd
                    // 'content' => $content
        ]);
    }

    
    public function actionResetGridCetakSkpd(){
        $session = Yii::$app->session;
        $tg_tetap_awal = filter_input(INPUT_POST, 'tg_tetap_awal');
        $tg_tetap_akhir = filter_input(INPUT_POST, 'tg_tetap_akhir');
        $kd_ayt = filter_input(INPUT_POST, 'kd_ayt');

        $no_tetap = filter_input(INPUT_POST, 'no_tetap');
        $no_tetap_akhir = filter_input(INPUT_POST, 'no_tetap_akhir');
        // var_dump($tg_tetap);
        //     die();
        if (!empty($tg_tetap_awal)) {
            $session->set('tg_tetap_awal', $tg_tetap_awal);
        } else {
            $session->set('tg_tetap_awal', null);
        }
        if (!empty($tg_tetap_akhir)) {
            $session->set('tg_tetap_akhir', $tg_tetap_akhir);
        } else {
            $session->set('tg_tetap_akhir', null);
        }
        if (!empty($kd_ayt)) {
            $session->set('kd_ayt', $kd_ayt);
        } else {
            $session->set('kd_ayt', null);
        }
        if (!empty($no_tetap)) {
            $session->set('no_tetap', $no_tetap);
        } else {
            $session->set('no_tetap', null);
        }
        if (!empty($no_tetap_akhir)) {
            $session->set('no_tetap_akhir', $no_tetap_akhir);
        } else {
            $session->set('no_tetap_akhir', null);
        }
        echo 1;
        // var_dump($_SESSION);die;
    }

    public function actionCetakskpd($tahun,$id_spt,$kd_ayt) {
        $jasperPHP = new JasperPHP();
        $pathImage = Yii::$app->basePath . '/' . '../themes/images/';
        //$path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpd.jasper';
        $pathSubReport = Yii::$app->basePath . '/' . '../reports/CetakSkpd/';
        $tgl_denda = date('d-m-Y');
        
        if($kd_ayt == '1109'){
            $path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpdWalet.jasper';
        } else if ($kd_ayt == '1108'){
            $path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpdAirTanah.jasper';
        } else if ($kd_ayt == '1106'){
            $path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpdGalianc.jasper';
        } else if ($kd_ayt == '1104'){
            $path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpdReklame.jasper';
        } else {
            $path = Yii::$app->basePath . '/' . '../reports/CetakSkpd/CetakSkpdOmset.jasper';
        }
        
        // $nomor_bayar = TData::findOne(['id_tetap' => $id]);

        // QrCode::png($nomor_bayar['nmr_bayar'], Yii::$app->basePath . '/' . '../uploads/qrcode/' . $nomor_bayar['nmr_bayar'] . '.png', 0, 3, 4, false);
        // if ($kd_ayt != null) {
        //     if($kd_ayt == '1104'){
        //         $sql .= " AND (i.kd_ayt = '$kd_ayt' OR ii.kd_ayt = '$kd_ayt')";
        //     }else{
        //         $sql .= " AND i.kd_ayt = '$kd_ayt'";
        //     }
        // }

        $param = array(
            "tahun" => $tahun,
            "IMAGE_DIR" => $pathImage,
            //"sql" => $sql,
            "SUBREPORT_DIR" => $pathSubReport,
            "id_spt" => (int) $id_spt,
            "tgl_denda" => $tgl_denda,

            //"qrcode" => Yii::$app->basePath . '/' . '../uploads/qrcode/' . $nomor_bayar['nmr_bayar'] . '.png',
        );
        // var_dump($param);die;
        // var_dump($path);die;
        $this->cetak($path, $param, true, 'skpd-'.$id_spt);
    }

    public function actionGetKodeRekeningSearch() {
        $tahun = filter_input(INPUT_POST, 'tahun');
        $status = filter_input(INPUT_POST, 'status');
        $kd_rek = filter_input(INPUT_POST, 'kd_rek');
        try {
            $sql = $this->queryGetData($tahun, $status, $kd_rek);
            $default = array(array("id" => 0, "text" => "Pilih Kode Rekening"));
            $connection = \Yii::$app->db;
            $model = $connection->createCommand($sql);
            $data = $model->queryAll();
            //print_r($data);die;

            $merged = array_merge($default, $data);
            if ($data != null) {
                echo json_encode($merged);
            } else {
                echo '1';
            }
        } catch (Exception $ex) {
            echo $ex;
        }
        //Yii::app()->end();
    }

    public function actionGetDetailAyat() {
        $id = (int) filter_input(INPUT_POST, 'id_ayt');
        try {
            $data = TAyat::find()->where(['id_ayt' => $id])->asArray()->one();
            if ($data != null) {
                echo json_encode($data);
            } else {
                echo '1';
            }
        } catch (Exception $ex) {
            echo $ex;
        }
        //Yii::app()->end();
    }

    private function queryGetData($tahun, $status, $kd_rek) {
        return "SELECT id_ayt AS id, kd_ayt||jn_ayt||kl_ayt||' - '||nm_ayt AS text FROM pad.t_ayat WHERE tahun = '$tahun' AND kd_ayt = '$kd_rek' AND status = '$status' AND jn_ayt <> '00' ORDER BY kd_ayt,jn_ayt,kl_ayt";
    }
    
    /**
     * Updates an existing TData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $th_spt
     * @param integer $no_data
     * @return mixed
     */
//    public function actionUpdate($th_spt, $no_data)
//    {
//        $model = $this->findModel($th_spt, $no_data);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'th_spt' => $model->th_spt, 'no_data' => $model->no_data]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing TData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $th_spt
     * @param integer $no_data
     * @return mixed
     */
//    public function actionDelete($th_spt, $no_data)
//    {
//        $this->findModel($th_spt, $no_data)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the TData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $th_spt
     * @param integer $no_data
     * @return TData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($th_spt, $no_data)
//    {
//        if (($model = TData::findOne(['th_spt' => $th_spt, 'no_data' => $no_data])) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
//    }
}
