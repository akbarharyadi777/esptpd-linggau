<?php
/* @var $this View */
/* @var $content string */

use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use app\modules\pad\models\TKukuh;
use app\modules\pad\models\TDaftar;
use app\modules\management\models\User;
use app\modules\management\components\GhostNav;
use app\modules\management\components\GhostHtml;

AppAsset::register($this);
?>
<style>
    #spinner
    {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?= Yii::getAlias('@images'); ?>/ajax-loader.gif) 50% 50% no-repeat #ede9df;
    }
</style>
<?php
//AdminLteAsset::register($this);
if (Yii::$app->user->isGuest) {
    ?>
    <?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language; ?>">
        <head>
            <meta charset="<?= Yii::$app->charset; ?>"/>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::getAlias('@images'); ?>/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-16x16.png">
            <link rel="manifest" href="<?= Yii::getAlias('@images'); ?>/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
            <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css">
            <?= Html::csrfMetaTags(); ?>
            <title><?= Html::encode($this->title); ?></title>
            <?php $this->head(); ?>
        </head>
        <body class="login-page">
            <div id="spinner"></div>
            <?php $this->beginBody(); ?>
            <?= $content; ?>
            <?php $this->endBody(); ?>
        </body>
    </html>
    <?php
    $this->endPage();
} else {
    $user = User::findOne(['id' => Yii::$app->user->identity->id]); ?>
    <?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language; ?>">
        <head>
            <meta charset="<?= Yii::$app->charset; ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags(); ?>
            <title><?= Html::encode($this->title); ?></title>
            <?php $this->head(); ?>
            <!--favicon-->
            <link rel="apple-touch-icon" sizes="57x57" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="<?= Yii::getAlias('@images'); ?>/favicon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="<?= Yii::getAlias('@images'); ?>/favicon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::getAlias('@images'); ?>/favicon/favicon-16x16.png">
            <link rel="manifest" href="<?= Yii::getAlias('@images'); ?>/favicon/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <style>
                #navbar .panel-heading {
                    color: #286403;
                    background: #69df21;
                    border-bottom: 1px solid #95f55b;
                    box-shadow: 0 1px 0 0 #16597f;
                    margin: 0 0 1px 0;
                    font-size: 15px;
                }

                .panel-default {
                    border-color: transparent;
                }
                .panel {
                    margin-bottom: 0px;
                    background-color: transparent   ;
                    border: 0px transparent;
                    border-radius: 0px;
                    -webkit-box-shadow: 0 1px 0px rgba(0, 0, 0, .0);
                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0);
                }   
                .input-group-addon {
                    border-radius: 0px;
                    border-color: #d2d6de;
                    color: #000;
                    background-color: #ededed !important;
                }
                .btn-flat {
                    padding: 9px 12px !important;
                }
                
                table {
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif  !important;
                    font-size: 12px  !important;
                    line-height: 1.42857143  !important;
                    color: #333  !important;
                    background-color: #fff !important;
                }
                td {
                    vertical-align:middle  !important;
                }
            </style>
        </head>
        <body class="wysihtml5-supported fixed skin-blue">

            <?php $this->beginBody(); ?>
            <div class="wrapper">

                <header class="main-header">
                    <a id="image" class="logo" href="<?= Url::to(Yii::$app->urlManager->createUrl(['/site/index'])); ?>">
                        <img src="<?= Yii::getAlias('@images'); ?>/logo-e-sptpd.png">
                    </a>
                    <nav class="navbar navbar-static-top" role="navigation">
                        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" onclick="gantiImage()">
                            <span class="sr-only">Toggle navigation</span>
                        </a>
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <?php if (Yii::$app->user->isSuperadmin == true) {
        ?>
                                    <li class="dropdown messages-menu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="hidden-xs">Management User &nbsp;</span> <i class="fa fa-gears"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <ul class="menu">
                                                    <li>
                                                        <?=
                                                        Html::a('<h4> User </h4>', [
                                                            '/user-management/user/index',
                                                        ]); ?>
                                                    </li>
                                                    <li>
                                                        <?=
                                                        Html::a('<h4> Role </h4>', [
                                                            '/user-management/role/index',
                                                        ]); ?>
                                                    </li>
                                                    <li>
                                                        <?=
                                                        Html::a('<h4> Permission </h4>', [
                                                            '/user-management/permission/index',
                                                        ]); ?>
                                                    </li>
                                                    <li>
                                                        <?=
                                                        Html::a('<h4> Permission Group </h4>', [
                                                            '/user-management/auth-item-group/index',
                                                        ]); ?>
                                                    </li>
                                                    <li>
                                                        <?=
                                                        Html::a('<h4> Last Visit </h4>', [
                                                            '/user-management/user-visit-log/index',
                                                        ]); ?>
                                                    </li>
													<li>
                                                        <?=
                                                        Html::a('<h4> Generate User </h4>', [
                                                            '/site/generate-user',
                                                        ]); ?>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                <?php
    } ?>
                                <!--<li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-envelope-o"></i>
                                        <span id="notification_count" class="label label-success"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">Status SPTPD</li>
                                        <li>
                                            <ul id="notifikasi" class="menu">
                                            </ul>
                                        </li>
                                    </ul>
                                </li>-->
                                <!-- <li class="dropdown messages-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="hidden-xs">Cetak &nbsp;</span> <i class="fa fa-file-pdf-o"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <ul class="menu" style="height: 100% !important;">
                                                <li>
                                                    <?//=
                                                    //GhostHtml::a('<h4> Cetak Sptpd</h4> ', [
                                                        //'/pad/pendataan/menucetaksptpd'
                                                    //])
                                                    ?>
                                                </li>
                                                <li>
                                                    <?//=
                                                    //GhostHtml::a('<h4> Cetak Skpd</h4> ', [
                                                        //'/pad/pendataanofficial/menucetakskpd'
                                                    //])
                                                    ?>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li> -->
                                <li class="dropdown user user-menu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?= Yii::getAlias('@images'); ?>/user-icon.png" class="user-image" alt="User Image"/>
                                        <span class="hidden-xs"><?= $user->username; ?></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="user-header">
                                            <img src="<?= Yii::getAlias('@images'); ?>/user-icon-lg.png" class="img-circle" alt="User Image" />
                                            <p>
                                            <?php
                                                $modelDaftar = TDaftar::findOne(['npwpd' => $user->username]);
    $nm_wp = '';
    if (!empty($modelDaftar)) {
        $nm_wp = $modelDaftar->nm_wp;
    } ?>
                                                <?= $nm_wp; ?><br>
                                                <?= $user->username; ?>
                                                <small>Register pada <?= date('F'.' '.'Y', $user->created_at); ?></small>
                                            </p>
                                        </li>
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <?=
                                                Html::a('Ubah password', [
                                                    '/user-management/auth/change-own-password',
                                                        ], ['class' => 'btn btn-default btn-flat']); ?>
                                            </div>
                                            <div class="pull-right">
                                                <?=
                                                Html::a('Sign out', [
                                                    '/user-management/auth/logout',
                                                        ], ['class' => 'btn btn-default btn-flat']); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
                <aside class="main-sidebar">
                    <section class="sidebar">
                        <div id="navbar">
                            <?php
                            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
    $kukuh = TKukuh::find()->where(['npwpd' => $user->username])->one();
    echo GhostNav::widget([
                                'heading' => 'MAIN MENU<div class="pull-right"><i class="fa fa-angle-double-right"></i></div>',
                                'options' => ['class' => 'sidebar-menu'],
                                'items' => [
                                    [
                                        'url' => ['/pad/pendataan/create-hotel'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'label' => 'Rekam Pajak Hotel',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_hotel == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Restoran',
                                        'url' => ['/pad/pendataan/create-restoran'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_restoran == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Hiburan',
                                        'url' => ['/pad/pendataan/create-hiburan'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_hiburan == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Penerangan Jalan',
                                        'url' => ['/pad/pendataan/create-ppj'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_ppj == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Mineral Bukan Logam dan batuan',
                                        'url' => ['/pad/pendataan/create-mblb'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_mblb == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Parkir',
                                        'url' => ['/pad/pendataan/create-parkir'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_parkir == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Rekam Pajak Sarang Burung Walet',
                                        'url' => ['/pad/pendataan/create-walet'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                        'visible' => (empty($kukuh) ? Yii::$app->user->isSuperadmin : ($kukuh->st_pjk_sbw == '0' ? false : true)),
                                    ],
                                    [
                                        'label' => 'Cetak SPTPD',
                                        'url' => ['/pad/pendataan/list-sptpd'],
                                        'template' => '<a href="{url}">
                                                    <i class="fa fa-chevron-circle-right"></i> <span>{label}</span>
                                                </a>',
                                    ],
                                ],
                            ]); ?>  
                        </div>
                    </section>
                </aside>
                <div class="content-wrapper">
                    <!--<section class="content-header">
                       <br /><br /><br />
                        <h3>Badan Pengelolaan Pajak dan Retribusi Daerah</h3>
                        <h1>Pemerintah Kabupaten Mimika</h1>
                    </section>-->
                    <section class="content">
                        <?= $content; ?>
                    </section>
                </div>

                <footer class="main-footer">

                    <small>e-SPTPD | Copyright © 2016</small>
                </footer>

            </div>
            <div id="spinner"></div>
            <?php $this->endBody(); ?>
        </body>
        <?php
        Modal::begin([
            'id' => 'modal-pop',
            'header' => '',
        ]); ?>
            <div class="modal-content">    
                <hr style="border-color: #c7c7c7;">
                <div class="modal-body">
                </div>
            </div>
        <?php
        Modal::end(); ?>
    </html>
    <?php
    $this->endPage();
}?>