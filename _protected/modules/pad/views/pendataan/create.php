<?php

use yii\helpers\Html;
use quangvule\PDFjs\Widget;

/* @var $this yii\web\View */
/* @var $model app\modules\pad\models\TDataSelf */
?>
<div class="tdata-self-create">
    <?=
    $this->render($form, [
        'model' => $model,
        'modelDaftar' => $modelDaftar,
        'id' => $id,
        'searchAyat' => $searchAyat,
        'dataAyat' => $dataAyat,
    ])
    ?>
    </div>
<div class="box box-widget" id="preveiw_pdf">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue">Preview Data SPTPD</h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <div class="col-md-11" id="canvas_preview">
        <?= Widget::widget(['url' => '/e-sptpd/reports/previewSptpd/loading.pad']); ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <?= Html::button(' Kembali', ['id' => 'btn_preview2', 'class' => 'btn btn-success']) ?>
                <?= Html::button($model->isNewRecord ? ' Simpan' : ' Edit', ['id' => 'btn_simpan', 'class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<<JS
        
$(document).ready(function(){
    $('#preveiw_pdf').hide();
    $('.content-wrapper').css('min-height', '100%');
});

$("#btn_preview, #btn_preview2").click(function(){
    if($('#from_input').css('display') == 'none'){
        PDFView.open('/e-sptpd/reports/previewSptpd/loading.pad');
        $("#from_input").fadeIn('fast');
        $("#preveiw_pdf").fadeOut('fast');
        $('html, body').animate({scrollTop: $('.content').offset().top}, 'slow');
    } else { 
        $("#from_input").fadeOut('fast');
        $("#preveiw_pdf").fadeIn('fast');
        console.log($('#from_input').css('display'));
        //$('#canvas_preview').html('');
        //$('#canvas_preview').html('<i class=\"fa fa-spinner fa-spin\" style="text-align:center;"></i>');
        $('html, body').animate({scrollTop: $('.content').offset().top}, 'slow');
        var dataform = new FormData($("#form-pendataan")[0]);
        $.ajax({
            type: "POST",
            url: "preview",
            data: dataform,
            processData: false,
            contentType: false,
            success: function (data) {
                if (parseInt(data) !== 0){
                    PDFView.open(data);
                } else {
                    bootbox.dialog({
                        message: "Gagal memuat data!",
                        title: "Peringatan!",
                        buttons: {
                            success: {
                            label: "Tutup",
                            className: "btn-success"
                                }
                            }
                        });
                }
            }
        });
    }
});

$("#tdataself-nilai").maskMoney({thousands: '.', decimal: ',', precision: '0'});
$.ajax({
        url: "getwp",
        data: 'no_daftar=' + $('#tdataself-no_spt').val(),
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            if (parseInt(data) === 1) {
                $('#tdataself-no_spt').val('');
            } else {
                //console.log(data);
                $('#tdataself-npwpd').val(data.npwpd);
                $('#tdaftar-kel_wp').val(data.kelurahan);
                $('#tdaftar-kec_wp').val(data.kecamatan);
                $('#tdaftar-nm_wp').val(data.nm_wp);
                $('#tdaftar-alm_wp').val(data.alm_wp);
                $('#tdataself-no_spt').attr('readOnly', true);
            }
            dapatkanNoRegister();
        }
    });
        
$('#tdataself-no_spt').change(function () {
        $(this).val(padLeft($(this).val(), 7));
        $.ajax({
        url: "getwp",
        data: 'no_daftar=' + $('#tdataself-no_spt').val(),
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            if (parseInt(data) === 1) {
                bootbox.dialog({
            message: "No Pokok tidak ditemukan!",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Tutup",
                    className: "btn-success"
                }
            }
        });
            } else {
                //console.log(data);
                $('#tdataself-npwpd').val(data.npwpd);
                $('#tdaftar-kel_wp').val(data.kelurahan);
                $('#tdaftar-kec_wp').val(data.kecamatan);
                $('#tdaftar-nm_wp').val(data.nm_wp);
                $('#tdaftar-alm_wp').val(data.alm_wp);
            }
            dapatkanNoRegister();
        }
    });
});

$('#m_ayat').on('show.bs.modal', function () {
    var modal = $(this);
    modal.find('.modal-body').html('');
    modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
    $.ajax({
        url: "listrekening",
        data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": $('#kd_ayt').val()},
        type: 'POST',
        //dataType: 'json',
        success: function (data) {
            //console.log(data);
            modal.find('.modal-body').html(data);
        }
    });
});

$("#tdataself-tgl_awal").change(function () {
    //console.log($(this).val());
    var parts = $(this).val().split('-');
    var now = new Date(parts[2], parts[1] - 1, parts[0]);
    var lastDayOfTheMonth = new Date(1900 + now.getYear(), now.getMonth() + 1, 0);
    if ($(this).val() != '') {
        $("#tdataself-tgl_akhir").val($.datepicker.formatDate('dd-mm-yy', lastDayOfTheMonth));
    }
    dapatkanNoRegister();
});

$("#tdataself-nilai").change(function () {
    var omset = $("#tdataself-nilai").val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    if (tarif == '') {
        bootbox.dialog({
            message: "Anda belum memilih Rekening!",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Tutup",
                    className: "btn-success"
                }
            }
        });
    } else {
        if ($('#kd_ayt').val() == '1105') {
            var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
            var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else if ($('#kd_ayt').val() == '1106' || $('#kd_ayt').val() == '1111') {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
        } else {
            $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100))));
        }
    }
});

$('#tdataself-koef1, #tdataself-koef2').change(function () {
    var omset = $("#tdataself-nilai").val() == '' ? 1 : $('#tdataself-nilai').val().replace(/\./g, "");
    var tarif = $("#tdataself-tarifpr").val();
    var koef1 = $('#tdataself-koef1').val() == '' ? 1 : $('#tdataself-koef1').val().replace(/\,/g, ".");
    var koef2 = $('#tdataself-koef2').val() == '' ? 1 : $('#tdataself-koef2').val().replace(/\,/g, ".");
    if (tarif == '') {
        bootbox.dialog({
            message: "Anda belum memilih Rekening!",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Tutup",
                    className: "btn-success"
                }
            }
        });
    } else {
        $("#tdataself-jml_pjk").val(numerik(Math.round(parseFloat(omset) * (parseFloat(tarif) / 100) * parseFloat(koef1) * parseFloat(koef2) * parseFloat($('#tdataself-tarifrp').val().replace(/\./g, "")))));
    }
});

$('#tdataself-th_spt').change(function () {
    dapatkanNoRegister();
});

$('#btn_simpan').click(function () {
    bootbox.dialog({
        message: "Apa Anda yakin ingin memproses data ini?",
        title: "Peringatan!",
        buttons: {
            success: {
                label: "Ya",
                className: "btn-success",
                callback: function () {
                    $("#spinner").fadeIn("slow");
                    //var dataform = $("#form-pendataan").
                    var dataform = new FormData($("#form-pendataan")[0]);
                    $.ajax({
                        type: "POST",
                        url: "simpan",
                        data: dataform,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $("#spinner").fadeOut("slow");
                            if (parseInt(data) === 1) {
                                bootbox.dialog({
                                    message: "Proses data berhasil.",
                                    title: "Notifikasi!",
                                    buttons: {
                                        success: {
                                            label: "Tutup",
                                            className: "btn-success",
                                            callback: function () {
                                                $("#btn_simpan").attr('style', 'display:none');
                                                $("#form-pendataan :input").prop("disabled", true);
                                                //$(#form-pendataan :input").attr("disabled", true);
                                            }
                                        }
                                    }
                                });
                            } else {
                                bootbox.dialog({
                                    message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                    title: "Notifikasi!",
                                    buttons: {
                                        success: {
                                            label: "Tutup",
                                            className: "btn-success",
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            },
            danger: {
                label: "Tidak",
                className: "btn-warning",
                callback: function () {

                }
            }
        }
    });
});

function dapatkanNoRegister() {
    // $("#loader-noregister").show();
    $.ajax({
        url: "getregister",
        data: {"tahun": $('#tdataself-th_spt').val(), "kd_ayt": $('#kd_ayt').val(), "status": 1, "tgl_awal": $('#tdataself-tgl_awal').val(), "no_pokok": $('#tdataself-no_spt').val(), "npwpd": $('#TDataSelf_npwpd').val()},
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            $('#tdataself-nmr_bayar').val(data);
            // $("#loader-noregister").hide();
        }
    });
}

$('#tdataself-tgl_awal').keypress(function (e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode == 9) {
        e.preventDefault();
        $('#tdataself-tgl_akhir').focus();

    }
});


$('#tdataself-th_spt').change(function () {
    //if ($('#kd_ayt').val() == '1111' || $('#kd_ayt').val() == '1106') {
     //   if (parseInt($(this).val()) < 2016) {
       //     $('#kd_ayt').val('1111');
         //   $('#tdataself-kd_rek').parent().find('.input-group-addon').text('1111');
        //} else {
          //  $('#kd_ayt').val('1106');
            //$('#tdataself-kd_rek').parent().find('.input-group-addon').text('1106');
       // }
   // }
    dapatkanNoRegister()
});

JS;

$this->registerJs($js);
?>