<?php

namespace app\modules\pad\controllers;

use Yii;
use yii\helpers\Json;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\UploadedFile;
use app\modules\pad\models\TLurah;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TDaftarFile;
use app\modules\pad\models\MultipleUploadForm;

/**
 * PendaftaranController.
 */
class PendaftaranController extends Controller
{
    public function actionCreate()
    {
        $this->layout = 'main_register';
        $model = new TDaftar();
        $no_pokok = $model->getNoPokok();

        $model->no_pokok = $no_pokok;
        $model->tgl_kirim = date('Y-m-d');
        $model->tgl_kembali = date('Y-m-d');
        $model->no_daftar = $no_pokok.'/'.date('Y');
        $model->kota_pemilik = 'LUBUKLINGGAU';
        $msg = '';
        $uploadForm = new MultipleUploadForm();

        if (Yii::$app->request->isPost) {
            try {
                $transaction = Yii::$app->db->beginTransaction();
                $no_pokok = $model->getNoPokok();
                $model->load(Yii::$app->request->post());
                $model->no_pokok = $model->golongan.$no_pokok;
                $model->no_daftar = $no_pokok.'/'.date('Y');
                $model->active_inactive = '1';
                $model->st = '1';
                $model->npwpd = $model->pr.$model->golongan.$model->kec_wp.$model->kel_wp.$no_pokok;
                if ($model->save()) {
                    $uploadForm->files = UploadedFile::getInstances($uploadForm, 'files');
                    foreach ($uploadForm->files as $key => $file) {
                        $tdf = new TDaftarFile();
                        $tdf->npwpd = $model->npwpd;
                        $tdf->nama_file = Yii::$app->request->post()['nama_file'][$key];
                        $tdf->file = $file->extension;
                        if ($tdf->save()) {
                            $file->saveAs($tdf->getPath());
                        } else {
                            throw new Exception('Gagal menyimpan data ke table pendaftaran file.');
                        }
                    }
                } else {
                    throw new Exception('Gagal menyimpan data ke table pendaftaran.');
                }
                $transaction->commit();
                $this->redirect(\Yii::$app->urlManager->createUrl('/pad/pendaftaran/berhasil'));
            } catch (Exception $e) {
                $transaction->rollBack();
                $msg = $e->getMessage();
            }
        }

        return $this->render('create', ['model' => $model, 'msg' => $msg]);
    }

    public function actionBerhasil()
    {
        $this->layout = 'main_register';

        return $this->render('berhasil');
    }

    public function actionGetKelurahan()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $kec_id = $parents[0];
                $data = TLurah::find()->orderBy('kd_lurah asc')->where(['kd_camat' => $kec_id])->all();
                foreach ($data as $key) {
                    array_push($out, ['id' => $key->kd_lurah, 'name' => $key->kd_lurah.' - '.$key->nm_lurah]);
                }
                echo Json::encode(['output' => $out, 'selected' => '']);

                return;
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
    }

    public function actionDownloadFile($id)
    {
        $model = TDaftarFile::find()->where(['id' => $id])->one();
        $path = $model->getPath();
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path, $model->npwpd.'-'.$model->nama_file.'.'.$model->file);
        }
    }
}
