<?php

/**
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\management\models\rbacDB\Role $model
 */
use app\modules\management\UserManagementModule;

$this->title = UserManagementModule::t('back', 'Editing role: ') . ' ' . $model->name;
?>

<?=

$this->render('_form', [
    'model' => $model,
])
?>
	