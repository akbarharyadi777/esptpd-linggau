<?php

use webvimark\extensions\GridPageSize\GridPageSize;
use app\modules\management\components\GhostHtml;
use app\modules\management\models\rbacDB\AuthItemGroup;
use app\modules\management\models\rbacDB\Permission;
use app\modules\management\UserManagementModule;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\management\models\rbacDB\search\PermissionSearch $searchModel
 * @var yii\web\View $this
 */
$this->title = UserManagementModule::t('back', 'Permissions');
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <?php
    Pjax::begin([
        'id' => 'permission-grid-pjax',
    ])
    ?>

    <?=
    GridView::widget([
        'id' => 'permission-grid',
        'dataProvider' => $dataProvider,
        'pager' => [
            'options' => ['class' => 'pagination pagination-sm'],
            'hideOnSinglePage' => true,
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'options' => ['style' => 'width:10px']],
            [
                'attribute' => 'description',
                'value' => function($model) {
                    return Html::a($model->description, ['view', 'id' => $model->name], ['data-pjax' => 0]);
                },
                        'format' => 'raw',
                    ],
                    'name',
                    [
                        'attribute' => 'group_code',
                        'filter' => ArrayHelper::map(AuthItemGroup::find()->asArray()->all(), 'code', 'name'),
                        'value' => function(Permission $model) {
                            return $model->group_code ? $model->group->name : '';
                        },
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '{update} {delete}',
                        'contentOptions' => ['style' => 'width:70px; text-align:center;'],
                    ],
                ],
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'toolbar' => [
                    ['content' =>
                        Html::a('Buat Permission', ['create'], ['class' => 'btn btn-success pull-right'])
                    ],
                    '{toggleData}',
                ],
                'export' => [
                    'fontAwesome' => true
                ],
                'striped' => true,
                'bordered' => true,
                'condensed' => true,
                'responsiveWrap' => true,
                'perfectScrollbar' => true,
                'panel' => [
                    'type' => GridView::TYPE_SUCCESS,
                ],
                'persistResize' => false,
            ]);
            ?>

            <?php Pjax::end() ?>
</div>