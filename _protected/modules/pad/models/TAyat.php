<?php

namespace app\modules\pad\models;

use Yii;

/**
 * This is the model class for table "t_ayat".
 *
 * @property integer $id_ayt
 * @property string $kd_ayt
 * @property string $jn_ayt
 * @property string $kl_ayt
 * @property string $nm_ayt
 * @property double $tarifpr
 * @property double $tarifrp
 * @property string $ket_ayt
 * @property string $tanggal_ayt
 * @property integer $tahun
 * @property integer $th
 * @property integer $bl
 * @property double $id1
 * @property double $id2
 * @property integer $createdby
 * @property string $createdtime
 * @property integer $updatedby
 * @property string $updatedtime
 * @property integer $id_ayt_lama
 */
class TAyat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pad.t_ayat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id1', 'id2'], 'number'],
            [['tanggal_ayt', 'createdtime', 'updatedtime'], 'safe'],
            [['tahun', 'th', 'bl', 'createdby', 'updatedby', 'id_ayt_lama'], 'integer'],
            [['kd_ayt'], 'string', 'max' => 10],
            [['jn_ayt', 'kl_ayt'], 'string', 'max' => 4],
            [['nm_ayt'], 'string', 'max' => 100],
            [['ket_ayt'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_ayt' => 'Id Ayt',
            'kd_ayt' => 'Kd Ayt',
            'jn_ayt' => 'Jn Ayt',
            'kl_ayt' => 'Kl Ayt',
            'nm_ayt' => 'Nm Ayt',
            'tarifpr' => 'Tarifpr',
            'tarifrp' => 'Tarifrp',
            'ket_ayt' => 'Ket Ayt',
            'tanggal_ayt' => 'Tanggal Ayt',
            'tahun' => 'Tahun',
            'th' => 'Th',
            'bl' => 'Bl',
            'id1' => 'Id1',
            'id2' => 'Id2',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'id_ayt_lama' => 'Id Ayt Lama',
        ];
    }
}
