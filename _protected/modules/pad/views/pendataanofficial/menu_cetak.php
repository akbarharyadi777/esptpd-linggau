<?php

use app\modules\pad\models\TAyat;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TData;
use app\modules\pad\models\TSpt;
use app\modules\pad\models\TTetap;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use kartik\datecontrol\DateControl;
use yii\widgets\ListView;
use yii\helpers\Url;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Menu Cetak SKPD';
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

   
    <?php
        $form = ActiveForm::begin([
            'id' => 'cetak-skpd-form',
            'type' => ActiveForm::TYPE_VERTICAL,
            'options' => [
                'enctype' => 'multipart/form-data',
            ]
        ]);
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            Tanggal Ketetapan
                        </label>
                    </div>
                    <div class="col-md-3">
                        <?=  DateControl::widget([
                            //'model' => $model,
                            'name' => 'tg_tetap_awal',
                            'id' => 'tg_tetap_awal',
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            //'attribute' => 'tgl_terima_dokumen_wp',
                            //'language' => 'ru',
                            'options'=> [
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => ''
                                ]
                            ],
                        ]); 
                        ?>
                    </div>

                    <div class="col-md-1" style="width: 4.333333%"><span> s/d</span> </div>

                    <div class="col-md-3">
                        <?=  DateControl::widget([
                            //'model' => $model,
                            'name' => 'tg_tetap_akhir',
                            'id' => 'tg_tetap_akhir',
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'readonly' => true,
                            //'attribute' => 'tgl_terima_dokumen_wp',
                            //'language' => 'ru',
                            'options'=> [
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => ''
                                ]
                            ],
                        ]); 
                        ?>
                    
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            Jenis Pajak
                        </label>
                        <!-- <select name="TData[no_rekening]" id="no_rekening"><option value="">--Pilih Rekening--</option></select>&nbsp; -->
                    </div>
                    <div class="col-md-3">    
                         <?=
                            $form->field($modelAyat, 'kd_ayt')->dropDownList(
                            ArrayHelper::map(TAyat::find()
                            ->where("jn_ayt::int=0 and kl_ayt::int=0 and kd_ayt not in ('1111', '1112', '1113', '1114') and left(kd_ayt, 2) = '11'")
                            ->orderBy('kd_ayt')
                            ->asArray()
                            ->all(), 'kd_ayt', 'nm_ayt'),
                            ['prompt' => 'Pilih Jenis Pajak', 'id' => 'kd_ayt'])->label(false)
                        ?>

                         
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <label class="control-label">
                            No Kohir
                        </label>
                        <!-- <select name="TData[no_rekening]" id="no_rekening"><option value="">--Pilih Rekening--</option></select>&nbsp; -->
                    </div>
                    <div class="col-md-1">    
                         <?=
                            $form->field($modelTetap, 'no_tetap')->textInput(['id' => 'no_tetap', 'maxlength' => 7])->label(false)
                        ?>
                         
                    </div>
                    <div class="col-md-1" style="width: 4.333333%"><span>s/d</span></div>
                    <div class="col-md-1">    
                         <?=
                            $form->field($modelTetap, 'no_tetap_akhir')->textInput(['id' => 'no_tetap_akhir' ,'maxlength' => 7, 'readonly' => true])->label(false)
                        ?>
                         
                    </div>
                </div>
            </div>

                    <?php
                        ActiveForm::end();
                    ?>

                <div class="col-md-12">
                    <div class="form-group">
                    <!-- <div class="col-md-3"> -->
                        <!-- <a class="btn btn-success" id="tampilkan_data">Tampilkan</a> -->
                        <?= Html::button('Tampilkan', [
                                'class' => 'btn btn-success',
                                'id' => 'search_btn'
                            ]) ?>
                        <div class="help-block"></div>
                    <!-- </div> -->
                    </div>
                </div>
        </div>
    </div>

<?php

    echo GridView::widget([
        'id' => 'gridskpd',
        'dataProvider' => $dataSkpd,
        //'filterModel' => $searchSkpd,
        'containerOptions'=>['style'=>'overflow: auto'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => '\yii\grid\SerialColumn'],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'No Kohir',
                'hAlign' => 'center',
                'attribute' => 'no_tetap',
                'width' => '7%',
            ],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'NPWPD',
                'attribute' => 'npwpd',
                'value' => function ($model) {
                    $npwpd = substr($model->npwpd, 0, 1) . '.' . substr($model->npwpd, 1, 6) . '.' . substr($model->npwpd, 7, 2) . '.' . substr($model->npwpd, 9, 2) . '.' . substr($model->npwpd, 11, 3);
                    return $npwpd;
                },
            ],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'Nama WP',
                'attribute' => 'nm_wp',
            ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Tgl Ketetapan',
                'width' => '10%',
                'filterType' => GridView::FILTER_DATE,
                'hAlign' => 'center',
                'attribute' => 'tg_tetap',
                'value' => function ($model) {
                    // $tspt = TSpt::findOne(['npwpd' => $model->npwpd]);
                    // $tdata = TData::findOne(['id_spt' => $tspt->id_spt]);
                    // $ttetap = Ttetap::findOne(['id_tetap' => $tdata->id_tetap]);
                    return date('d-m-Y', strtotime($model->tg_tetap));
                },
            ],
            // [
            //     //'class' => '\kartik\grid\DataColumn',
            //     'header' => 'Tgl Cetak',
            //     'width' => '10%',
            //     'filterType' => GridView::FILTER_DATE,
            //     'hAlign' => 'center',
            //     'attribute' => 'tg_cetak',
            //     'value' => function ($model) {
            //         if ($model->tg_cetak != null){
            //             return date('d-m-Y', strtotime($model->tg_cetak));
            //         } else {
            //             return '-';
            //         }
            //     },
            // ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Jenis Pajak',
                'attribute' => 'kd_ayt',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(TAyat::find()
                                ->where("jn_ayt::int=0 and kl_ayt::int=0 and kd_ayt not in ('1111', '1112', '1113', '1114') and left(kd_ayt, 2) = '11'")
                                //->orderBy('kd_ayt')
                                ->asArray()
                                ->all(), 'kd_ayt', 'nm_ayt'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Kode Rekening'],
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->nm_ayt;
                },
            ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Omset',
                'hAlign' => 'right',
                'width' => '120px',
                //'attribute' => 'masaspt',
                'value' => function ($model) {
                    return number_format($model->jml_pr, 2, ',', '.');
                },
            ],
            [
                'format' => 'raw',
                'header' => 'Cetak',
                //'vAlign' => 'middle',
                'hAlign' => 'center',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'mergeHeader' => true,
                'value' => function ($model) {
                    return Html::a("<span class='glyphicon glyphicon-print'>", 'javascript:void(0)', ["class" => "btn btn-success btn_cetak",
                                    "tahun" => $model->tahun ,
                                    "id_spt" => $model->id_spt ,
                                    "kd_ayt" => $model->kd_ayt ,]);
                                    //"tg_tetap" => $model->tg_tetap ,
                                    //"npwpd" => $model->npwpd ,]);

                    // return Html::a('<i class="glyphicon glyphicon-print"></i>', [
                    //             '/pad/pendataanofficial/cetakskpd', 'id_tetap' => $model->id_tetap
                    //                 ], [
                    //             'class' => 'btn btn-sm btn-success',
                    //             'target' => '_blank'
                    // ]);
                },
            ],
            ],
            'toolbar' => [],
            'export' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false,
                ],
            ],
            'responsive' => true,
            'responsiveWrap' => false,
            'perfectScrollbar' => true,
            'hover' => true,
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'heading' => '<i class="glyphicon glyphicon-book"></i>',
            ],
        ]);
    ?>

    
</div>

<?php
    // ActiveForm::end();
    
    $urlcetak =  Url::to(['/pad/pendataanofficial/cetakskpd']);
$js = <<<JS
    
    $('#tg_tetap_awal').on('change', function(){
        var tg_tetap_awal = $('#tg_tetap_awal').val();
        console.log(tg_tetap_awal);
        if(tg_tetap_awal != ""){
            $("#tg_tetap_akhir-disp").attr("readonly",false);
        }
    });

    $('#no_tetap').on('change', function(){
        var no_tetap = $('#no_tetap').val();
        console.log(no_tetap);
        if(no_tetap != ""){
            $("#no_tetap_akhir").attr("readonly",false);
        }
    });

    $.ajax({
        url:"reset-grid-cetak-skpd",
        data:{
            tg_tetap_awal:$('#tg_tetap_awal').val(),
            tg_tetap_akhir:$('#tg_tetap_akhir').val(),
            kd_ayt:$('#kd_ayt').val(),
            no_tetap:$('#no_tetap').val(),
            no_tetap_akhir:$('#no_tetap_akhir').val()
        },
        type: "POST",
        dataType: "json",
        success: function (data) {
            if(data == 1){
                console.log(data);
                $("#gridskpd").yiiGridView("applyFilter");
                return false;
            }
        }
    });
    
    $('#search_btn').on('click', function(){
        $.ajax({
            url:"reset-grid-cetak-skpd",
            data:{
                tg_tetap_awal:$('#tg_tetap_awal').val(),
                tg_tetap_akhir:$('#tg_tetap_akhir').val(),
                kd_ayt:$('#kd_ayt').val(),
                no_tetap:$('#no_tetap').val(),
                no_tetap_akhir:$('#no_tetap_akhir').val()
            },
            type: "POST",
            dataType: "json",
            success: function (data) {
                if(data == 1){
                    console.log(data);
                    $("#gridskpd").yiiGridView("applyFilter");
                    return false;
                }
            }
        });
    });

    $(document).on('click', '.btn_cetak', function (e) {
        e.preventDefault();
        var tahun = $(this).attr('tahun'),
            id_spt = $(this).attr('id_spt'),
            kd_ayt = $(this).attr('kd_ayt'),
            tg_cetak = $(this).attr('tg_cetak');
        window.location.href = '$urlcetak' + '?tahun=' + tahun +'&id_spt=' + id_spt+'&kd_ayt=' + kd_ayt;
    });



  
JS;

$this->registerJs($js);
?>