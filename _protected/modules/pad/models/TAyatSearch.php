<?php

namespace app\modules\pad\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TAyatSearch represents the model behind the search form about `app\modules\pad\models\TAyat`.
 */
class TAyatSearch extends TAyat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ayt', 'tahun', 'th', 'bl', 'createdby', 'updatedby', 'id_ayt_lama'], 'integer'],
            [['kd_ayt', 'jn_ayt', 'kl_ayt', 'nm_ayt', 'ket_ayt', 'tanggal_ayt', 'createdtime', 'updatedtime'], 'safe'],
            [['id1', 'id2'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $session = Yii::$app->session;

        $query = TAyat::find()
                ->where("pad.t_ayat.kd_ayt='$this->kd_ayt' and pad.t_ayat.tahun='$this->tahun' and pad.t_ayat.jn_ayt::int != 0 and pad.t_ayat.tarifpr != 0")
                ->orderBy('pad.t_ayat.jn_ayt asc, pad.t_ayat.kl_ayt asc');
        if (strlen(Yii::$app->user->identity->username) == '14') {
            $query->join('inner join', 'pad.t_kukuh_detail',
            "pad.t_ayat.kd_ayt=pad.t_kukuh_detail.kd_ayt and 
            pad.t_ayat.jn_ayt=pad.t_kukuh_detail.jn_ayt and 
            pad.t_ayat.kl_ayt=pad.t_kukuh_detail.kl_ayt and
            pad.t_kukuh_detail.npwpd = '".Yii::$app->user->identity->username."'");
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_ayt' => $this->id_ayt,
            'tarifpr' => $this->tarifpr,
            'tarifrp' => $this->tarifrp,
            'tanggal_ayt' => $this->tanggal_ayt,
            //'tahun' => ,
            'th' => $this->th,
            'bl' => $this->bl,
            'id1' => $this->id1,
            'id2' => $this->id2,
            'createdby' => $this->createdby,
            'createdtime' => $this->createdtime,
            'updatedby' => $this->updatedby,
            'updatedtime' => $this->updatedtime,
            'id_ayt_lama' => $this->id_ayt_lama,
        ]);

        $query->andFilterWhere(['like', 'jn_ayt', $this->jn_ayt])
                ->andFilterWhere(['like', 'kl_ayt', $this->kl_ayt])
                ->andFilterWhere(['like', 'upper(nm_ayt)', strtoupper($this->nm_ayt)])
                ->andFilterWhere(['like', 'ket_ayt', $this->ket_ayt]);
        //->andFilterWhere(['like', 'kd_ayt', ]);

        return $dataProvider;
    }
}
