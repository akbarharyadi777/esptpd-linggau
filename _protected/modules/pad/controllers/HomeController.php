<?php

namespace app\modules\pad\controllers;

use app\controllers\AppController;
use app\modules\management\models\User;
use app\modules\pad\models\Pegawai;
use app\modules\pad\models\TDaftar;
use Yii;

/**
 * RekamPendataanController implements the CRUD actions for TDataSelf model.
 */
class HomeController extends AppController
{
    public function actionIndex()
    {
        $user = '';
        $model = '';
        $model2 = '';
        if (!empty(Yii::$app->user->identity->id)) {
            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
            $model = TDaftar::findOne(['npwpd' => $user->username]);
            $model2 = Pegawai::findOne(['nip' => $user->username]);
        }

        return $this->render('index_', [
                    'model' => $model,
                    'user' => $user,
                    'model2' => $model2,
        ]);
    }
}
