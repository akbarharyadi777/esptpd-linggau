<?php

use app\modules\management\components\GhostHtml;
use app\modules\management\UserManagementModule;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use webvimark\extensions\GridBulkActions\GridBulkActions;
use webvimark\extensions\GridPageSize\GridPageSize;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\management\models\rbacDB\search\AuthItemGroupSearch $searchModel
 */
$this->title = UserManagementModule::t('back', 'Permission group');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>


    <?php
    Pjax::begin([
        'id' => 'auth-item-group-grid-pjax',
    ])
    ?>

    <?=
    GridView::widget([
        'id' => 'auth-item-group-grid',
        'dataProvider' => $dataProvider,
        'pager' => [
            'options' => ['class' => 'pagination pagination-sm'],
            'hideOnSinglePage' => true,
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:10px']],
            [
                'attribute' => 'name',
                'value' => function($model) {
                    return Html::a($model->name, ['update', 'id' => $model->code], ['data-pjax' => 0]);
                },
                        'format' => 'raw',
                    ],
                    'code',
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'template' => '{update}{delete}',
                        'contentOptions' => ['style' => 'width:70px; text-align:center;'],
                    ],
                ],
                'containerOptions' => ['style' => 'overflow: auto'],
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'enablePushState' => false,
                    ],
                ],
                'toolbar' => [
                    ['content' =>
                        Html::a('Buat Permission Group', ['create'], ['class' => 'btn btn-success pull-right'])
                    ],
                    '{toggleData}',
                ],
                'export' => [
                    'fontAwesome' => true
                ],
                'striped' => true,
                'bordered' => true,
                'condensed' => true,
                'responsiveWrap' => true,
                'perfectScrollbar' => true,
                'panel' => [
                    'type' => GridView::TYPE_SUCCESS,
                ],
                'persistResize' => false,
            ]);
            ?>

            <?php Pjax::end() ?>
</div>
