<?php

use webvimark\extensions\GridPageSize\GridPageSize;
use app\modules\management\components\GhostHtml;
use app\modules\management\models\rbacDB\AuthItemGroup;
use app\modules\management\models\rbacDB\Role;
use app\modules\management\UserManagementModule;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\management\models\rbacDB\search\RoleSearch $searchModel
 * @var yii\web\View $this
 */
$this->title = UserManagementModule::t('back', 'Tabel Role');
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>

    <?php
    Pjax::begin([
        'id' => 'role-grid-pjax',
    ])
    ?>

    <?=
    GridView::widget([
        'id' => 'role-grid',
        'dataProvider' => $dataProvider,
        'pager' => [
            'options' => ['class' => 'pagination pagination-sm'],
            'hideOnSinglePage' => true,
        ],
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:10px']],
            [
                'attribute' => 'description',
            ],
            'name',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
                'contentOptions' => ['style' => 'width:70px; text-align:center;'],
            ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true,
        'pjaxSettings' => [
            'options' => [
                'enablePushState' => false,
            ],
        ],
        'toolbar' => [
            ['content' =>
                Html::a('Buat Role', ['create'], ['class' => 'btn btn-success pull-right'])
            ],
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'striped' => false,
        'bordered' => true,
        'condensed' => true,
        'responsiveWrap' => true,
        'perfectScrollbar' => true,
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
        ],
        'persistResize' => false,
    ]);
    ?>

    <?php Pjax::end() ?>
</div>