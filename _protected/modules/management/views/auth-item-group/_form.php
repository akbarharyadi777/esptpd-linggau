<?php

use app\modules\management\UserManagementModule;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\modules\management\models\rbacDB\AuthItemGroup $model
 * @var yii\bootstrap\ActiveForm $form
 */
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'auth-item-group-form',
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
    ]);
    ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'autofocus' => $model->isNewRecord ? true : false]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'code')->textInput(['maxlength' => 64]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <?php if ($model->isNewRecord): ?>
                    <?=
                    Html::submitButton(
                            '<span class="glyphicon glyphicon-plus-sign"></span> ' . UserManagementModule::t('back', 'Create'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php else: ?>
                    <?=
                    Html::submitButton(
                            '<span class="glyphicon glyphicon-ok"></span> ' . UserManagementModule::t('back', 'Save'), ['class' => 'btn btn-primary']
                    )
                    ?>
                <?php endif; ?>
                <?=
                Html::a(
                        UserManagementModule::t('back', 'Kembali'), ['index'], ['class' => 'btn btn-warning']
                )
                ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

