<?php

/**
 * @var $this yii\web\View
 * @var ActiveForm $form
 * @var array $routes
 * @var array $childRoutes
 * @var array $permissionsByGroup
 * @var array $childPermissions
 * @var Permission $item
 */

use app\modules\management\components\GhostHtml;
use app\modules\management\UserManagementModule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\rbac\Permission;
use kartik\widgets\ActiveForm;

$this->title = 'Setting untuk : ' . $item->description;
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= GhostHtml::a(UserManagementModule::t('back', 'Kembali'), ['index'], ['class' => 'btn btn-sm btn-warning']) ?></h3>
    </div>
    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success text-center">
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> <?= UserManagementModule::t('back', 'Child permissions') ?>
                    </strong>
                </div>
                <div class="panel-body">

                    <?php // Html::beginForm(['set-child-permissions', 'id' => $item->name]) ?>

                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::to(['set-child-permissions', 'id' => $item->name]),
                                'type' => ActiveForm::TYPE_HORIZONTAL,
                                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                    ]);
                    ?>

                    <div class="row">
                        <?php foreach ($permissionsByGroup as $groupName => $permissions): ?>
                            <div class="col-sm-6">
                                <fieldset>
                                    <legend><?= $groupName ?></legend>

                                    <?php foreach ($permissions as $permission): ?>
                                        <label>
                                            <?php $isChecked = in_array($permission->name, ArrayHelper::map($childPermissions, 'name', 'name')) ? 'checked' : '' ?>
                                            <input type="checkbox" <?= $isChecked ?> name="child_permissions[]" value="<?= $permission->name ?>">
                                            <?= $permission->description ?>
                                        </label>

                                        <?=
                                        GhostHtml::a(
                                                '<span class="glyphicon glyphicon-edit"></span>', ['view', 'id' => $permission->name], ['target' => '_blank']
                                        )
                                        ?>
                                        <br/>
                                    <?php endforeach ?>

                                </fieldset>
                                <br/>
                            </div>


                        <?php endforeach ?>
                    </div>


                    <hr/>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success btn-sm']
                    )
                    ?>
                    <?php ActiveForm::end(); ?>
                    <?//= Html::endForm() ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>
                        <span class="glyphicon glyphicon-th"></span> Routes

                        <?=
                        Html::a(
                                'Refresh routes (dan hapus yang tak terpakai)', ['refresh-routes', 'id' => $item->name, 'deleteUnused' => 1], [
                            'class' => 'btn btn-default btn-sm pull-right',
                            'style' => 'margin-top:-5px; text-transform:none;',
                            'data-confirm' => UserManagementModule::t('back', 'Apa Anda yakin ingin merefresh route dan menghapus yang tak terpakai?.'),
                                ]
                        )
                        ?>

                        <?=
                        Html::a(
                                'Refresh routes', ['refresh-routes', 'id' => $item->name], [
                            'class' => 'btn btn-default btn-sm pull-right',
                            'style' => 'margin-top:-5px; text-transform:none;',
                                ]
                        )
                        ?>


                    </strong>
                </div>

                <div class="panel-body">

                    <?php
                    $form = ActiveForm::begin([
                                'action' => Url::to(['set-child-routes', 'id' => $item->name]),
                                'type' => ActiveForm::TYPE_HORIZONTAL,
                                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
                    ]);
                    ?>
                    
                    <?php //= Html::beginForm(['set-child-routes', 'id' => $item->name]) ?>

                    <div class="row">
                        <div class="col-sm-3">
                            <?=
                            Html::submitButton(
                                    UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success btn-sm']
                            )
                            ?>
                        </div>

                        <div class="col-sm-6">
                            <input id="search-in-routes" autofocus="on" type="text" class="form-control input-sm" placeholder="Search route">
                        </div>

                        <div class="col-sm-3 text-right">
                            <span id="show-only-selected-routes" class="btn btn-default btn-sm">
                                <i class="fa fa-minus"></i> Lihat yang terpilih
                            </span>
                            <span id="show-all-routes" class="btn btn-default btn-sm hide">
                                <i class="fa fa-plus"></i> Lihat Semua
                            </span>

                        </div>
                    </div>

                    <hr/>

                    <?=
                    Html::checkboxList(
                            'child_routes', ArrayHelper::map($childRoutes, 'name', 'name'), ArrayHelper::map($routes, 'name', 'name'), [
                        'id' => 'routes-list',
                        'separator' => '<div class="separator"></div>',
                        'item' => function($index, $label, $name, $checked, $value) {
                            return Html::checkbox($name, $checked, [
                                        'value' => $value,
                                        'label' => '<span class="route-text">' . $label . '</span>',
                                        'labelOptions' => ['class' => 'route-label'],
                                        'class' => 'route-checkbox',
                            ]);
                        },
                                    ]
                            )
                            ?>

                            <hr/>
                            <?=
                            Html::submitButton(
                                    UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success btn-sm']
                            )
                            ?>

                            <?//= Html::endForm() ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $js = <<<JS

var routeCheckboxes = $('.route-checkbox');
var routeText = $('.route-text');

// For checked routes
var backgroundColor = '#D6FFDE';

function showAllRoutesBack() {
	$('#routes-list').find('.hide').each(function(){
		$(this).removeClass('hide');
	});
}

//Make tree-like structure by padding controllers and actions
routeText.each(function(){
	var _t = $(this);

	var chunks = _t.html().split('/').reverse();
	var margin = chunks.length * 40 - 40;

	if ( chunks[0] == '*' )
	{
		margin -= 40;
	}

	_t.closest('label').css('margin-left', margin);

});

// Highlight selected checkboxes
routeCheckboxes.each(function(){
	var _t = $(this);

	if ( _t.is(':checked') )
	{
		_t.closest('label').css('background', backgroundColor);
	}
});

// Change background on check/uncheck
routeCheckboxes.on('change', function(){
	var _t = $(this);

	if ( _t.is(':checked') )
	{
		_t.closest('label').css('background', backgroundColor);
	}
	else
	{
		_t.closest('label').css('background', 'none');
	}
});


// Hide on not selected routes
$('#show-only-selected-routes').on('click', function(){
	$(this).addClass('hide');
	$('#show-all-routes').removeClass('hide');

	routeCheckboxes.each(function(){
		var _t = $(this);

		if ( ! _t.is(':checked') )
		{
			_t.closest('label').addClass('hide');
			_t.closest('div.separator').addClass('hide');
		}
	});
});

// Show all routes back
$('#show-all-routes').on('click', function(){
	$(this).addClass('hide');
	$('#show-only-selected-routes').removeClass('hide');

	showAllRoutesBack();
});

// Search in routes and hide not matched
$('#search-in-routes').on('change keyup', function(){
	var input = $(this);

	if ( input.val() == '' )
	{
		showAllRoutesBack();
		return;
	}

	routeText.each(function(){
		var _t = $(this);

		if ( _t.html().indexOf(input.val()) > -1 )
		{
			_t.closest('label').removeClass('hide');
			_t.closest('div.separator').removeClass('hide');
		}
		else
		{
			_t.closest('label').addClass('hide');
			_t.closest('div.separator').addClass('hide');
		}
	});
});

JS;

        $this->registerJs($js);
        ?>