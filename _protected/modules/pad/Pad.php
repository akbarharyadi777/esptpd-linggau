<?php

namespace app\modules\pad;

class Pad extends \yii\base\Module {

    public $controllerNamespace = 'app\modules\pad\controllers';

    public function init() {
        parent::init();

        // custom initialization code goes here
    }

}
