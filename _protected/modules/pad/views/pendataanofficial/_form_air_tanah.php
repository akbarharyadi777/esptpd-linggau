<?php

use kartik\datecontrol\DateControl;
use kartik\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\MaskedInput;
use yii\helpers\ArrayHelper;
use app\modules\pad\models\TKet;
use app\modules\pad\models\Pegawai;
use app\modules\pad\models\TUnitusaha;
use kartik\datetime\DateTimePicker;
use app\modules\management\models\User;
use kartik\widgets\Select2;

/* @var $this View */
$this->title = $judul;
?>
<div class="box box-widget" id="from_input">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'form-pendataan-hotel',
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => [
                    'enctype' => 'multipart/form-data',
                ]
    ]);
    ?>
    <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div id="alert-no_pokok" class="alert alert-warning" style="display:none;">
                        <strong>Peringatan!</strong> No Pokok yang Anda masukan salah.
                    </div>
                </div>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2 col-md-pull-0">
                        <?php
                        if (!$modelSpt->isNewRecord) {
                            $modelSpt->tg_data = date('d-m-Y', strtotime($modelSpt->tg_data));
                        }
                        echo $form->field($modelSpt, 'tg_data')->widget(datecontrol::classname(), [
                            'type' => DateControl::FORMAT_DATE,
                            'ajaxConversion' => false,
                            'widgetOptions' => [
                                'layout' => '{picker}{input}',
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'startDate' => ''
                                ]
                            ]
                        ]);
                        ?>
                    </div>

                    <div class="col-md-1 col-md-offset-2">
                        <?=
                        $form->field($modelData, 'th_spt')->widget(MaskedInput::ClassName(), [
                            //'name' => 'input-3',
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 4, 'greedy' => false]
                        ]);
                        ?>
                    </div>

                    <div class="col-md-2 col-md-offset-3">
                        <?php
                        echo $form->field($modelDaftar, 'no_pokok')->widget(MaskedInput::ClassName(), [
                            'name' => 'input-3',
                            'mask' => '9',
                            'clientOptions' => ['repeat' => 6, 'greedy' => false]
                        ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                    <?= 
                        $form->field($modelData, 'st_spt')
                            ->dropDownList(
                                    ArrayHelper::map(TKet::find()->where(['kd_spt' => $modelData->st_spt])->asArray()->all(), 'kd_spt', 'singk')
                                    )
                        ?>          
                    </div>
                    <div class="col-md-4 col-md-offset-2">
                        <label for="exampleInputEmail1">Periode</label>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo DatePicker::widget([
                                        'model' => $modelSpt,
                                        'attribute' => 'tgl_awal',
                                        'attribute2' => 'tgl_akhir',
                                        'options' => ['placeholder' => 'tanggal awal'],
                                        'options2' => ['placeholder' => 'tanggal akhir'],
                                        'type' => DatePicker::TYPE_RANGE,
                                        'form' => $form,
                                        'layout' => '{input1}<span class="input-group-addon kv-field-separator">s/d</span>{input2}',
                                        'pluginOptions' => [
                                            'format' => 'dd-mm-yyyy',
                                            'autoclose' => true,
                                        ]
                                    ]);
                                    ?>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-3">
                        <?= $form->field($modelData, 'nmr_bayar')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
                    </div>
                </div>
            </div><!--end row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <?php echo $form->field($modelData, 'judul')->textArea(['readOnly' => false])->label('Keterangan') ?>
                        </div>

                        <div class="col-md-1 hidden">
                            <?= $form->field($modelData, 'no_data')->textInput(['readOnly' => true]) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                <hr style="width: 97%; color: black; height: 1px; background-color:black;">
                    </div>
                </div>
             <div class="row">
                <div class="col-md-12">
                     <div class="col-md-3">
                        <?php
                        echo $form->field($modelData, 'kd_rek', [
                            'addon' => [
                                'prepend' => [
                                    'content' => $kode_rekening,
                                ],
                                'append' => [
                                    'content' => Html::a('<i class="fa fa-search"></i>', '', ['class' => 'btn btn-success btn-flat', 'data-toggle' => 'modal', 'data-target' => '#m_ayat']),
                                    'asButton' => true
                                ]
                            ]
                        ])->label('Kode Rekening')->textInput(['readOnly' => true]);
                        $modelData->kd_ayt = $kode_rekening;
                        echo $form->field($modelData, 'id_ayt')->textInput(['class'=>'hidden'])->label(false);
                        echo $form->field($modelData, 'kd_ayt')->textInput(['class'=>'hidden'])->label(false);
                        ?>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <?php
                         echo $form->field($searchAyat, 'nm_ayt')->label('Nama Rekening')->textInput(['readOnly' => true]);
                        ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                         echo $form->field($modelData, 'jumlah', [
                            'addon' => [
                                'append' => [
                                    'content' => 'M<sup>3</sup>'
                                ]
                            ]
                        ])->label('Volume')->textInput(['style' => 'text-align:right;']);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="col-md-3">
                        <?php
                         echo $form->field($searchAyat, 'tarifrp', [
                            'addon' => [
                                'prepend' => [
                                    'content' => 'Rp. '
                                ]
                            ]
                        ])->label('Nilai Pasar')->textInput(['readOnly' => true, 'style' => 'text-align:right;']);
                        ?>
                    </div>
                    <div class="col-md-2 col-md-offset-1">
                        <?php
                         echo $form->field($searchAyat, 'tarifpr', [
                            'addon' => [
                                'append' => [
                                    'content' => '%'
                                ]
                            ]
                        ])->label('Tarif Persen')->textInput(['readOnly' => true, 'onkeyup' => "this.value=this.value.replace(/[^0-9.,]/g,'')"]);
                        ?>
                    </div>
                    <div class="col-md-3 col-md-offset-2">
                        <?php
                         echo $form->field($modelData, 'nilai', [
                            'addon' => [
                                'prepend' => [
                                    'content' => 'Rp. '
                                ]
                            ]
                        ])->label('NPA')->textInput(['readOnly' => true, 'style' => 'text-align:right;', "onkeyup" => "this.value=this.value.replace(/[^0-9.,]/g,'')"]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <?= Html::button('<i class="fa fa-plus"></i> Tambah', ['id' => 'btn_add', 'class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 10px;">
                <div id="div-error-pajak" class="alert alert-warning" style="display: none;">
                    <strong>Peringatan!</strong> Harap lengkapi data pajak terlebih dahulu.
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table-with-style table table-bordered table-condensed table-striped" id="gridPajak" style="margin: 0px auto;width: 97%; color: black; height: 1px; background-color:black;">
                        <thead>
                            <tr>
                                <th style="text-align: center;" rowspan="2">No Rek.</th>
                                <th style="text-align: center;" rowspan="2">JENIS AIR TANAH</th>
                                <th style="text-align: center;" colspan="3">Penetapan Pajak Terutang</th>
                                <th style="text-align: center;" rowspan="2">POKOK PAJAK (Rp.)</th>
                                <th rowspan="2"></th>
                            </tr>
                            <tr>
                                <th style="text-align: center;">Volume (M<sup>3</sup>)</th>
                                <th style="text-align: center;">Nilai Pasar (Rp)</th>
                                <th style="text-align: center;">TARIF PAJAK (%)</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                    <div class="col-md-12">
                        <hr style="width: 97%; color: black; height: 1px; background-color:black;">
                    </div>
                </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?= $form->field($modelDaftar, 'npwpd')->textInput(['readOnly' => true]) ?>
                    </div>
                    <div class="col-md-4 col-md-offset-1">
                        <?php echo $form->field($modelDaftar, 'nm_wp')->textInput(['readOnly' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                            if ($kode_rekening == '1102'){
                                echo $form->field($modelData, 'kode_unit')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(TUnitusaha::find()->asArray()->all(),'kode_unit', 'nama_unit'),
                                        'options' => ['placeholder' => 'Pilih Dinas ...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label('Nama Dinas');
                            } ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <?php echo $form->field($modelDaftar, 'alm_wp')->textArea(['readOnly' => true, 'row' => 3]) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kel_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-4">
                        <?php echo $form->field($modelDaftar, 'kec_wp')->textInput(['readOnly' => true]) ?>
                    </div>

                    <div class="col-md-3">
                        <?php
                        echo $form->field($modelData, 'jml_pr', ['addon' => [
                                'prepend' => [
                                    'content' => 'Rp.',
                                ],
                                'append' => [
                                    'content' => ',00',
                                ]
                            ]
                        ])->textInput([
                            'style' => 'text-align:right;',
                            'readOnly' => true
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="row hidden">
                <div class="col-md-12">

                    <div class="col-md-4">
                        
                        <?php 
                        echo $form->field($modelData, 'nip')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(Pegawai::find()->asArray()->all(),'nip', 'nm_pegawai'),
                                'options' => ['placeholder' => 'Pilih Petugas ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label('Petugas Pemungut'); ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                            if ($modelData->isNewRecord) {
                                echo $form->field($modelData, 'penerima')->textInput(['readOnly' => true, 'value' => \Yii::$app->user->identity->username])->label('Petugas Penerima');
                            } else {
                                echo $form->field($modelData, 'penerima')->textInput(['readOnly' => true, 'value' => User::findOne(['id' => $modelData->createdby])->username])->label('Petugas Penerima');
                            }
                        ?>
                    </div>
                    <div class="col-md-2">
                        <?php
                            if ($modelData->isNewRecord) {
                                echo $form->field($modelData, 'tgl_penerima')->textInput(['readOnly' => true, 'value' => date('d-m-Y')])->label('Tanggal Terima');
                            } else {
                                echo $form->field($modelData, 'tgl_penerima')->textInput(['readOnly' => true, 'value' => date('d-m-Y', strtotime($modelData->createdtime))])->label('Tanggal Terima');
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div style="box-shadow: 0 0 31px 0 rgba(0, 0, 0, 0.3) inset; overflow:hidden;" class="alert">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?=
                            $form->field($modelData, 'upload_file')->label(false)->widget(FileInput::classname(), [
                                'name' => 'file',
                                'options' => ['accept' => '.xls, .xlsx, .pdf, .zip, .rar'],
                                'pluginOptions' => [
                                    'browseClass' => 'btn btn-success',
                                    'showPreview' => false,
                                    'showCaption' => true,
                                    'showRemove' => true,
                                    'showUpload' => false,
                                    'removeClass' => 'btn btn-danger',
                                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> '
                                ]
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?= Html::button(' Simpan', ['id' => 'btn_simpan', 'class' => 'btn btn-success']) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>

    <?php
$js = <<<JS

jQuery(document).ready(function(){

    $('#tdaftar-no_pokok').change(function(){
        $(this).val(padLeft($(this).val(), 6));
        $('#alert-no_pokok').hide();
        $.ajax({
            url: "getwp",
            data: 'no_daftar=' + $(this).val(),
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                if (parseInt(data) === 1) {
                    $('#tdaftar-npwpd').val('');
                    $('#tdaftar-kel_wp').val('');
                    $('#tdaftar-kec_wp').val('');
                    $('#tdaftar-nm_wp').val('');
                    $('#tdaftar-alm_wp').val('');
                    if($('#tdaftar-no_pokok').val() != '000000'){
                        $('#alert-no_pokok').show();
                    }  
                } else {
                    $('#tdaftar-npwpd').val(data.npwpd);
                    $('#tdaftar-kel_wp').val(data.kelurahan);
                    $('#tdaftar-kec_wp').val(data.kecamatan);
                    $('#tdaftar-nm_wp').val(data.nm_wp);
                    $('#tdaftar-alm_wp').val(data.alm_wp);
                }
            }
        });
        dapatkanNoRegister();
    }).trigger('change');

    $('#m_ayat').on('show.bs.modal', function () {
        var modal = $(this);
        modal.find('.modal-body').html('');
        modal.find('.modal-body').html('<i class=\"fa fa-spinner fa-spin\"></i>');
        $.ajax({
            url: "listrekening",
            data: {"tahun": $('#tdata-th_spt').val(), "kd_ayt": '$kode_rekening'},
            type: 'POST',
            //dataType: 'json',
            success: function (data) {
                modal.find('.modal-body').html(data);
            }
        });
        dapatkanNoRegister();
    });

    $("#tspt-tgl_awal").change(function () {
        var parts = $(this).val().split('-');
        var now = new Date(parts[2], parts[1] - 1, parts[0]);
        var lastDayOfTheMonth = new Date(1900 + now.getYear(), now.getMonth() + 1, 0);
        if ($(this).val() != '') {
            $("#tspt-tgl_akhir").val($.datepicker.formatDate('dd-mm-yy', lastDayOfTheMonth));
        }
        dapatkanNoRegister();
    });

    $('#tdata-th_spt').change(function(){
        dapatkanNoRegister();
        $.ajax({
            url: "get-no-data",
            data: {
                tahun:$(this).val()
            },
            type: 'POST',
            dataType: 'json',
            success: function(data){
                $("#tdata-no_data").val(data);
            }
        });
    });

    $('#btn_add').click(function (e) {
            e.preventDefault();
            if ($('#tdata-id_ayt').val() === '') {
                $('#div-error-pajak').show();
            } else if ($('#tdata-jumlah').val() === '') {
                $('#div-error-pajak').show();
            } else {
                var id_ayt = $('#tdata-id_ayt').val();
                var kode_rek = '$kode_rekening' + $('#tdata-kd_rek').val().toString();
                var nm_ayt = $('#tayatsearch-nm_ayt').val();
                var volume = $('#tdata-jumlah').val().replace(',', '.');
                var nilai_pasar = $('#tayatsearch-tarifrp').val().replace(/\./g, "");
                var nilai_pasar2 = $('#tayatsearch-tarifrp').val().replace(/\./g, "");
                var tarif = $('#tayatsearch-tarifpr').val();
                var npa = $('#tdata-nilai').val();
                var jml_pajak = volume * nilai_pasar.replace(/\./g,'') * tarif / 100;
                var jml_pajak2 = numerik(jml_pajak);
                var jml_pajak3 = jml_pajak;
                var volume2 = volume.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                var data_yg_disimpan = '0_' + id_ayt + '_' + volume + '_' + npa + '_' + jml_pajak;
                var data = "<tr><td style='text-align:center;'><input type='hidden' name='row[]' value='" + data_yg_disimpan + "'><span>" + kode_rek + "</span></td><td><span>" + nm_ayt + "</span></td><td style='text-align:right;'><span>" + volume2 + "</span></td><td style='text-align:right;'><span>" + numerik(nilai_pasar) + "</span></td><td style='text-align:right;'><span>" + tarif + "</span></td><td style='text-align:right;'><span class='jml_pajak'>" + numerik(jml_pajak3) + "</span><span class='jml_pajak2 hidden'>" + jml_pajak + "</span></td><td style='text-align:center;'><a class='hapus' href='javascript:void(0)'><i class='fa fa-trash'></i></a></td></tr>";
                $("table#gridPajak").append(data);
                $('#tdata-id_ayt').val('');
                $('#tdata-kd_rek').val('');
                $('#tayatsearch-nm_ayt').val('');
                $('#tdata-jumlah').val('');
                $('#tayatsearch-tarifrp').val('');
                $('#tayatsearch-tarifpr').val('');
                $('#tdata-nilai').val('');
                var jml_pajak = 0;
                $('.jml_pajak2').each(function () {
                    jml_pajak += parseFloat($(this).text());
                });
                $('#tdata-jml_pr').val(numerik(jml_pajak));
                $('#div-error-pajak').hide();
            }

    });


    $(document).on('change', "#tdata-jumlah", function (e){
        var volume = ($('#tdata-jumlah').val() === '' || $('#tdata-jumlah').val() === null ? 0 : parseFloat($('#tdata-jumlah').val().replace(',', '.')));
        var tarifrp = ($('#tayatsearch-tarifrp').val() === '' || $('#tayatsearch-tarifrp').val() === null ? 0 : parseFloat($('#tayatsearch-tarifrp').val().replace(/\./g, "")));
        var npa = volume * tarifrp;
        $('#tdata-nilai').val(numerik(npa));
    });
   
    $(document).on('click', "a.hapus", function (e) {
        e.preventDefault();
        var selector = $(this);
        bootbox.dialog({
            message: "Apa Anda yakin ingin menghapus data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Ya",
                    class: "btn-success",
                    callback: function () {
                        selector.parent().parent().remove();
                        var jml_pajak2 = 0;
                        $('.jml_pajak2').each(function () {
                            jml_pajak += parseFloat($(this).text());
                        });
                        $('#tdata-jml_pr').val(numerik(jml_pajak2));
                    }
                },
                danger: {
                    "label": "Tidak",
                    "class": "btn"
                }
            }
        });
    });

    $('#btn_simpan').click(function () {
        bootbox.dialog({
            message: "Apa Anda yakin ingin memproses data ini?",
            title: "Peringatan!",
            buttons: {
                success: {
                    label: "Ya",
                    className: "btn-success",
                    callback: function () {
                        $("#spinner").fadeIn("slow");
                        var dataform = new FormData($("#form-pendataan-hotel")[0]);
                        $.ajax({
                            type: "POST",
                            url: "save",
                            data: dataform,
                            processData: false,
                            contentType: false,
                            success: function (data) {
                                $("#spinner").fadeOut("slow");
                                if (parseInt(data) === 1) {
                                    bootbox.dialog({
                                        message: "Proses data berhasil.",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                                callback: function () {
                                                    $("#btn_simpan").attr('style', 'display:none');
                                                    $("#form-pendataan :input").prop("disabled", true);
                                                    //$(#form-pendataan :input").attr("disabled", true);
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    bootbox.dialog({
                                        message: "Proses data gagal. Silahkan cek data yang Anda masukan kembali",
                                        title: "Notifikasi!",
                                        buttons: {
                                            success: {
                                                label: "Tutup",
                                                className: "btn-success",
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },
                danger: {
                    label: "Tidak",
                    className: "btn-warning",
                    callback: function () {

                    }
                }
            }
        });
    });

    function dapatkanNoRegister() {
        var th_spt = $('#tdata-th_spt').val();
        var kd_ayt = '$kode_rekening';
        var status = 1;
        var tgl_awal = $("#tspt-tgl_awal").val();
        var no_pokok = $("#tdaftar-no_pokok").val();
        var npwpd = $('#tdaftar-npwpd').val();
        $.ajax({
            url: "getregister",
            data: {
                "tahun": th_spt, 
                "kd_ayt": kd_ayt, 
                "status": status, 
                "tgl_awal": tgl_awal, 
                "no_pokok": no_pokok, 
                "npwpd": npwpd
                },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                $("#tdata-nmr_bayar").val(data);
            }
        });
    };


    

});

JS;

$this->registerJs(
    $js,
    View::POS_END,
    'js-page'
);

?>

    <?php
    Modal::begin([
        'id' => 'm_ayat',
        'header' => 'Data Rekening',
    ]);
//    $searchAyat = new TAyatSearch();
//    $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
//    $dataAyat->pagination->pageSize = 5;
    echo $this->render('_m_ayat', [
        'searchAyat' => $searchAyat,
        'dataAyat' => $dataAyat,
    ]);
    ?> 

    <?php
    Modal::end();
    ?>

</div>
