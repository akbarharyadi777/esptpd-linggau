<?php

use app\modules\pad\models\TAyat;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TData;
use app\modules\pad\models\TSpt;
use app\modules\pad\models\TTetap;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Menu Cetak SPTPD';
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title; ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
<div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
<?php

    echo GridView::widget([
        'id' => 'gridsptpd',
        'dataProvider' => $data,
        //'filterModel' => $searchSkpd,
        'containerOptions' => ['style' => 'overflow: auto'],
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => '\yii\grid\SerialColumn'],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'Tahun SPT',
                'hAlign' => 'center',
                'attribute' => 'th_spt',
                'width' => '10%',
            ],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'No SPT',
                'hAlign' => 'center',
                'attribute' => 'no_spt',
                'width' => '5%',
            ],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'NPWPD',
                'attribute' => 'npwpd',
                'value' => function ($model) {
                    return $model->npwpd;
                },
            ],
            [
                //'class' => '\yii\grid\DataColumn',
                'header' => 'Nama WP',
                'value' => function ($model) {
                    return TDaftar::findOne($model->npwpd)->nm_wp;
                },
            ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Periode',
                'filterType' => GridView::FILTER_DATE,
                'value' => function ($model) {
                    // $tspt = TSpt::findOne(['npwpd' => $model->npwpd]);
                    // $tdata = TData::findOne(['id_spt' => $tspt->id_spt]);
                    // $ttetap = Ttetap::findOne(['id_tetap' => $tdata->id_tetap]);
                    return date('d-m-Y', strtotime($model->tgl_awal)).' s/d '.date('d-m-Y', strtotime($model->tgl_akhir));
                },
            ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Jenis Pajak',
                'attribute' => 'kd_ayt',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(TAyat::find()
                                ->where("jn_ayt::int=0 and kl_ayt::int=0 and kd_ayt not in ('1111', '1112', '1113', '1114') and left(kd_ayt, 2) = '11'")
                                //->orderBy('kd_ayt')
                                ->asArray()
                                ->all(), 'kd_ayt', 'nm_ayt'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Kode Rekening'],
                'format' => 'raw',
                'value' => function ($model) {
                    return TAyat::findOne($model->id_ayt)->nm_ayt;
                },
            ],
            [
                //'class' => '\kartik\grid\DataColumn',
                'header' => 'Jumal Pajak',
                'hAlign' => 'right',
                //'attribute' => 'masaspt',
                'value' => function ($model) {
                    return number_format($model->jml_pjk, 2, ',', '.');
                },
            ],
            [
                'format' => 'raw',
                'header' => 'Cetak',
                //'vAlign' => 'middle',
                'hAlign' => 'center',
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'mergeHeader' => true,
                'value' => function ($model) {
                    return Html::a("<span class='glyphicon glyphicon-print'>", 'javascript:void(0)', ['class' => 'btn btn-success btn_cetak',
                                    'tahun' => $model->th_spt,
                                    'no_data_self' => $model->no_data_self, ]);
                },
            ],
            ],
            'toolbar' => [],
            'export' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'options' => [
                    'enablePushState' => false,
                ],
            ],
            'responsive' => true,
            'responsiveWrap' => false,
            'perfectScrollbar' => true,
            'hover' => true,
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'heading' => '<i class="glyphicon glyphicon-book"></i>',
            ],
        ]);
    ?>

    
</div>
</div>
</div>
</div>
<?php
$url_cetak = Url::to(['pendataan/cetak']);
$js = <<<JS
$(document).on('click', '.btn_cetak', function(){
    window.open('$url_cetak' + '?no_data_self=' + $(this).attr('no_data_self') + '&tahun=' + $(this).attr('tahun'), '_blank');
}); 
JS;

$this->registerJs($js);
?>