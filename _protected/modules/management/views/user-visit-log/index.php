<?php

use webvimark\extensions\DateRangePicker\DateRangePicker;
use app\modules\management\UserManagementModule;
use yii\helpers\Html;
use yii\widgets\Pjax;
use webvimark\extensions\GridPageSize\GridPageSize;
use kartik\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\modules\management\models\search\UserVisitLogSearch $searchModel
 */
$this->title = UserManagementModule::t('back', 'Visit log');
?>
<div class="user-visit-log-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
    <div class="box box-widget">
        <div class="box-header with-border">
            <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
            <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
        </div>

        <div class="row">
            <div class="col-sm-12 text-right">
                <?= GridPageSize::widget(['pjaxId' => 'user-visit-log-grid-pjax']) ?>
            </div>
        </div>

        <?php
        Pjax::begin([
            'id' => 'user-visit-log-grid-pjax',
        ])
        ?>

        <?=
        GridView::widget([
            'id' => 'user-visit-log-grid',
            'dataProvider' => $dataProvider,
            'pager' => [
                'options' => ['class' => 'pagination pagination-sm'],
                'hideOnSinglePage' => true,
                'lastPageLabel' => '>>',
                'firstPageLabel' => '<<',
            ],
            'layout' => '{items}<div class="row"><div class="col-sm-8">{pager}</div><div class="col-sm-4 text-right">{summary}</div></div>',
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn', 'options' => ['style' => 'width:10px']],
                [
                    'attribute' => 'user_id',
                    'value' => function($model) {
                        return @$model->user->username;
                    },
                            'format' => 'raw',
                        ],
                        'language',
                        'os',
                        'browser',
                        array(
                            'attribute' => 'ip',
                            'value' => function($model) {
                                return Html::a($model->ip, "http://ipinfo.io/" . $model->ip, ["target" => "_blank"]);
                            },
                                    'format' => 'raw',
                                ),
                                'visit_time:datetime',
//                                [
//                                    'class' => 'kartik\grid\ActionColumn',
//                                    'template' => '{view}',
//                                    'contentOptions' => ['style' => 'width:70px; text-align:center;'],
//                                ],
                            ],
                        ]);
                        ?>

                        <?php Pjax::end() ?>
                    </div>
                </div>

                <?php
                DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'visit_time',
                ])
                ?>