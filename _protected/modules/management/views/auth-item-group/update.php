<?php

use app\modules\management\UserManagementModule;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\modules\management\models\rbacDB\AuthItemGroup $model
 */
$this->title = UserManagementModule::t('back', 'Editing permission group') . ': ' . $model->name;
?>
<div class="auth-item-group-update">

    <?= $this->render('_form', compact('model')) ?>


</div>
