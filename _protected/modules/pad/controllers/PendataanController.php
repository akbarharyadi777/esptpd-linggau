<?php

namespace app\modules\pad\controllers;

use Yii;
use yii\web\Response;
use yii\base\Exception;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use app\modules\pad\models\TAyat;
use app\controllers\AppController;
use app\modules\pad\models\TCamat;
use app\modules\pad\models\TLurah;
use app\modules\pad\models\TDaftar;
use app\modules\pad\models\TUsahaWp;
use app\modules\pad\models\TDataSelf;
use app\modules\pad\models\TNomorMax;
use app\modules\management\models\User;
use app\modules\pad\models\TAyatSearch;
use app\modules\pad\models\PosisiPegawai;
use app\modules\pad\models\TDataSelfMblb;
use app\modules\pad\models\TDataSelfSearch;

/**
 * RekamPendataanController implements the CRUD actions for TDataSelf model.
 */
class PendataanController extends AppController
{
    /**
     * Lists all TDataSelf models.
     *
     * @return mixed
     */
//    public function actionIndex($id) {
//        if ($id == '1101') {
//            $searchModel = new TDataSelfSearch();
//            $dataProvider = $searchModel->searchHotels(Yii::$app->request->queryParams);
//            $dataProvider->pagination->pageSize = 5;
//        }
//        return $this->render('index', [
//                    'searchModel' => $searchModel,
//                    'dataProvider' => $dataProvider,
//        ]);
//    }

    /**
     * Displays a single TDataSelf model.
     *
     * @param string $th_spt
     * @param int    $no_data_self
     *
     * @return mixed
     */
//    public function actionView($th_spt, $no_data_self) {
//        return $this->render('view', [
//                    'model' => $this->findModel($th_spt, $no_data_self),
//        ]);
//    }

    /**
     * Creates a new TDataSelf model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function renderLayoutForm($kode_rekening, $judul, $form, $code_form)
    {
        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        $modelData = new TDataSelf();
        $modelDaftar = new TDaftar();
        $modelUsaha = new TUsahaWp();
        $modelData->th_spt = date('Y');

        $modelAyat = new TAyat();

        return $this->render($form, [
            'modelData' => $modelData,
            'user' => $user,
            'modelDaftar' => $modelDaftar,
            'kode_rekening' => $kode_rekening,
            'judul' => $judul,
            'modelAyat' => $modelAyat,
            'modelUsaha' => $modelUsaha,
        ]);
    }

    public function actionNoRes()
    {
        $tahun = $_POST['tahun'];
        $kode_ayat = $_POST['kd_ayt'];
        echo str_pad($this->no_form($tahun), 4, '0', STR_PAD_LEFT);
    }

    public function no_form($tahun, $kode_ayat)
    {
        $nomor = 0;
        $tnm_max = TNomorMax::findOne(['tahun' => $tahun]);
        if ($kode_ayat == '1101') {
            $nomor = $tnm_max->id_hot;
        } elseif ($kode_ayat == '1102') {
            $nomor = $tnm_max->id_res;
        } elseif ($kode_ayat == '1103') {
            $nomor = $tnm_max->id_hib;
        } elseif ($kode_ayat == '1107') {
            $nomor = $tnm_max->id_park;
        } elseif ($kode_ayat == '1111') {
            // $nomor = $tnm_max->id_mblb;
        }

        return $nomor + 1;
    }

    public function actionCreateHotel()
    {
        $kode_rekening = '1101';
        $judul = 'Pendataan Pajak Hotel';
        $form = '_form_omzet';
        $code_form = 'HTL';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreateRestoran()
    {
        $kode_rekening = '1102';
        $judul = 'Pendataan Pajak Restoran';
        $form = '_form_omzet';
        $code_form = 'RES';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreateHiburan()
    {
        $kode_rekening = '1103';
        $judul = 'Pendataan Pajak Hiburan';
        $form = '_form_omzet';
        $code_form = 'HIB';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreatePpj()
    {
        $kode_rekening = '1105';
        $judul = 'Pendataan Pajak Penerangan Jalan';
        $form = '_form_ppj';
        $code_form = 'PPJ';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreateParkir()
    {
        $kode_rekening = '1107';
        $judul = 'Pendataan Pajak Parkir';
        $form = '_form_omzet';
        $code_form = 'PRK';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreateWalet()
    {
        $kode_rekening = '1109';
        $judul = 'Pendataan Pajak Burung Walet';
        $form = '_form_walet';
        $code_form = 'SBW';

        return $this->renderLayoutForm($kode_rekening, $judul, $form, $code_form);
    }

    public function actionCreateMblb()
    {
        $kode_rekening = '1111';
        $judul = 'Pendataan Pajak Mineral Bukan Logam dan Batuan';
        $form = '_form_mblb';
        $code_form = 'GAL';
        $user = User::findOne(['id' => Yii::$app->user->identity->id]);
        $modelData = new TDataSelf();
        $modelDaftar = new TDaftar();
        $modelData->th_spt = date('Y');
        $no_form = str_pad($this->no_form($modelData->th_spt, $kode_rekening), 4, '0', STR_PAD_LEFT);

        $modelData->no_spt = $no_form.'/'.$code_form.'/'.substr($modelData->th_spt, 2, 2);
        $modelData->no_form = $no_form.'.'.date('m').'.'.substr(date('Y'), 2, 2);

        $modelDataMblb = new TDataSelfMblb();
        $modelAyat = new TAyat();

        return $this->render($form, [
            'modelData' => $modelData,
            'user' => $user,
            'modelDaftar' => $modelDaftar,
            'kode_rekening' => $kode_rekening,
            'judul' => $judul,
            'modelAyat' => $modelAyat,
            'modelDataMblb' => $modelDataMblb,
        ]);
    }

    public function actionGetwp()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $no_daftar = filter_input(INPUT_POST, 'no_daftar');
        $kd_ayt = filter_input(INPUT_POST, 'kd_ayt');
        $jn_ayt = filter_input(INPUT_POST, 'jn_ayt');

        try {
            $dataWP = TDaftar::find()->where("substring(no_pokok, 2)='".$no_daftar."'")->one();
            if ($dataWP != null) {
                $lurah = TLurah::findOne(['kd_camat' => $dataWP->kec_wp, 'kd_lurah' => $dataWP->kel_wp]);
                $camat = TCamat::findOne(['kd_camat' => $dataWP->kec_wp]);
                $usaha = TUsahaWp::find()->where("npwpd='".$dataWP->npwpd."' and kd_ayt='".$kd_ayt."' and jn_ayt='".$jn_ayt."'")->one();

                $json = array(
                    'npwpd' => $dataWP->npwpd,
                    'nm_wp' => $dataWP->nm_wp,
                    'alm_wp' => $dataWP->alm_wp,
                    'kecamatan' => $camat->nm_camat,
                    'kelurahan' => $lurah->nm_lurah,
                    'nm_usaha' => (!empty($usaha) ? $usaha->nm_usaha : ''),
                    'nmr_siup' => (!empty($usaha) ? $usaha->nmr_siup : ''),
                    'jm_peg' => (!empty($usaha) ? $usaha->jm_peg : ''),
                    'tg_siup' => (!empty($usaha) ? (empty($usaha->tg_siup) ? '' : date('d-m-Y', strtotime($usaha->tg_siup))) : ''),
                );
                echo json_encode($json);
            } else {
                echo '1';
            }
        } catch (Exception $ex) {
            $ex = '1';
            echo $ex;
        }
    }

    public function actionGetKodeRekeningSearch()
    {
        $tahun = filter_input(INPUT_POST, 'th_spt');
        $kd_rek = '1111';
        try {
            $default = array(array('id' => 0, 'text' => 'Pilih Kode Rekening'));
            $rows = (new \yii\db\Query())
                        ->select(["CONCAT(kd_ayt, jn_ayt, kl_ayt, ' - ', nm_ayt) AS text", 'id_ayt AS id'])
                        ->from('pad.t_ayat')
                        ->where('tahun = '.$tahun." AND kd_ayt = '$kd_rek' AND jn_ayt <> '00' AND kl_ayt <> '00' ORDER BY kd_ayt,jn_ayt,kl_ayt")
                        ->all();
            $merged = array_merge($default, $rows);
            if ($rows != null) {
                echo json_encode($merged);
            } else {
                echo '1';
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function actionListrekening()
    {
        $searchAyat = new TAyatSearch();
        if (!empty($_POST['kd_ayt'])) {
            $searchAyat->kd_ayt = $_POST['kd_ayt'];
        }
        if (!empty($_POST['tahun'])) {
            $searchAyat->tahun = $_POST['tahun'];
        }
        $dataAyat = $searchAyat->search(Yii::$app->request->queryParams);
        $dataAyat->pagination->pageSize = 100;

        return $this->renderAjax('_m_ayat', [
                    'searchAyat' => $searchAyat,
                    'dataAyat' => $dataAyat,
        ]);
    }

    public function actionListWp()
    {
        $search = new TDaftar();
        $data = $search->search(Yii::$app->request->queryParams);
        $data->pagination->pageSize = 5;

        return $this->renderAjax('_m_wp', [
                    'search' => $search,
                    'data' => $data,
        ]);
    }

    public function actionGetregister()
    {
        $no_register = 0;
        $tgl_awal = filter_input(INPUT_POST, 'tgl_awal');
        $no_pokok = filter_input(INPUT_POST, 'no_pokok');
        $npwpd = str_replace('.', '', filter_input(INPUT_POST, 'npwpd'));
        $no_register = filter_input(INPUT_POST, 'status').filter_input(INPUT_POST, 'kd_ayt');
        $no_register .= substr($tgl_awal, 3, 2).substr($tgl_awal, -2);
        $banyakbayar = count(TDataSelf::find()->where("npwpd='$npwpd' and masaspt='".substr($tgl_awal, -4).substr($tgl_awal, 3, 2)."'")->all()) + 1;
        $count = str_pad($banyakbayar, 3, '0', STR_PAD_LEFT);
        $no_register .= $count;
        echo json_encode(str_replace(' ', '', $no_register));
    }

    public function actionGetDetailAyat()
    {
        $id_ayt = (int) filter_input(INPUT_POST, 'id_ayt');
        try {
            $data = TAyat::find()->where(['id_ayt' => $id_ayt])->asArray()->one();
            if ($data != null) {
                echo json_encode($data);
            } else {
                echo '1';
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function actionSave()
    {
        $modelData = new TDataSelf();
        $modelDaftar = new TDaftar();
        $modelAyat = new TAyat();

        $connection = \Yii::$app->db;

        $transaction = $connection->beginTransaction();

        $modelData->load(Yii::$app->request->post());
        $modelDaftar->load(Yii::$app->request->post());
        $modelAyat->load(Yii::$app->request->post());

        try {
            if (empty($modelData->id_ayt)) {
                $nm = TAyat::find()->where(['kd_ayt' => '1111', 'tahun' => $modelData->th_spt, 'jn_ayt' => '00', 'kl_ayt' => '00'])->one();
                $kode_rekening = '1111';
                $modelData->id_ayt = $nm->id_ayt;
            } else {
                $kode_rekening = TAyat::findOne(['id_ayt' => $modelData->id_ayt])->kd_ayt;
            }
            // $code = explode('/', $modelData->no_spt);
            // $no_form = str_pad($this->no_form($modelData->th_spt, $kode_rekening), 4, '0', STR_PAD_LEFT);
            $modelData->no_spt = $modelDaftar->no_pokok;
            // $modelData->no_form = $no_form.date('m').substr(date('Y'), 2, 2);
            $modelData->no_data_self = TNomorMax::findOne(['tahun' => $modelData->th_spt])->no_data + 1;
            $modelData->masaspt = substr($modelData->tgl_awal, -4).substr($modelData->tgl_awal, 3, 2);
            $modelData->jml_pjk = str_replace('.', '', $modelData->jml_pjk);
            $modelData->nilai = str_replace('.', '', $modelData->nilai);
            if (!empty($modelData->koef2)) {
                $modelData->koef2 = str_replace('.', '', $modelData->koef2);
            }
            $modelData->tgl_awal = date('Y-m-d', strtotime($modelData->tgl_awal));
            $modelData->tgl_akhir = date('Y-m-d', strtotime($modelData->tgl_akhir));
            $modelData->npwpd = $modelDaftar->npwpd;
            $modelData->st_sumber = '2';

            // $modelData->petugas = PosisiPegawai::findOne(['kd_jabatan' => '12'])->nip;
            $modelData->tgl_jatuh_tempo = $this->hitungJatuhTempo($modelData->tgl_awal);
            $modelData->kd_ayt = $kode_rekening;

            $files = UploadedFile::getInstance($modelData, 'upload_file');

            if ($files != null) {
                $modelData->upload_file = $modelData->no_data_self.'-'.$modelData->th_spt.'.'.$files->extension;
            } else {
                $modelData->upload_file = null;
            }
            if ($modelData->save(false)) {
                if ($files != null) {
                    if (!file_exists('uploads/pendataan/')) {
                        mkdir('uploads/pendataan/', 0777, true);
                    }
                    $files->saveAs('uploads/pendataan/'.$modelData->upload_file);
                }

                $tmax = TNomorMax::findOne(['tahun' => $modelData->th_spt]);
                if ($tmax == null) {
                    $no = new TNomorMax();
                    $no->no_data = $modelData->no_data_self;
                    $no->id_res = 0;
                    $no->id_hot = 0;
                    $no->id_rek = 0;
                    $no->no_tetap = 0;
                    $no->no_setor = 0;
                    $no->no_tegur = 0;
                    $no->no_paksa = 0;
                    $no->no_mohon = 0;
                    $no->tahun = $modelData->th_spt;
                    $no->save();
                } else {
                    $tmax->no_data = $modelData->no_data_self;
                    // if ($code[1] == 'HTL') {
                    //     $tmax->id_hot = (int) $no_form;
                    // } elseif ($code[1] == 'RES') {
                    //     $tmax->id_res = (int) $no_form;
                    // } elseif ($ocde[1] == 'HIB') {
                    //     $tmax->id_hub = (int) $no_form;
                    // } elseif ($code[1] == 'PRK') {
                    //     $tmax->id_park = (int) $no_form;
                    // } elseif ($code[1] == 'GAL') {
                    // $tmax->id_mblb = (int) $no_form;
                    // for ($i = 0; $i < count($_POST['TDataSelfMblb']['volume']); ++$i) {
                    //     $modelDataMblb1 = new TDataSelfMblb();
                    //     $modelDataMblb1->no_data_self = $modelData->no_data_self;
                    //     $modelDataMblb1->th_spt = $modelData->th_spt;
                    //     $modelDataMblb1->id_ayt = $_POST['TDataSelfMblb']['id_ayt'][$i];
                    //     $modelDataMblb1->jml_pengenaan = (float) str_replace(',', '.', str_replace('.', '', $_POST['TDataSelfMblb']['jml_pengenaan'][$i]));
                    //     $modelDataMblb1->volume = (float) str_replace(',', '.', $_POST['TDataSelfMblb']['volume'][$i]);
                    //     $modelDataMblb1->jml_bayar = (float) str_replace(',', '.', str_replace('.', '', $_POST['TDataSelfMblb']['jml_bayar'][$i]));
                    //     if ($modelDataMblb1->save()) {
                    //         continue;
                    //     } else {
                    //         $transaction->rollback();
                    //         echo CActiveForm::validate($modelDataMblb1);
                    //         exit;
                    //     }
                    // }
                    // }
                    $tmax->update();
                }
                if ($modelData->kd_ayt != '1105') {
                    $jn_ayt = TAyat::findOne(['id_ayt' => $modelData->id_ayt])->jn_ayt;
                    $modelUsaha = TUsahaWp::find()->where("npwpd='".$modelData->npwpd."' and kd_ayt='".$modelData->kd_ayt."' and jn_ayt='".$jn_ayt."'")->one();
                    if (empty($modelUsaha)) {
                        $modelUsaha = new TUsahaWp();
                        $modelUsaha->npwpd = $modelData->npwpd;
                        $modelUsaha->kd_ayt = $modelData->kd_ayt;
                        $modelUsaha->jn_ayt = $jn_ayt;
                        $modelUsaha->createdtime = date('Y-m-d H:i:s');
                        $modelUsaha->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                    }
                    $modelUsaha->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                    $modelUsaha->updatedtime = date('Y-m-d H:i:s');

                    $modelUsaha->load(Yii::$app->request->post());
                    if (!$modelUsaha->save()) {
                        echo json_encode(ActiveForm::validate($modelUsaha));
                        $transaction->rollback();
                    }
                }
                $transaction->commit();
                echo $modelData->th_spt.'/'.$modelData->no_data_self;
            } else {
                echo json_encode(ActiveForm::validate($modelData));
                $transaction->rollback();
            }
        } catch (Exception $ex) {
            echo json_encode($ex);
            $transaction->rollback();
        }
    }

    public function hitungJatuhTempo($tgl)
    {
        $tglJTempo = strtotime('+30 day', strtotime(date('Y-m-d', strtotime($tgl))));
        $tglJTempo1 = date('Y-m-d', $tglJTempo);

        return $tglJTempo1;
    }

    public function actionCetak($no_data_self, $tahun)
    {
        $paramsInput = array('tahun' => $tahun, 'no_data_self' => (int) $no_data_self, 'pencetak' => 'E-SPTPD-ONLINE');
        $tdataSelf = TDataSelf::find()->where(['no_data_self' => $no_data_self, 'th_spt' => $tahun])->one();
        $kdayt = $tdataSelf->kd_ayt;
        // var_dump($kdayt);
        // die();
        if ($kdayt == '1101') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdHotel.jasper';
        } elseif ($kdayt == '1102') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdRestoran.jasper';
        } elseif ($kdayt == '1103') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdHiburan.jasper';
        } elseif ($kdayt == '1105') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpd_PPJ.jasper';
            $paramsInput = array('tahun' => $tahun, 'no_data_self' => (int) $no_data_self);
        } elseif ($kdayt == '1111') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdMBLBSelf.jasper';
        } elseif ($kdayt == '1107') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdParkir.jasper';
        } elseif ($kdayt == '1109') {
            $path = Yii::$app->basePath.'/'.'../reports/CetakSptpd/cetakSptpdWalet.jasper';
        }
        $this->cetak($path, $paramsInput, true, 'sptpd-'.$no_data_self.'-'.$tahun);
    }

    public function actionListSptpd()
    {
        $search = new TDataSelfSearch();
        $data = $search->search(Yii::$app->request->queryParams);
        $data->pagination->pageSize = 5;

        return $this->render('menu_cetak', [
                    'search' => $search,
                    'data' => $data,
        ]);
    }
}
