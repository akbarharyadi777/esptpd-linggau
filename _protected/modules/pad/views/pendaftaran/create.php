<?php

use yii\web\View;
use yii\helpers\Url;
use kartik\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use app\modules\pad\models\TCamat;
use app\modules\pad\models\TUsaha;
use kartik\datecontrol\DateControl;

/* @var $this View */
$this->title = 'Rekam Pendaftaran';
?>
<div class="box box-widget" id="from_input">
  <div class="box-header with-border">
      <h3 class="box-title text-light-blue"><?=$this->title; ?></h3>
      <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
  </div>
  <?php
    $form = ActiveForm::begin([
      'id' => 'form-pendaftaran',
      'type' => ActiveForm::TYPE_VERTICAL,
      'options' => [
        'enctype' => 'multipart/form-data',
      ],
    ]);
  ?>
  <?php 
  if (!empty($msg)) {
      ?>
    <div class="col-md-12">
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Error!</strong> <?= $msg; ?>.
      </div>
    </div>
    <?php
  } ?>
  <div class="col-md-12">
    <fieldset>
      <legend>DIISI OLEH SELURUH WAJIB PAJAK</legend>
      <div class="row">
        <div class="col-md-1">
          <?= $form->field($model, 'pr')->dropDownList(
            ['P' => 'P', 'R' => 'R']
        ); ?>
        </div>
        <div class="col-md-2 col-md-offset-3">
          <?= $form->field($model, 'golongan')->dropDownList(
            [1 => '1 - Pribadi', 2 => '2 - Badan'],
            ['prompt' => '']
        ); ?>
        </div>
        <div class="col-md-2 col-md-offset-2">
          <?= $form->field($model, 'no_pokok')->textInput(['readOnly' => true]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <?= $form->field($model, 'nm_wp')->textInput(['maxlength' => '255']); ?>
        </div>
        <div class="col-md-8">
          <?= $form->field($model, 'alm_wp')->textArea(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <?= $form->field($model, 'kec_wp')->dropDownList(
            ArrayHelper::map(TCamat::find()->orderBy('kd_camat asc')->all(), 'kd_camat',
              function ($model) {
                  return $model['kd_camat'].' - '.$model['nm_camat'];
              }), ['id' => 'kec-id', 'prompt' => 'Pilih Kecamatan']
          ); ?>
        </div>
        <div class="col-md-4">
          <?= $form->field($model, 'kel_wp')->widget(DepDrop::classname(), [
              'options' => ['id' => 'kel-id'],
              'pluginOptions' => [
                  'depends' => ['kec-id'],
                  'placeholder' => 'Pilih Kelurahan',
                  'url' => Url::to(['/pad/pendaftaran/get-kelurahan']),
              ],
          ]); ?>
        </div>
        <div class="col-md-3">
          <?= $form->field($model, 'telp1', [
                  'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-phone"></i>']],
              ])->textInput([
                'onkeyup' => "this.value=this.value.replace(/[^\d]/,'')",
                'maxlength' => 12,
            ]); ?>
        </div>
      </div>
    </fieldset>
  </div>
  <div class="col-md-12">
    <fieldset>
      <legend>KETERANGAN PEMILIK ATAU PENGELOLA</legend>
      <div class="row">
        <div class="col-md-8">
          <?= $form->field($model, 'nm_pemilik')->textInput(['maxlength' => '255']); ?>
        </div>
        <div class="col-md-8">
          <?= $form->field($model, 'alm_pemilik')->textArea(); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <?= $form->field($model, 'kec_pemilik')->textInput(['maxlength' => '255']); ?>
        </div>
        <div class="col-md-4">
          <?= $form->field($model, 'kel_pemilik')->textInput(['maxlength' => '255']); ?>
        </div>
        <div class="col-md-4">
          <?= $form->field($model, 'kota_pemilik')->textInput(['maxlength' => '255']); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <?= $form->field($model, 'telp2', [
                  'addon' => ['prepend' => ['content' => '<i class="glyphicon glyphicon-phone"></i>']],
              ])->textInput([
                'onkeyup' => "this.value=this.value.replace(/[^\d]/,'')",
                'maxlength' => 12,
            ]); ?>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <?= $form->field($model, 'kd_usaha')->dropDownList(
            ArrayHelper::map(TUsaha::find()->orderBy('kd_usaha asc')->all(), 'kd_usaha',
              function ($model) {
                  return $model['kd_usaha'].' - '.$model['nm_usaha'];
              }), ['id' => 'usaha-id']
          ); ?>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <?= $form->field($model, 'no_daftar')->textInput(['readOnly' => true]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <?= $form->field($model, 'tgl_kirim')->widget(DateControl::classname(), [
                  'type' => DateControl::FORMAT_DATE,
                  'ajaxConversion' => false,
                  'widgetOptions' => [
                      'layout' => '{picker}{input}',
                      'pluginOptions' => [
                          'autoclose' => true,
                          'startDate' => '',
                      ],
                  ],
              ]);
          ?>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <?= $form->field($model, 'tgl_kembali')->widget(DateControl::classname(), [
                  'type' => DateControl::FORMAT_DATE,
                  'ajaxConversion' => false,
                  'widgetOptions' => [
                      'layout' => '{picker}{input}',
                      'pluginOptions' => [
                          'autoclose' => true,
                          'startDate' => '',
                      ],
                  ],
              ]);
          ?>
        </div>
      </div>
    </fieldset>
    <fieldset>
      <legend>Upload File : </legend>
        <div class="row">
          <div class="col-md-12" style="padding:20px;">
            <button type="button" class="btn btn-default addButton"  style="float:right;"><i class="fa fa-plus"></i> Tambah File</button>
          </div>
        </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group field-tdaftarfile-nama_file has-success">
            <label class="control-label" for="tdaftarfile-nama_file">Jenis File</label>
            <input type="text" class="tdaftarfile-nama_file form-control" name="nama_file[]" value="FC KTP" maxlength="255" aria-invalid="false" readonly=true>
            <div class="help-block"></div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="form-group field-tdaftarfile-kota_pemilik has-success">
            <label class="control-label" for="tdaftarfile-kota_pemilik">File</label>
            <input accept=
                    "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*" 
                    class="form-control file-upload" 
                    name="MultipleUploadForm[files][]" type="file" />
            <div class="help-block"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group field-tdaftarfile-nama_file has-success">
            <label class="control-label" for="tdaftarfile-nama_file">Jenis File</label>
            <input type="text" class="tdaftarfile-nama_file form-control" name="nama_file[]" value="IJIN USAHA" maxlength="255" aria-invalid="false" readonly=true>
            <div class="help-block"></div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="form-group field-tdaftarfile-kota_pemilik has-success">
            <label class="control-label" for="tdaftarfile-kota_pemilik">File</label>
            <input accept=
                    "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*" 
                    class="form-control file-upload" 
                    name="MultipleUploadForm[files][]" type="file" />
            <div class="help-block"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group field-tdaftarfile-nama_file has-success">
            <label class="control-label" for="tdaftarfile-nama_file">Jenis File</label>
            <input type="text" class="tdaftarfile-nama_file form-control" name="nama_file[]" value="FOTO LOKASI USAHA" maxlength="255" aria-invalid="false" readonly=true>
            <div class="help-block"></div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="form-group field-tdaftarfile-kota_pemilik has-success">
            <label class="control-label" for="tdaftarfile-kota_pemilik">File</label>
            <input accept=
                    "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*" 
                    class="form-control file-upload" 
                    name="MultipleUploadForm[files][]" type="file" />
            <div class="help-block"></div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <div class="form-group field-tdaftarfile-nama_file has-success">
            <label class="control-label" for="tdaftarfile-nama_file">Jenis File</label>
            <input type="text" class="tdaftarfile-nama_file form-control" name="nama_file[]" value="AKTA USAHA" maxlength="255" aria-invalid="false" readonly=true>
            <div class="help-block"></div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="form-group field-tdaftarfile-kota_pemilik has-success">
            <label class="control-label" for="tdaftarfile-kota_pemilik">File</label>
            <input accept=
                    "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
                    text/plain, application/pdf, image/*" 
                    class="form-control file-upload" 
                    name="MultipleUploadForm[files][]" type="file" />
            <div class="help-block"></div>
          </div>
        </div>
      </div>
      <div id="div-add">
      </div>
    </fieldset>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box-footer">
        <?=Html::submitButton(' <i class="glyphicon glyphicon-ok"></i> Register', ['id' => 'btn_simpan', 'class' => 'btn btn-success']); ?>
        <?=Html::a(' <i class="glyphicon glyphicon-share-alt"></i> Login', ['/site/login'], ['id' => 'btn_simpan', 'class' => 'btn btn-primary']); ?>
      </div>
    </div>
  </div>
  <?php
    ActiveForm::end();
  ?>
</div>


<?php
$js = <<<JS

jQuery(document).ready(function(){
  $('#tdaftar-telp1').on('keyup', function () {
    var telepon = $('#tdaftar-telp1').val();
    $('#tdaftar-telp2').val(telepon);
  });
  
  $('#tdaftar-alm_wp').on('keyup', function () {
    var alamat = $('#tdaftar-alm_wp').val();
    $('#tdaftar-alm_pemilik').val(alamat);
  });
  
  $('.addButton').click(function(){
    var retVal = prompt("Masukan jenis file : ", "Ketik Jenis File");
    $('#div-add').append("<div class='row'>" +
        "<div class='col-md-3'>" +
          "<div class='form-group field-tdaftarfile-nama_file has-success'>" +
            "<label class='control-label' for='tdaftarfile-nama_file'>Jenis File</label>" +
            "<input type='text' class='tdaftarfile-nama_file form-control' name='nama_file[]' value='" + retVal + "' maxlength='255' aria-invalid='false' readonly=true>" +
            "<div class='help-block'></div>" +
          "</div>" +
        "</div>" +
        "<div class='col-md-3 col-md-offset-1'>" +
          "<div class='form-group field-tdaftarfile-kota_pemilik has-success'>" +
            "<label class='control-label' for='tdaftarfile-kota_pemilik'>File</label>" +
            "<input accept='application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*' class='form-control file-upload' name='MultipleUploadForm[files][]' type='file' />" +
            "<div class='help-block'></div>" +
          "</div>" +
        "</div>" +
        "<div class='col-md-1'>" +
          "<div class='form-group field-tdaftarfile-nama_file has-success'>" +
            "<label class='control-label' for='tdaftarfile-nama_file'>Hapus</label>" +
            '<a href="javascript:void(0)" class="remove_attr btn btn-success btn-flat"><i class="fa fa-remove"></i></a>' +
            "<div class='help-block'></div>" +
          "</div>" +
        "</div>" +
      "</div>");
  });

  $(document).on("click",".remove_attr", function(e){
    e.preventDefault();
    $(this).parent().parent().parent().remove();
  });

  $('#form-pendaftaran').submit(function() {
    var error = true;
    $(".file-upload").each(function(){
      console.log($(this).val());
      if ($(this).val() == '') {
        error = false;
        $(this).parent().removeClass('has-success').addClass('has-error');
        $(this).parent().find('.help-block').text('File tidak boleh kosong');
      } else {
        $(this).parent().removeClass('has-error').addClass('has-success');
        $(this).parent().find('.help-block').text('');
      }
    });
    return error;
  });

  $('#kec-id').on('change', function(){
    var kecamatan = $("#kec-id option:selected").text().substring(5);
    $("#tdaftar-kec_pemilik").val(kecamatan);
  });

  $('#kel-id').on('change', function(){
    var kelurahan = $("#kel-id option:selected").text().substring(6);
    $("#tdaftar-kel_pemilik").val(kelurahan);
  });
});

JS;

$this->registerJs(
    $js,
    View::POS_END,
    'js-page'
);
