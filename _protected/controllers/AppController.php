<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use JasperPHP\JasperPHP;
use webvimark\modules\UserManagement\components\GhostAccessControl;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AppController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'ghost-access' => [
                'class' => GhostAccessControl::className(),
            ],
        ];
    }

    public function init()
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect(\Yii::$app->urlManager->createUrl('site/login'));
        }
    }

    public function cetak($path, $param, $download, $namafile)
    {
        $jasperPHP = new JasperPHP();
        $db = Yii::$app->basePath.'/config/db.php';
        $pathImage = Yii::$app->basePath.'/'.'../themes/images/';
        $pathreport = Yii::$app->basePath.'/'.'../temp/'.$namafile;
        $param['IMAGE_DIR'] = $pathImage;
        require $db;
        // $jasperPHP->compile(Yii::$app->basePath.'/'.'../reports/CetakSptpd/CetakSptpdHotel.jrxml')->execute();
        $jasperPHP->process($path, $pathreport, array('pdf'), $param, array(
            'driver' => 'postgres',
            'username' => $userName,
            'host' => $databaseAddress,
            'database' => $databaseName,
            'password' => $password,
            'port' => $port,
            'charset' => 'utf8',
        ))->execute();

        if ($download == true) {
            if (file_exists($pathreport.'.pdf')) {
                return Yii::$app->response->sendFile($pathreport.'.pdf', $namafile.'.pdf');
            }

            // unlink($pathreport . '.pdf');
        } else {
            $pathreport = rename($pathreport.'.pdf', $pathreport.'.pad');
        }
    }
}
