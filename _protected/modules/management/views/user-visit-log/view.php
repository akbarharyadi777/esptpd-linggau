<?php

use app\modules\management\models\UserVisitLog;
use app\modules\management\UserManagementModule;
use webvimark\modules\UserManagement\components\GhostHtml;
use yii\web\View;
use yii\widgets\DetailView;

/**
 * @var View $this
 * @var UserVisitLog $model
 */
$this->title = $model->id;
?>
<div class="user-visit-log-view">
    <div class="box box-widget">
        <div class="box-header with-border">
            <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
            <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
        </div>
        <div class="box-header with-border">
            <h3 class="box-title text-light-blue"><?= GhostHtml::a(UserManagementModule::t('back', 'Kembali'), ['index'], ['class' => 'btn btn-sm btn-warning']) ?></h3>
        </div>

        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'user_id',
                    'value' => @$model->user->username,
                ],
                'ip',
                'language',
                'os',
                'browser',
                'user_agent',
                'visit_time:datetime',
            ],
        ])
        ?>

    </div>
</div>
