<?php

/**
 * @var yii\widgets\ActiveForm $form
 * @var app\modules\management\models\rbacDB\Role $model
 */
use app\modules\management\models\rbacDB\AuthItemGroup;
use app\modules\management\UserManagementModule;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>
<div class="box box-widget">
    <div class="box-header with-border">
        <h3 class="box-title text-light-blue"><?= $this->title ?></h3>
        <div class="pull-right"><i class="fa fa-angle-double-right box-title text-light-blue"></i></div>
    </div>
    <?php
    $form = ActiveForm::begin([
                'id' => 'role-form',
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 4]
            ])
    ?>
    <div class="col-md-12">
        <div class="col-md-6">
            <?= $form->field($model, 'description')->textInput(['maxlength' => 255, 'autofocus' => $model->isNewRecord ? true : false]) ?>
        </div>
        <div class="col-md-6">
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>
        </div>
        <div class="col-md-6">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box-footer">
                <?php if ($model->isNewRecord): ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Buat'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php else: ?>
                    <?=
                    Html::submitButton(
                            UserManagementModule::t('back', 'Simpan'), ['class' => 'btn btn-success']
                    )
                    ?>
                <?php endif; ?>
                <?=
                Html::a(
                        UserManagementModule::t('back', 'Kembali'), ['/user-management/role/index'], ['class' => 'btn btn-warning']
                )
                ?>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>