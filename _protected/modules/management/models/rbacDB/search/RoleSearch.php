<?php
namespace app\modules\management\models\rbacDB\search;

class RoleSearch extends AbstractItemSearch
{
	const ITEM_TYPE = self::TYPE_ROLE;
}