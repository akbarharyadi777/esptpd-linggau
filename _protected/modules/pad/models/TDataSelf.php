<?php

namespace app\modules\pad\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "t_data_self".
 *
 * @property string $th_spt
 * @property int $no_data_self
 * @property int $id_ayt
 * @property float $nilai
 * @property float $jml_pjk
 * @property float $jumlah
 * @property string $keterangan
 * @property float $koef1
 * @property float $koef2
 * @property string $npwpd
 * @property string $masaspt
 * @property string $no_spt
 * @property string $tgl_awal
 * @property string $tgl_akhir
 * @property string $tahun
 * @property string $tgl_terima
 * @property string $nmr_bayar
 * @property string $flag
 * @property int $createdby
 * @property string $createdtime
 * @property int $updatedby
 * @property string $updatedtime
 * @property float $denda
 * @property string $tgl_jatuh_tempo
 * @property string $kd_ayt
 */
class TDataSelf extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $kd_rek;
    public $periode;
    public $tarifpr;
    public $nm_ayt;
    public $tarifrp;

    public static function tableName()
    {
        return 'pad.t_data_self';
    }

    public function behaviors()
    {
        return [
            [
                'class' => '\yiidreamteam\upload\FileUploadBehavior',
                'attribute' => 'upload_file',
                'filePath' => '@appRoot/uploads/[[pk]].[[extension]]',
                'fileUrl' => '/uploads/[[pk]].[[extension]]',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['th_spt', 'no_data_self', 'id_ayt', 'npwpd', 'masaspt', 'no_spt', 'tgl_awal', 'tgl_akhir', 'tgl_terima', 'nmr_bayar', 'tgl_jatuh_tempo'], 'required'],
            [['no_data_self', 'id_ayt', 'createdby', 'updatedby'], 'integer'],
            [['jumlah', 'denda'], 'number'],
            [['tgl_awal', 'tgl_akhir', 'tgl_terima', 'createdtime', 'updatedtime', 'tgl_jatuh_tempo'], 'safe'],
            [['th_spt', 'tahun', 'kd_ayt'], 'string', 'max' => 4],
            [['keterangan'], 'string', 'max' => 255],
            //[['upload_file'], 'file'],
            [['upload_file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf, xls, xlsx, zip, rar'],
            [['upload_file', 'koef1', 'koef2', 'jml_pjk', 'nilai'], 'safe'],
            //[['upload_file'], 'string', 'max' => 100],
            //[['npwpd'], 'string', 'max' => 14],
            [['masaspt'], 'string', 'max' => 6],
            // [['no_spt'], 'string', 'max' => 7],
            [['nmr_bayar'], 'string', 'max' => 20],
            [['flag'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'th_spt' => 'Tahun',
            'no_data_self' => 'No Data Self',
            'id_ayt' => 'Id Ayt',
            'nilai' => 'Field ini',
            'jml_pjk' => 'Jumlah Pajak',
            'jumlah' => 'Jumlah',
            'keterangan' => 'Keterangan',
            'koef1' => 'Koef1',
            'koef2' => 'Koef2',
            'npwpd' => 'NPWPD',
            'masaspt' => 'Masaspt',
            'no_spt' => 'No. Pokok',
            'tgl_awal' => 'Tgl Awal',
            'tgl_akhir' => 'Tgl Akhir',
            'tahun' => 'Tahun',
            'tgl_terima' => 'Tanggal Diterima',
            'nmr_bayar' => 'Nomor Bayar',
            'flag' => 'Flag',
            'createdby' => 'Createdby',
            'createdtime' => 'Createdtime',
            'updatedby' => 'Updatedby',
            'updatedtime' => 'Updatedtime',
            'denda' => 'Denda',
            'tgl_jatuh_tempo' => 'Tgl Jatuh Tempo',
            'kd_ayt' => 'Kd Ayt',
            'no_spt' => 'No SPT',
        ];
    }

    public function getTAyat()
    {
        return $this->hasOne(TAyat::className(), ['id_ayt' => 'id_ayt']);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->nilai)) {
                $this->nilai = 0;
                $this->jml_pjk = 0;
            }
            if ($this->isNewRecord) {
                $this->createdtime = date('Y-m-d H:i:s');
                $this->createdby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedby = Yii::$app->user->identity->id == null ? -1 : Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
                $no_reg = self::find()->where(['substring(nmr_bayar, 1, 9)' => substr($this->nmr_bayar, 0, 9)])->all();
                foreach ($no_reg as $key) {
                    if ($this->nmr_bayar == $key->nmr_bayar) {
                        $this->nmr_bayar = substr($this->nmr_bayar, 0, 9).str_pad((string) (substr($this->nmr_bayar, 9, 11) + 1), 3, '0', STR_PAD_LEFT);
                    }
                }
            } else {
                $this->updatedby = Yii::$app->user->identity->id;
                $this->updatedtime = date('Y-m-d H:i:s');
                $this->nmr_bayar = self::model()->find()->where(['no_data_self' => $this->no_data_self, 'th_spt' => $this->th_spt])->nmr_bayar;
            }

            return true;
        }
    }
}
